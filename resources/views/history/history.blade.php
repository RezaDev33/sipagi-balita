@extends('layouts.admin')

@section('title')
    <title>History</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">History</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="header-title">Aktivitas Petugas </h4><br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama Petugas</th>
                                <th scope="col">Aktivitas</th>
                                <th scope="col">Nama Pasien</th>
                                <th scope="col">Hasil</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Umi Chabibah</td>
                                    <td>Tambah Pasien</td>
                                    <td>Juan</td>
                                    <td>Nama Orang Tua : Kastuti <br> Alamat : Ds.Legok </td>
                                </tr>  
                                <tr>
                                    <th scope="row">2</th>
                                    <td>Reza Pahlevi Yahya</td>
                                    <td>Diagnosis</td>
                                    <td>Nofal</td>
                                    <td>Gizi Buruk</td>
                                </tr>  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
              	
            </div>
        </div>
    </div>
</main>
@endsection