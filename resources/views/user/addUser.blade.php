@extends('layouts.admin')

@section('title')
    <title>Daftar User</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">User</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="header-title">Daftar User</h4><br>
                        <form method="POST" action="/user/store">
                        {{ csrf_field() }}
                            <div class="form-group col-sm-4">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" class="form-control" name="username" id="inputUsername"placeholder="Username">
                                @if($errors->has('usename'))
                                <div class="text-danger">
                                    {{ $errors->first('username')}}
                                </div>
                                @endif
                            </div>
                            {{-- <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" type="text" name="email" id="inputEmail"placeholder="Email">
                            </div> --}}
                            <div class="form-group col-sm-4">
                                <label>Level User</label>
                                <select class="form-control" id="selectLevel" name="level_selected" required focus>
                                    <option value="" disabled selected>Silakan pilih level user</option>        
                                    @foreach($level as $value)
                                    <option value="{{$value->id_level}}">{{ $value->ket_level }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('level_selected'))
                                <div class="text-danger">
                                    {{ $errors->first('level_selected')}}
                                </div>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control" name="password" id="inputPass"placeholder="Password">
                                @if($errors->has('password'))
                                <div class="text-danger">
                                    {{ $errors->first('password')}}
                                </div>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="exampleInputPassword1">Konfirmasi Password</label>
                                <input type="password" class="form-control" name="password_confirmation" id="inputPass"placeholder="Konfirmasi Password">
                                @if($errors->has('password_confirmation'))
                                <div class="text-danger">
                                    {{ $errors->first('password_confirmation')}}
                                </div>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
              	
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    var mydropdown = document.getElementById('selectLevel');
    mydropdown.onchange = function(){
        mytextbox.value = mytextbox.value  + this.value; //to appened
       mytextbox.innerHTML = this.value;
       }
</script>
@endsection