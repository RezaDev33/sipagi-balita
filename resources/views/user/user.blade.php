@extends('layouts.admin')

@section('title')
    <title>Daftar User</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">User</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="header-title">Daftar User</h4><br>
                        <a href="{{route('user.create')}}">
                            <button type="button" class="btn btn-primary">Tambah User</button><br><br>
                        </a>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Nomor HP</th>
                                <th scope="col">Status</th>
                                <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Reza</td>
                                    <td>087775559651</td>
                                    <td>Petugas</td>
                                    <td>
                                        <a href="">
                                            <span class="badge badge-primary">Edit</span>
                                        </a>
                                        <a href="">
                                            <span class="badge badge-danger">Hapus</span>
                                        </a>
                                    </td>
                                </tr>
                                
                                
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
              	
            </div>
        </div>
    </div>
</main>
@endsection