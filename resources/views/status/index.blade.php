@extends('layouts.admin')

@section('title')
    <title>Daftar-Rekap-Pasien</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Rekap</li>
    </ol>
    <div class="container">    
        <div class="card">
            <div class="col-12 mt-3">
                <a class="btn btn-success" href="{{ route('export') }}">Export Data Balita</a>
            </div>
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="user-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Umur</th>
                                        <th>Orang Tua</th>
                                        <th>Tanggal Pemeriksaan</th>
                                        <th>BB/U</th>
                                        <th>PB-TB/U</th>
                                        <th>BB/PB-TB</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(function(){
    $('#user-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('rekap.data') }}",
        },
        columns: [
            { data: 'rownum', name: 'rownum' },
            { data: 'nama', name: 'nama' },
            { data: 'jenis_kelamin', name: 'jenis_kelamin' },
            { data: 'umur', name: 'umur' },
            { data: 'orang_tua', name: 'orang_tua' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'diagnosis_bbu', name: 'diagnosis_bbu' },
            { data: 'diagnosis_pbu', name: 'diagnosis_pbu' },
            { data: 'diagnosis_bbpb', name: 'diagnosis_bbpb' },
        ]
    });
});
</script>
@endsection