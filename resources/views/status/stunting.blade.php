@extends('layouts.admin')

@section('title')
    <title>Daftar-Pasien-Stunting</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Stunting</li>
    </ol>
    <div class="container">    
        <div class="card">
            {{-- <div class="col-12 mt-3 mb-3">
                <button type="button" name="create_record" id="create_record" class="btn btn-success btn-md">Create Record</button>
            </div> --}}
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="user-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Umur</th>
                                        <th>Orang Tua</th>
                                        <th>Tanggal Pemeriksaan</th>
                                        <th>Kecamatan</th>
                                        <th>Desa</th>
                                        <th>Status Gizi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(function(){
    $('#user-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('stunting.data') }}",
        },
        columns: [
            { data: 'rownum', name: 'rownum' },
            { data: 'nama', name: 'nama' },
            { data: 'jenis_kelamin', name: 'jenis_kelamin' },
            { data: 'umur', name: 'umur' },
            { data: 'orang_tua', name: 'orang_tua' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'kecamatan', name: 'kecamatan' },
            { data: 'desa', name: 'desa' },
            { data: 'status_gizi', name: 'status_gizi' }
        ]
    });
});
</script>
@endsection