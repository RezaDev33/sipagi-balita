@extends('layouts.admin')

@section('title')
    <title>Edit Pasien</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Pasien</li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-8">
                <div class="card mt-3">
                    <div class="card-header"><h5>Edit Pasien</h5></div>
                    <div class="card-body">
                    <form class="row" action="{{ route('update', $pasien->id_pasien) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="_method" value="POST">
                        <div class="form-process"></div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Anak ke berapa?</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="number" min="1" max="20" name="anak_ke"  class="form-control required" value="{{$pasien->anak_ke}}"  placeholder="Anak ke">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Nama Anak</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nm_pasien" class="form-control required" value="{{$pasien->nm_pasien}}"  placeholder="nama anak">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" name="jenis_kelamin"  class="form-control required" value="{{$pasien->jenis_kelamin}}"  placeholder="Anak ke">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Tanggal Lahir</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="tgl_lahir" class="form-control required" value="{{$pasien->tgl_lahir}}"  placeholder="2017/01/01">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Nomor KK</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="no_kk" class="form-control required" value="{{$pasien->no_kk}}"  placeholder="Nomor Kartu Keluarga">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">NIK</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nik" class="form-control required" value="{{$pasien->nik}}"  placeholder="NIK anak">
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-weight">Berat badan saat lahir</label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="float" max="140" name="bb_lahir" id="fitness-form-weight" class="form-control required" value="{{$pasien->bb_lahir}}"  placeholder="contoh : 4.3">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-height">Panjang badan saat lahir</label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="float" maxlength="3" name="tb_lahir" id="fitness-form-height" class="form-control required" value="{{$pasien->tb_lahir}}"  placeholder="contoh : 70">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white">cm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <label class="d-block">KIA :</label>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" checked id="customRadio1" name="kia" value="ya" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Ya</label>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="kia" value="tidak" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Tidak</label>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <label class="d-block">IMD :</label>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" checked id="customRadio1" name="imd" value="ya" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Ya</label>
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="imd" value="tidak" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Tidak</label>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Nama Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nm_ortu"  class="form-control required" value="{{$pasien->nm_ortu}}"  placeholder="Nama Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Status Ekonomi</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="status_ekonomi"  class="form-control required" value="{{$pasien->status_ekonomi}}"  placeholder="Nama Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">NIK Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nik_ayah" id="event-registration-first-name" class="form-control required" value="{{$pasien->nik_ayah}}"  placeholder="NIK Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Telp/HP Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="tlp_ortu"  class="form-control required" value="{{$pasien->tlp_ortu}}"  placeholder="Nomor Telp/HP Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3 col-form-label">
                                    <label for="fitness-form-age">Alamat</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="alamat" id="event-registration-first-name" class="form-control required" value="{{$pasien->alamat}}"  placeholder="NIK Orangtua">
                                </div>
                            </div>
                            
                        </div>                        
                        <div class="col-12 form-group">
                            
                        </div>
                        <div class="col-12 form-group">
                            
                        </div>
                        <div class="col-12">
                            <button type="submit" name="event-registration-submit" class="btn btn-secondary">Simpan</button>
                        </div>

                        <input type="hidden" name="prefix" value="event-registration-">
                    </form>
                    </div>
                </div>
            </div>              	
            </div>
        </div>
    </div>
</main>
<!-- <script>
    function getval(sel){
        var kecamatan = sel.value;
        $.ajax({
            type:'GET',
            url:"{{url('alamat')}}/"+kecamatan,
            success:function(data){
                var kelurahan ='';
                var panjangdata = data[0].length;
                for (var index = 0; index < panjangdata; index++) {
                  kelurahan+=`<option value="${data[0][index].nm_desa}">${data[0][index].nm_desa}</option>`; 
                //   console.log(data[0][index].id_desa);
                  document.getElementById("desa").innerHTML =kelurahan;
                }
                // console.log(data);
            }
        });
        console.log(kecamatan);
    }
    
</script> -->
@endsection
