@extends('layouts.admin')

@section('title')
    <title>Daftar Pasien</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">pasien</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-2">
                    <div class="card-body">
                        <h4 class="header-title">Daftar Pasien</h4><br>
                        <a href="/pasien/add">
                            <button type="button" class="btn btn-primary">Tambah Pasien</button><br><br>
                        </a> 
                            <div class="table-responsive">
                                <table id="user_table" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th width="15%">Nama Anak</th>
                                            <th width="10%">Tanggal Lahir</th>
                                            <th width="15%">Nama Orang Tua</th>
                                            <th width="25%">Alamat</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
                 <h4 class="modal-title">Data Pengukuran</h4>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                 @csrf
                <div class="form-group">
                    <label class="control-label col-md-12">Tinggi Badan</label>
                    <div class="col-md-8">
                     <input type=text class="number" id="tinggi_badan" name="tinggi_badan" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-12">Berat Badan</label>
                    <div class="col-md-8">
                     <input type="decimal" class="number" id="berat_badan"  name="berat_badan" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-12">Jenis Pengukuran</label>
                    <div class="col-md-8">
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio3" name="jenis_pengukuran" value="PB" class="custom-control-input"  checked>
                            <label class="custom-control-label" for="customRadio3">Tiduran</label>
                        </div>
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio4" name="jenis_pengukuran" value="TB" class="custom-control-input" >
                            <label class="custom-control-label" for="customRadio4">Berdiri</label>
                        </div>
                    </div>
                    </div>
                </div>
                       <div class="form-group">
                        <div class="col-md-8">
                        <input type="hidden" name="id" id="hidden_id" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-success" value="Proses" />
                        </div>   
                    </div>
                </form>
               </div>
            </div>
           </div>
       </div>
    </div>

    <div id="confirmMod" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Confirmation</h2>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                        <h4 style="margin:0;" id="text_proses">Apakah kamu yakin akan mendignosis pasien?</h4>
                    </div>
                    <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-primary">Diagnosis</button>
                        <button type="button" class="btn btn-default" id="cencel_button" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    

        <div id="infoModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="text_title">Behasil</h2>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                        <div class="modal-body">
                            <h4 style="margin:0;" id="text_hasil">Pasien telah berhasil di diagnosis</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id="infoSalah" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="text_title">Gagal</h2>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                            <div class="modal-body">
                                <h4 style="margin:0;" id="text_hasil">Pasien gagal di diagnosis</h4>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>

</main>
<script>
$(document).ready(function(){
    $('.number').keypress(function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
    });
    $.fn.dataTable.ext.errMode = 'none';
    
    $('#user_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('pasien.index') }}",
        },
        columns: [
            {data: 'no', name: 'no'},
            {data: 'nm_pasien',name: 'nm_pasien'},
            {data: 'tgl_lahir',name: 'tgl_lahir'},
            {data: 'nm_ortu',name: 'nm_ortu'},
            {data: 'alamat',name: 'alamat'},
            {data: 'action',name: 'action',orderable: false}
        ]
    });

    var user_id;

    $(document).on('click', '.diagnosis', function(){
        user_id = $(this).attr('id');
        $('#hidden_id').val(user_id);
        $('#confirmModal').modal('show');
    });
    $('#sample_form').on('submit', function(event){
        event.preventDefault();
        var action_url = "{{ route('diagnosis.data') }}";

        $.ajax({
        url: action_url,
        method:"POST",
        data:$(this).serialize(),
        dataType:"json",
            beforeSend:function(){
                    $('#confirmModal').modal('hide');
                    $('#confirmMod').modal('show');
                    $('#text_proses').text('Data sedang di proses, mohon tunggu sebentar!');
                    $('#ok_button').text('Memproses data...').attr('disabled', true);
                    $('#cencel_button').attr('disabled', true);
            },
            success:function(data)
            {
                if(data.errors)
                {
                    setTimeout(function(){
                    $('#confirmMod').modal('hide');
                    $('#user-table').DataTable().ajax.reload();
                    $('#infogagal').modal('show');
                    }, 2000);
                }
                if(data.success)
                {
                html = '<div class="alert alert-success">' + data.success + '</div>';
                    setTimeout(function(){
                    $('#confirmMod').modal('hide');
                    $('#user-table').DataTable().ajax.reload();
                    $('#infoModal').modal('show');
                    }, 2000);
                }
            }
        });
    });

        $('#ok_button').click(function(){
            $.ajax({
                url:"diagnosis/pasien/"+user_id,
                beforeSend:function(){
                    $('#text_proses').text('Data sedang di proses, mohon tunggu sebentar!');
                    $('#ok_button').text('Memproses data...').attr('disabled', true);
                    $('#cencel_button').attr('disabled', true);
            },
            // success:function(data)
            // {
            //     setTimeout(function(){
            //     $('#confirmModal').modal('hide');
            //     $('#user-table').DataTable().ajax.reload();
            //     $('#infoModal').modal('show');
            //     }, 2000);
            // }
            })
        });
});
</script>
@endsection