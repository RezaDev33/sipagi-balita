@extends('layouts.admin')

@section('title')
    <title>Daftar Pasien</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">pasien</li>
    </ol>
    <div class="container">    
        <div class="card">
            {{-- <div class="col-12 mt-3 mb-3">
                <button type="button" name="create_record" id="create_record" class="btn btn-success btn-md">Create Record</button>
            </div> --}}
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nama Anak</th>
                                        <th>Nama Orang Tua</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($datas as $data)
                                    <tr>
                                        <td>
                                            
                                             {{$data->nm_pasien}}
                                        </a>
                                        </td>
                                        <td>
                                            {{$data->nm_ortu}}
                                        </td>
                                        <td>
                                            {{$data->alamat}}
                                        </td>
                                        <!-- <td>
                                        <div class="btn-group dropdown">
                                            <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action
                                            </button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 30px, 0px);">
                                            <a class="dropdown-item" href="{{route('user.edit', $data->id)}}"> Edit </a>
                                                <form action="{{ route('user.destroy', $data->id) }}" class="pull-left"  method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button class="dropdown-item" onclick="return confirm('Anda yakin ingin menghapus data ini?')"> 
                                                    Delete
                                                </button>
                                        </form>
                                        
                                        </div>
                                        </div>
                                        </td> -->
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 50
    });

} );
</script>
@stop

@endsection