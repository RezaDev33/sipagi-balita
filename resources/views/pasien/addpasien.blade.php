@extends('layouts.admin')

@section('title')
    <title>Tambah Pasien</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Pasien</li>
        <li class="breadcrumb-item active">Tambah</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-8">
                <div class="card mt-3">
                    <div class="card-header"><h5>Tambah Pasien</h5></div>
                    <div class="card-body">
                    <form class="row" action="store" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    
                        <div class="form-process"></div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Anak ke berapa?</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="number" min="1" max="20" name="anak_ke" id="anak_ke" class="form-control required" value="" required placeholder="Anak ke">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <label class="d-block">Jenis Kelamin</label>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio"  name="jenis_kelamin" value="laki-laki" checked>Laki-laki
                                                
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" name="jenis_kelamin" value="perempuan" >Perempuan
                                             
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Tanggal Lahir</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="tgl_lahir" id="event-registration-first-name" class="form-control required" value="" required placeholder="2019-10-10">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Nomor KK</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="number" name="no_kk"  class="form-control required" value="" required placeholder="Nomor Kartu Keluarga">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">NIK</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="number" name="nik" id="event-registration-first-name" class="form-control required" value="" required placeholder="NIK anak">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Nama Anak</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nm_pasien" id="event-registration-first-name" class="form-control required" value="" required placeholder="nama anak">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-weight">Berat badan saat lahir</label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="float"  name="bb_lahir" id="fitness-form-weight" class="form-control required" value="" required placeholder="contoh : 4.3">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white">kg</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-height">Panjang badan saat lahir</label>
                                </div>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input type="float"  name="tb_lahir" id="fitness-form-height" class="form-control required" value="" required placeholder="contoh : 70">
                                        <div class="input-group-append">
                                            <span class="input-group-text bg-white">cm</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <label class="d-block">Buku KIA :</label>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" name="kia" value="ya" checked>Ya
                                                
                                            </div>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" name="kia" value="tidak">Tidak
                                                
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 form-group">
                                    <label class="d-block">IMD :</label>
                                            <div class="form-check form-check-inline">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="imd" value="ya" checked>Ya
                                                    
                                                </div>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" name="imd" value="tidak" >Tidak
                                                    
                                                </div>
                                            </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Nama Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="nm_ortu" id="event-registration-first-name" class="form-control required" value="" required placeholder="Nama Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Status Ekonomi</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="status_ekonomi" id="event-registration-first-name" class="form-control required" value="" required placeholder="Status Ekonomi">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">NIK Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="number" name="nik_ayah" id="event-registration-first-name" class="form-control required" value="" required placeholder="NIK Ayah">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Telp/HP Orangtua</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="tlp_ortu" id="event-registration-first-name" class="form-control required" value=""  required placeholder="Nomor Telp/HP Orangtua">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-form-label">
                                    <label for="fitness-form-age">Alamat</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" name="alamat" id="event-registration-first-name" class="form-control required" value=""  required placeholder="Alamat">
                                </div>
                            </div>
                            
                        </div>                        
                        <div class="col-12 form-group">
                            
                        </div>
                        <div class="col-12 form-group">
                            
                        </div>
                        <div class="col-12">
                            <button type="submit" name="event-registration-submit" class="btn btn-secondary">Simpan</button>
                        </div>

                        <input type="hidden" name="prefix" value="event-registration-">
                    </form>
                    </div>
                </div>
            </div>              	
            </div>
        </div>
    </div>
</main>
<!-- <script>
    function getval(sel){
        var kecamatan = sel.value;
        $.ajax({
            type:'GET',
            url:"{{url('alamat')}}/"+kecamatan,
            success:function(data){
                var kelurahan ='';
                var panjangdata = data[0].length;
                for (var index = 0; index < panjangdata; index++) {
                  kelurahan+=`<option value="${data[0][index].nm_desa}">${data[0][index].nm_desa}</option>`; 
                //   console.log(data[0][index].id_desa);
                  document.getElementById("desa").innerHTML =kelurahan;
                }
                // console.log(data);
            }
        });
        console.log(kecamatan);
    }
    
</script> -->
@endsection
