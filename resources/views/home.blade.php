@extends('layouts.admin')

@section('title')
    <title>Dashboard</title>
@endsection


@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Aktivitas</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="callout callout-info">
                                        <small class="text-muted">Sangat Kurang</small>
                                        <br>
                                    <strong class="h4">{{$sangat_kurang}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-danger">
                                        <small class="text-muted">Kurang</small>
                                        <br>
                                        <strong class="h4">{{$kurang}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">Lebih</small>
                                        <br>
                                        <strong class="h4">{{$lebih}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Sangat Pendek</small>
                                        <br>
                                        <strong class="h4">{{$sangat_pendek}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">Pendek</small>
                                        <br>
                                        <strong class="h4">{{$pendek}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Sangat Kurus</small>
                                        <br>
                                        <strong class="h4">{{$sangat_kurus}}</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="callout callout-danger">
                                        <small class="text-muted">Kurus</small>
                                        <br>
                                        <strong class="h4">{{$kurus}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">Risiko Gemuk</small>
                                        <br>
                                        <strong class="h4">{{$risiko_gemuk}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Gemuk</small>
                                        <br>
                                        <strong class="h4">{{$gemuk}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-primary">
                                        <small class="text-muted">Obesitas</small>
                                        <br>
                                        <strong class="h4">{{$obesitas}}</strong>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="callout callout-success">
                                        <small class="text-muted">Normal</small>
                                        <br>
                                        <strong class="h4">{{$normal}}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection