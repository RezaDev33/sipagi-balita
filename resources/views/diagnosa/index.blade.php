@extends('layouts.admin')

@section('title')
    <title>Diagnosa</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Diagnosa</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            @if (session('alert'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>{{session('alert')}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
            <div class="col-6">
                <div class="card mt-3">
                    <div class="card-header"><h5>Diagnosis Kelainan Pada Balita</h5></div>
                    <div class="card-body">
                        <form class="needs-validation" novalidate="" method="post" action="/diagnosis/proses">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Nama</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" placeholder="Nama Balita">
                                    @error('nama')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Umur</label>
                                    <input type="number" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ old('umur') }}" placeholder="Umur Balita (Bulan)">
                                    @error('umur')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label class="d-block">Jenis Kelamin</label>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio1" name="jenis_kelamin" value="laki-laki" class="custom-control-input" @if(old('jenis_kelamin')=="laki-laki") checked @endif>
                                            <label class="custom-control-label" for="customRadio1">Laki-laki</label>
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio2" name="jenis_kelamin" value="perempuan" class="custom-control-input" @if(old('jenis_kelamin')!="laki-laki") checked @endif>
                                            <label class="custom-control-label" for="customRadio2">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Tinggi Badan</label>
                                    <input type="text" class="form-control" id="validationServer03" placeholder="Tinggi Badan Balita" required="">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    <label for="validationCustom01">Berat Badan</label>
                                    <input type="text" class="form-control" id="validationServer03" placeholder="Berat Badan Balita" required="">
                                    <div class="valid-feedback">
                                        Looks good!
                                    </div>
                                </div>
                            </div> --}}
                            <div class="form-row">
                                <div class="col-md-3 mb-4">
                                    <label for="validationCustom01">Berat Badan</label>
                                    <input type="number" class="form-control @error('berat_badan') is-invalid @enderror" id="berat_badan" name="berat_badan" value="{{ old('berat_badan') }}" placeholder="BB (kg)">
                                    @error('berat_badan')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-3 mb-4">
                                    <label for="validationCustom01">PB / TB</label>
                                    <input type="number" class="form-control @error('tinggi_badan') is-invalid @enderror" id="tinggi_badan" name="tinggi_badan" value="{{ old('tinggi_badan') }}" placeholder="TB/PB (cm)">
                                    @error('tinggi_badan')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-5 mb-4">
                                    <label class="d-block">Jenis Pengukuran</label>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio3" name="jenis_pengukuran" value="PB" class="custom-control-input"  @if(old('jenis_pengukuran')=="PB") checked @endif>
                                            <label class="custom-control-label" for="customRadio3">Terlentang</label>
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="customRadio4" name="jenis_pengukuran" value="TB" class="custom-control-input" @if(old('jenis_pengukuran')!="PB") checked @endif>
                                            <label class="custom-control-label" for="customRadio4">Berdiri</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                  <input class="form-check-input @error('cek') is-invalid @enderror" type="checkbox" name="cek" value="cek" id="invalidCheck3" required>
                                  <label class="form-check-label" for="invalidCheck3">
                                    Data yang diisi telah sesuai dengan petunjuk.
                                  </label>
                                  @error('cek')
                                  <div class="invalid-feedback">
                                    Anda harus menyetujui sebelum melakukan proses diagnosa.
                                  </div>
                                  @enderror
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Diagnosis</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card text-white bg-success mt-3">
                    <div class="card-header"><h5>Petunjuk Pengisian Form</h5></div>
                    <div class="card-body">
                      <h6 class="card-title"><u>Fitur Diagnosis</u></h6>
                      <p class="card-text">Fitur ini hanya merupakan contoh untuk mendiagnosa pasien secara langsung, oleh karena itu hasil diagnosa tidak di inputkan ke database, untuk mendignosa pasien dianjurkan untuk memdaftarkan pasien terlebih dahulu.</p>
                      <h6 class="card-title"><u>Pengukuran Tinggi dan Panjang badan</u></h6>
                      <p class="card-text">Pengukuran panjang badan dapat dilakukan jika balita berumur 0 sampai dengan 24 bulan, pengukuran dilakukan dalam posisi balita tiduran.</p>
                      <p class="card-text">Pengukuran tinggi badan dapat dilakukan jika balita berumur 24 sampai dengan 60 bulan, pengukuran dilakukan dalam posisi balita berdiri.</p>
                      <p class="card-text">Hasil pengukuran panjang badan atau tinggi badan di inputkan pada kolom PB/TB, lalu pilih jenis pengukuran sesuai yang di praktikan.</p>
                    </div>
                </div>
            </div>
              	
            </div>
        </div>
    </div>
</main>
@endsection
