@extends('layouts.admin')

@section('title')
    <title>Proses Diagnosa</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Diagnosa</li>
        <li class="breadcrumb-item active">Proses</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card mt-3">
                        <div class="card-body">
                            <h4> Nilai Defuzzykasi = {{$hasil}}</h4>
                            <h4> Hasil Diagnosa  = {{$kriteria}}</h4>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card mt-3">
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h4> Nilai Konsekuen</h4>
                                        <ol>
                                            @foreach ($rule1 as $item)
                                                <li>{{$item}}</li>    
                                            @endforeach
                                        </ol>
                                    </div>
                                    <div class="col-sm-6">
                                        <h4> Nilai Predikat</h4>
                                        <ol>
                                            @foreach ($rule2 as $item)
                                                <li>{{$item}}</li>    
                                            @endforeach
                                        </ol>
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection