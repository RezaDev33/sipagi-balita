@extends('layouts.admin')

@section('title')
    <title>Proses Diagnosa</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Diagnosa</li>
        <li class="breadcrumb-item active">Proses</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card border-dark mb-3">
                        <div class="card-header"><h4>Laporan Hasil Diagnosa</h4></div>
                        <div class="card-body text-dark">
                            <div class="row">
                                <div class="col-6 col-sm-2 mt-2"><h5>Nama</h5></div>
                                <div class="col-6 col-sm-2 mt-2"><h5>{{$request->nama}}</h5></div>
                            
                                <!-- Force next columns to break to new line at md breakpoint and up -->
                                <div class="w-100 d-none d-md-block"></div>
                            
                                <div class="col-6 col-sm-2 mt-2"><h5>Umur</h5></div>
                                <div class="col-6 col-sm-2 mt-2"><h5>{{$request->umur}} bulan</h5></div>

                                <!-- Force next columns to break to new line at md breakpoint and up -->
                                <div class="w-100 d-none d-md-block"></div>
                            
                                <div class="col-6 col-sm-2 mt-2"><h5>Jenis Kelamin</h5></div>
                                <div class="col-6 col-sm-2 mt-2"><h5>{{$request->jenis_kelamin}}</h5></div>
                            </div>
                            <div class="card mt-3">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item">
                                        <div class="row">      
                                            <div class="col-sm-3">  
                                                <h5>Tabel PB/U atau TB/U</h5>
                                                {{-- <table class="table table-bordered">                               
                                                    <tbody>
                                                        <tr>
                                                            <th class="sdmin3">{{$tb_u->sdmintiga}}</th>
                                                            <th class="sdmin2">{{$tb_u->sdmindua}}</th>
                                                            <th class="sdmin1">{{$tb_u->sdminsatu}}</th>
                                                            <th class="sdmedian">{{$tb_u->median}}</th>
                                                            <th class="sdplus1">{{$tb_u->sdplussatu}}</th>
                                                            <th class="sdplus2">{{$tb_u->sdplusdua}}</th>
                                                            <th class="sdplus3">{{$tb_u->sdplustiga}}</th>  
                                                        </tr>
                                                    </tbody>
                                                </table> --}}
                                                <div class="table-responsive-sm">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Tinggi Badan</th>
                                                            <th scope="col">Hasil Diagnosis</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th>{{$request->tinggi_badan}}</th>
                                                            <th>{{$kriteria[0]}}</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <h5>Tabel BB/U</h5>
                                                {{-- <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th class="sdmin3">{{$bb_u->sdmintiga}}</th>
                                                            <th class="sdmin2">{{$bb_u->sdmindua}}</th>
                                                            <th class="sdmin1">{{$bb_u->sdminsatu}}</th>
                                                            <th class="sdmedian">{{$bb_u->median}}</th>
                                                            <th class="sdplus1">{{$bb_u->sdplussatu}}</th>
                                                            <th class="sdplus2">{{$bb_u->sdplusdua}}</th>
                                                            <th class="sdplus3">{{$bb_u->sdplustiga}}</th>  
                                                        </tr>
                                                    </tbody>
                                                </table> --}}
                                                <div class="table-responsive-sm">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Berat Badan</th>
                                                            <th>Hasil Diagnosis</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th>{{$request->berat_badan}}</th>
                                                            <th>{{$kriteria[1]}}</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <h5>Tabel BB/TB atau BB/PB</h5>
                                                {{-- <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th class="sdmin3">{{$bb_pb->sdmintiga}}</th>
                                                            <th class="sdmin2">{{$bb_pb->sdmindua}}</th>
                                                            <th class="sdmin1">{{$bb_pb->sdminsatu}}</th>
                                                            <th class="sdmedian">{{$bb_pb->median}}</th>
                                                            <th class="sdplus1">{{$bb_pb->sdplussatu}}</th>
                                                            <th class="sdplus2">{{$bb_pb->sdplusdua}}</th>
                                                            <th class="sdplus3">{{$bb_pb->sdplustiga}}</th>
                                                        </tr>
                                                    </tbody>
                                                </table> --}}
                                                <div class="table-responsive-sm">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th scope="col">Berat Badan</th>
                                                            <th scope="col">Tinggi Badan</th>
                                                            <th scope="col">Hasil Diagnosis</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <th>{{$request->berat_badan}}</th>
                                                            <th>{{$request->tinggi_badan}}</th>
                                                            <th>{{$kriteria[2]}}</th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="alert {{$alert}}" role="alert">
                                                <div class="row">
                                                    <div class="col-6 col-sm-3 mt-2"><h5>Kesimpulan Diagnosis</h5></div>
                                                    <div class="col-6 col-sm-3 mt-2"><h5>{{$kriteria[3]}}</h5></div>
                                                </div>
                                            </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-dark">
                            <a class="btn btn-primary" href="{{ url('diagnosis') }}" role="button">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection