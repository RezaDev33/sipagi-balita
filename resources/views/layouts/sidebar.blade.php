<nav class="sidebar-nav">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="/home">
                <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="/pasien">
                <i class="nav-icon icon-people"></i> Pasien
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/diagnosis">
                <i class="nav-icon icon-layers"></i> Diagnosis
            </a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" href="/pasien">
                <i class="nav-icon icon-people"></i> Posyandu
            </a>
        </li> --}}
        {{-- <li class="nav-item">
            <a class="nav-link" href="/standar-pertumbuhan">
                <i class="nav-icon icon-globe"></i> Tabel WHO
            </a>
        </li> --}}
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-globe"></i> Tabel WHO
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="/bbu">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> BB / U
                    </a>
                    <a class="nav-link" href="/pbu">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> PB / U
                    </a>
                    <a class="nav-link" href="/tbu">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> TB / U
                    </a>
                    <a class="nav-link" href="/bbpb">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> BB / PB
                    </a>
                    <a class="nav-link" href="/bbtb">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> BB / TB
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/rule">
                <i class="nav-icon icon-layers"></i> Rule
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/rekap">
                <i class="nav-icon icon-layers"></i> Rekap Status Gizi
            </a>
        </li>
        {{-- <li class="nav-item">
            <a class="nav-link" href="/user">
                <i class="nav-icon icon-user"></i> User
            </a>
        </li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon icon-globe"></i> User
            </a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="/dinaskesehatan">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> Dinas Kesehatan
                    </a>
                    <a class="nav-link" href="/puskesmas">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> Puskesmas
                    </a>
                    <a class="nav-link" href="/posyandu">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="nav-icon icon-plus"></i> Posyandu
                    </a>
                </li>
            </ul>
        </li>
        --}}
    </ul>
</nav>