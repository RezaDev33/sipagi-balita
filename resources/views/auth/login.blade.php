@extends('layouts.auth')

@section('title')
    <title>Login</title>
@endsection

@section('content')
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100 p-l-50 p-r-50 p-t-77 p-b-30">
                    <form action="{{route('login')}}" method="post" class="login100-form validate-form">
                        @csrf
                        <span class="login100-form-title p-b-55">
                            Login
                        </span>
                        @if (session('alert'))
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>{{session('alert')}}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <div class="wrap-input100 validate-input m-b-16" data-validate = "Username is required">
                            <input class="input100 {{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" name="username" placeholder="Username" value="{{old('username')}}" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <span class="lnr lnr-user"></span>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input m-b-16" data-validate = "Password is required">
                            <input class="input100 {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" name="password" placeholder="Password" required>
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <span class="lnr lnr-lock"></span>
                            </span>
                        </div>

                        <div class="contact100-form-checkbox m-l-4">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div>
                        
                        <div class="container-login100-form-btn p-t-25">
                            <button class="login100-form-btn">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection