@extends('layouts.admin')

@section('title')
    <title>Standar Pertumbuhan</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Rule</li>
    </ol>
    <div class="container">    
        <div class="card">
            <div class="col-12 mt-3">
                <button type="button" name="create_record" id="create_record" class="btn btn-success btn-md">Tambah Rule</button>
            </div>
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="user-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Status Gizi</th>
                                        <th scope="col">Kategori</th>
                                        <th scope="col">Batas</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
         <div class="modal-content">
          <div class="modal-header">
                 <h4 class="modal-title">Tamabah Rule</h4>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="sample_form" class="form-horizontal">
                 @csrf
                   <div class="form-group">
                    <label class="control-label col-md-4" > Status Gizi </label>
                    <div class="col-md-8">
                    <select class="form-control" id="status">
                        @foreach ($status as $status) 
                        <option name="status" value="{{ $status->id_status_gizi}}">{{$status->status_gizi }}</option>
                        @endforeach
                    </select>
                    </div>
                  </div>
                  <input type="hidden" name="data_status" id="test2" >
                  <div class="form-group">
                    <label class="control-label col-md-4" > Kategori </label>
                    <div class="col-md-8">
                    <select class="form-control" id="kategori">
                        @foreach ($kategori as $kategori) 
                        <option name="kategori" value="{{ $kategori->id_kategori}}">{{$kategori->nm_kategori }}</option>
                        @endforeach
                    </select>
                    </div>
                  </div>
                  <input type="hidden" name="data_kategori"  id="test1" >
                  {{-- <div class="form-group">
                    <label class="control-label col-md-4">Status Gizi</label>
                    <div class="col-md-8">
                     <input type="text" name="status" id="status" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Kategori</label>
                        <div class="col-md-8">
                         <input type="text" name="kategori" id="kategori" class="form-control" />
                        </div> --}}
                   <div class="form-group">
                    <label class="control-label col-md-4">Batas</label>
                    <div class="col-md-8">
                     <input type="text" name="batas" id="batas" class="form-control" />
                    </div>
                   </div>
                       <div class="form-group">
                        <div class="col-md-8">
                        <input type="hidden" name="action" id="action" value="Add" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-success" value="Tambah" />
                        </div>   
                    </div>
                </form>
               </div>
            </div>
           </div>
       </div>
    </div>


       <div id="confirmModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Confirmation</h2>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                    <div class="modal-body">
                        <h4 style="margin:0;">Apakah kamu yakin ingin menghapus data?</h4>
                    </div>
                    <div class="modal-footer">
                    <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div id="infoModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Behasil</h2>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                        <div class="modal-body">
                            <h4 style="margin:0;">Data berhasil di hapus?</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                        </div>
                    </div>
                </div>
            </div>

    </main>
<script>
    $(function(){
    $.fn.dataTable.ext.errMode = 'none'; // hilangkan error massage
    $('#user-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('rule.data') }}",
        },
        columns: [
            { data: 'status', name: 'status_gizi.status_gizi' },
            { data: 'kategori', name: 'kategori.nm_kategori' },
            { data: 'batas', name: 'batas' },
            {
                data: 'action',
                name: 'action',
                orderable: false
            }
        ]
    });
    $('#create_record').click(function(){
        $('.modal-title').text('Tambah Rule');
        $('#action_button').val('Tambah');
        $( "#kategori" ).val($("#kategori option:first").val());
        var textbox = document.getElementById("test1");
        $( "test1" ).val(textbox.value =$( "#kategori" ).val())
        $( "#status" ).val($("#status option:first").val());
        var textbox = document.getElementById("test2");
        $( "test2" ).val(textbox.value =$( "#status" ).val())
        $( "#kategori" ).change(function() {
				var sel = $( "#kategori option:selected" ).val();
				var textbox = document.getElementById("test1");
				textbox.value =$( "#kategori" ).val();
			});
        $( "#status" ).change(function() {
				var sel = $( "#status option:selected" ).val();
				var textbox = document.getElementById("test2");
				textbox.value =$( "#status" ).val();
			});
        $('#action').val('Add');
        $('#form_result').html('');
        $('#formModal').modal('show');
    });
    
    $('#sample_form').on('submit', function(event){
    event.preventDefault();
    var action_url = '';

    if($('#action').val() == 'Add')
    {
    action_url = "{{ route('rule.store') }}";
    }

    if($('#action').val() == 'Edit')
    {
    action_url = "{{ route('rule.update') }}";
    }

    $.ajax({
    url: action_url,
    method:"POST",
    data:$(this).serialize(),
    dataType:"json",
        success:function(data)
        {
            var html = '';
            if(data.errors)
            {
            html = '<div class="alert alert-danger">';
            for(var count = 0; count < data.errors.length; count++)
            {
            html += '<p>' + data.errors[count] + '</p>';
            }
            html += '</div>';
            }
            if(data.success)
            {
            html = '<div class="alert alert-success">' + data.success + '</div>';
            $('#sample_form')[0].reset();
            $('#formModal').modal('hide');
            $('#user-table').DataTable().ajax.reload();
            }
            $('#form_result').html(html);
        }
        });
    });

    $(document).on('click', '.edit', function(){
        var id = $(this).attr('id');
            $('#form_result').html('');
            $.ajax({
            url :"/rule/edit/"+id+"",
            dataType:"json",
            success:function(data)
            {
                // $('#kategori').val(data.result.kategori);
                // $('#status').val(data.result.status);
                $('#batas').val(data.result.batas);
                $('#hidden_id').val(id);
                $('.modal-title').text('Edit Rule');
                $( "#kategori" ).val($("#kategori option:first").val());
                var textbox = document.getElementById("test1");
                $( "test1" ).val(textbox.value =$( "#kategori" ).val())
                $( "#status" ).val($("#status option:first").val());
                var textbox = document.getElementById("test2");
                $( "test2" ).val(textbox.value =$( "#status" ).val())
                $( "#kategori" ).change(function() {
                        var sel = $( "#kategori option:selected" ).val();
                        var textbox = document.getElementById("test1");
                        textbox.value =$( "#kategori" ).val();
                    });
                $( "#status" ).change(function() {
                        var sel = $( "#status option:selected" ).val();
                        var textbox = document.getElementById("test2");
                        textbox.value =$( "#status" ).val();
                    });
                $('#action_button').val('Edit');
                $('#action').val('Edit');
                $('#formModal').modal('show');
            }
        })
    });

    var user_id;

    $(document).on('click', '.delete', function(){
        user_id = $(this).attr('id');
        $('#confirmModal').modal('show');
        });

        $('#ok_button').click(function(){
        $.ajax({
            url:"rule/destroy/"+user_id,
            beforeSend:function(){
                $('#ok_button').text('Deleting...');
        },
        success:function(data)
        {
            setTimeout(function(){
            $('#confirmModal').modal('hide');
            $('#user-table').DataTable().ajax.reload();
            $('#infoModal').modal('show');
            }, 2000);
        }
        })
    });
});
</script>

@endsection
