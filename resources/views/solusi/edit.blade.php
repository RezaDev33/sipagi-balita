@extends('layouts.admin')

@section('title')
    <title>Edit Solusi</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Pasien</li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                            {{ $error }} <br/>
                            @endforeach
                        </div>
                        @endif
                    <div class="card-body">
                        <h4 class="header-title">Edit Solusi</h4><br>
                        <form action="/solusi/update/{{$solusi->id}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        {{method_field('PUT')}}
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="text-input" class=" form-control-label">Status</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" name="status" value="{{$solusi->status}}"  class="form-control">
                                <small class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="textarea-input" class=" form-control-label">Penyebab</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <textarea name="penyebab" id="textarea-input" rows="9" placeholder="Maukkan Deskripsi..." class="form-control">{{$solusi->penyebab}}</textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="textarea-input" class=" form-control-label">Solusi</label>
                            </div>
                            <div class="col-12 col-md-9">
                                <textarea name="solusi" id="textarea-input" rows="9" placeholder="Maukkan Deskripsi..." class="form-control">{{$solusi->solusi}}</textarea>
                            </div>
                        </div>
                        <button type="reset" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left"></i> Kembali
                        </button>
                            
                        <button type="submit" class="btn btn-success btn-sm">
                            <i class="fa fa-floppy-o"></i> Simpan
                        </button>
                    </form>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</main>
@endsection