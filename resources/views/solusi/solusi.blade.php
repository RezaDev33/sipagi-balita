@extends('layouts.admin')

@section('title')
    <title>Solusi</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Solusi</li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
            <div class="col-12">
                <div class="card mt-5">
                    <div class="card-body">
                        <h4 class="header-title">Daftar Solusi</h4><br>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Status</th>
                                <th scope="col">Penyebab</th>
                                <th scope="col">Solusi</th>
                                <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($solusi as $s)
                                <tr>
                                    <th scope="row">1</th>
                                    <td>{{$s->status}}</td>
                                    <td>{{$s->penyebab}}</td>
                                    <td>{{$s->solusi}}</td>
                                    <td>
                                        <a href="/solusi/edit/{{$s->id}}">
                                            <span class="badge badge-primary">edit</span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</main>
@endsection