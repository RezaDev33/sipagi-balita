@extends('layouts.admin')

@section('title')
    <title>Standar Pertumbuhan</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Standar Pertumbuhan</li>
        <li class="breadcrumb-item active">TB / U</li>
    </ol>
    <div class="container">    
        <div class="card">
            {{-- <div class="col-12 mt-3 mb-3">
                <button type="button" name="create_record" id="create_record" class="btn btn-success btn-md">Create Record</button>
            </div> --}}
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="user-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Umur</th>
                                        <th>-3 SD</th>
                                        <th>-2 SD</th>
                                        <th>-1 SD</th>
                                        <th>Median</th>
                                        <th>+1 SD</th>
                                        <th>+2 SD</th>
                                        <th>+3 SD</th>
                                        <th>Jenis Kelamin</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(function(){
    $('#user-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "{{ route('tbu.data') }}",
        },
        columns: [
            { data: 'parameter', name: 'parameter' },
            { data: 'sdmintiga', name: 'sdmintiga' },
            { data: 'sdmindua', name: 'sdmintiga' },
            { data: 'sdminsatu', name: 'sdmintiga' },
            { data: 'median', name: 'sdmintiga' },
            { data: 'sdplussatu', name: 'sdmintiga' },
            { data: 'sdplusdua', name: 'sdmintiga' },
            { data: 'sdplustiga', name: 'sdmintiga' },
            { data: 'jenis_kelamin', name: 'jenis_kelamin' }
        ]
    });
});
</script>

@endsection
