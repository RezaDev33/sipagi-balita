@extends('layouts.admin')

@section('title')
    <title>Standar Pertumbuhan</title>
@endsection

@section('content')
<main class="main">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Standar Pertumbuhan</li>
        <li class="breadcrumb-item active">BB / U</li>
    </ol>
    <div class="container">    
        <div class="card">
            {{-- <div class="col-12 mt-3 mb-3">
                <form id="formnobtn" action="" method="POST" data-ajax="false">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-6">
                            <label class="d-block">Pilih Jenis Kelamin</label>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio1" name="jenis_kelamin" value="laki-laki" class="custom-control-input" @if(old('jenis_kelamin')=="laki-laki") checked @endif>
                                    <label class="custom-control-label" for="customRadio1">Laki-laki</label>
                                </div>
                            </div>
                            <div class="form-check form-check-inline">
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="customRadio2" name="jenis_kelamin" value="perempuan" class="custom-control-input" @if(old('jenis_kelamin')!="laki-laki") checked @endif>
                                    <label class="custom-control-label" for="customRadio2">Perempuan</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div> --}}
            <div class="col-12 mt-3 mb-3">
                        <div class="table-responsive">
                            <table id="user-table" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Umur</th>
                                        <th>-3 SD</th>
                                        <th>-2 SD</th>
                                        <th>-1 SD</th>
                                        <th>Median</th>
                                        <th>+1 SD</th>
                                        <th>+2 SD</th>
                                        <th>+3 SD</th>
                                        <th>Jenis Kelamin</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(function(){
        $('#user-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('bbu.data') }}",
            },
            columns: [
                { data: 'parameter', name: 'parameter' },
                { data: 'sdmintiga', name: 'sdmintiga' },
                { data: 'sdmindua', name: 'sdmintiga' },
                { data: 'sdminsatu', name: 'sdmintiga' },
                { data: 'median', name: 'sdmintiga' },
                { data: 'sdplussatu', name: 'sdmintiga' },
                { data: 'sdplusdua', name: 'sdmintiga' },
                { data: 'sdplustiga', name: 'sdmintiga' },
                { data: 'jenis_kelamin', name: 'jenis_kelamin' }
            ]
        });
    });
    // $(document).ready(function() {
    //     $('input[type="radio"]').click(function(){
    //         var checked_radio_id = $(this).val();
    //             $('#checked_id').val(checked_radio_id);
    //             }
    //     $('form#formnobtn').submit();
    //     });
    // });
</script>

@endsection
