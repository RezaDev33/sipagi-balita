<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SIPAGIBalita</title>
  <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
  <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/style.css')}}" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Medilab
    Theme URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <!--banner-->
  <section id="banner" class="banner">
    <div class="bg-color">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
              <a class="navbar-brand" href="#"><img src="{{asset('img/logo.png')}}" class="img-responsive" style="width: 140px; margin-top: -16px;"></a>
            </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#banner">Home</a></li>
                <li class=""><a href="#service">Problem</a></li>
                <li class=""><a href="#about">About</a></li>
                <li class=""><a href="#contact">Contact</a></li>
                <li class=""><a href="/login">Login</a></li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <div class="container">
        <div class="row">
          <div class="banner-info">
            <div class="banner-logo text-center">
              <img src="{{asset('img/logo.png')}}" class="img-responsive">
            </div>
            <div class="banner-text text-center">
              <h1 class="white">Ayo Cek Gizi Balita!!</h1>
              <p>Program Penanganan Masalah Gizi Balita,<br>Cegah Gizi Buruk dan Stunting</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ banner-->
  <!--service-->
  <link rel="stylesheet" href="assets/css/bootstrap.css">
  <section id="service" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4">
          <h2 class="ser-title">Masalah Gizi Balita</h2>
          <hr class="botm-line">
          <p>Berdasarkan laporan gizi global atau Global Nutrition Report di tahun 2014, Indonesia termasuk ke dalam 17 negara yang memiliki 3 permasalahan gizi sekaligus, yaitu stunting (pendek), wasting (kurus), dan overweight atau gizi lebih (obesitas).</p>
          <p>Berdasarkan hasil Riset Kesehatan Dasar (Riskesdas) Kementerian Kesehatan 2018 menunjukkan 17,7% bayi usia di bawah 5 tahun (balita) masih mengalami masalah gizi. Angka tersebut terdiri atas balita yang mengalami gizi buruk sebesar 3,9% dan yang menderita gizi kurang sebesar 13,8%.</p>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="service-info">
            <div class="icon-info">
              <h4>Nutrisi Balita</h4>
              <p>Gizi (nutrients) merupakan ikatan kimia yang diperlukan tubuh untuk melakukan fungsinya, yaitu menghasilkan energi, membangun dan memelihara jaringan, serta mengatur proses-proses kehidupan.</p>
              <p>Balita butuh lebih banyak lemak dan lebih sedikit serat agar terjadi penambahan berat badan yang sehat. Berikan si Kecil makanan anak dengan nutrisi yang seimbang.</p>
              <p>Masa balita adalah periode perkembangan fisik dan mental yang pesat. Pada masa ini, otak balita telah siap menghadapi berbagai stimulasi seperti belajar berjalan dan berbicara lebih lancar.</p>
            </div>
          </div>
         </div>
        <div class="col-md-4 col-sm-4">
          <div class="service-info">
            <div class="icon-info">
              <h4>Menu Seimbang</h4>
              <p>Karbohidrat: nasi, roti, sereal, kentang, mi, dll.</p>
              <p>Buah dan sayur: pisang, pepaya, jeruk, tomat, wortel, dll.</p>
              <p>Susu dan produk olahannya: susu pertumbuhan, keju dan yoghurt.</p>
              <p>Protein: ikan, susu, daging, telur, kacang-kacangan, dll.</p>
              <p>Lemak: Seperti yang terdapat dalam minyak, santan, mentega, roti, dan kue juga mengandung omega 3 dan 6 yang penting untuk perkembangan otak. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ service-->
  <!--cta-->
  <section id="cta-1" class="section-padding">
    <div class="container">
      <div class="row">
      <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <div id="productCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#productCarousel" data-slide-to="1"></li>
            <li data-target="#productCarousel" data-slide-to="2"></li>
            <li data-target="#productCarousel" data-slide-to="3"></li>
          </ol>

          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="{{asset('img/slide.png')}}"  alt="Image" style="width:1180px;height:600px;">
            </div>
            <div class="item">
              <img src="{{asset('img/slide1.jpg')}}"  alt="Image" style="width:1180px;height:600px;">
            </div>
            <div class="item">
              <img src="{{asset('img/slide2.jpg')}}"  alt="Image" style="width:1180px;height:600px;">
            </div>
            <div class="item">
              <img src="{{asset('img/slide01.png')}}"  alt="Image" style="width:1180px;height:600px;">
            </div>
          </div>
          <a class="left carousel-control" href="#productCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#productCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div> 
      </div>     
        <!-- <div class="schedule-tab">
        <div class="col-md-4 col-sm-4 bor-left">
            <div class="mt-boxy-color"></div>
            <div class="medi-info">
              <h3>Pemeriksaan di Posyandu</h3>
              <p>Pemeriksaan bisa dilakukan setiap hari kamis oleh petugas di Posyandu Sakura, Nusa Indah, Melati, Mawar, dll. Waktu pemeriksaan dari jam 08.00 - 13.00 WIB.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="medi-info">
              <h3>Pemeriksaan di Puskesmas</h3>
              <p>Pemeriksaan bisa dilakukan sesuai jadwal oleh petugas bagian gizi anak di Puskesmas dengan melakukan administrasi terlebih dahulu seperti biasanya.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 mt-boxy-3">
            <div class="mt-boxy-color"></div>
            <div class="time-info">
              <h3>Jadwal Pemeriksaan Puskesmas</h3>
              <table style="margin: 8px 0px 0px;" border="1">
                <tbody>
                  <tr>
                    <td>Senin - Kamis</td>
                    <td>08.00 - 14.00</td>
                  </tr>
                  <tr>
                    <td>Jumat</td>
                    <td>08.00 - 12.00</td>
                  </tr>
                  <tr>
                    <td>Sabtu & Minggu</td>
                    <td>Libur</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div> -->
          
     
      </div>
    </div>
  </section>
  <!--cta-->
  <!--about-->
  <section id="about" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-4 col-xs-12">
          <div class="section-title">
            <h2 class="head-title lg-line">Penyuluhan<br>Gizi Balita</h2>
            <hr class="botm-line">
            <p>Umumnya pemeriksaan status gizi balita dimulai dengan penyuluhan rutin oleh Pihak Desa dan Puskesmas Lohbener kepada masyarakat setempat.</p>
            <p>Pemeriksaan ini merupakan upaya untuk menanggulangi masalah gizi dan stunting pada balita, sehingga mengurangi angka gizi buruk di Indonesia.</p>
            <p>Balita diperiksa status gizinya di Posyandu atau Puskesmas terdekat dengan proses penimbangan berat badan dan pengukuran panjang/tinggi badan.</p>
            <p>Setiap bulan Februari dan Agustus Posyandu atau Puskesmas akan membagikan vitamin A secara gratis untuk balita.</p>
            <p>Selain medapatkan pemeriksaan balita juga mendapatkan PMT(Pemberian Makanan Tambahan) berupa kudapan yang bermutu untuk mencukupi gizinya.</p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div style="visibility: visible;" class="col-sm-12 more-features-box">
            <p></p>
            <div class="more-features-box-text">
              <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
              <div class="more-features-box-text-description">
                <h3>Info Gizi Buruk</h3>
                <p>Mengacu pada tabel pertumbuhan anak dari WHO, balita dikatakan mengalami gizi buruk ketika hasil pengukuran indikator BB/TB untuk status gizinya kurang dari 70 persen nilai median. Atau mudahnya, nilai cut off z score berada nilai pada kurang dari -3 SD. Gizi buruk paling sering dialami oleh balita dengan usia di bawah 5 tahun, ketika tubuhnya kekurangan energi protein (KEP) kronis.</p>
              </div>
            </div>
            <div class="more-features-box-text">
              <div class="more-features-box-text-icon"> <i class="fa fa-angle-right" aria-hidden="true"></i> </div>
              <div class="more-features-box-text-description">
                <h3>Info Stunting</h3>
                <p>stunting adalah persoalan gagal tumbuh di mana anak tumbuh tidak optimal seperti seharusnya, karena kekurangan gizi. Berdasarkan tabel grafik, anak disebut stunting bila nilai Z-score -2 SD, dan disebut stunting berat (severely stunting) bila nilai Z-score -3 SD. Stunting bisa diawali karena bayi lahir prematur, atau berat badan lahir rendah (BBLR).</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ about-->
  <!--contact-->
  <section id="contact" class="section-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="ser-title">Contact us</h2>
          <hr class="botm-line">
        </div>
        <div class="col-md-4 col-sm-4">
          <h3>Contact Info</h3>
          <div class="space"></div>
          <p><i class="fa fa-map-marker fa-fw pull-left fa-2x"></i>Jln. Lohbener<br> Kec. Lohbener, Kab. Indramayu</p>
          <div class="space"></div>
          <p><i class="fa fa-envelope-o fa-fw pull-left fa-2x"></i>puskesmas_lohbener@gmail.com</p>
          <div class="space"></div>
          <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i>0231 4776 982</p>
        </div>
      </div>
    </div>
  </section>
  <!--/ contact-->
  <!--footer-->
  <footer id="footer">
    <div class="footer-line">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            © Copyright 2020 Teknik Informatika - Polindra
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!--/ footer-->

  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.min.js')}}"></script>
  <script src="{{asset('js/custom.js')}}"></script>
  <script src="{{asset('contactform/contactform.js')}}"></script>
  
</body>

</html>
