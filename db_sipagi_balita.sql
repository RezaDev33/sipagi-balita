-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Mar 2020 pada 15.34
-- Versi server: 10.4.8-MariaDB
-- Versi PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sipagi_balita`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `desa`
--

CREATE TABLE `desa` (
  `id_desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `nm_desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `desa`
--

INSERT INTO `desa` (`id_desa`, `id_kecamatan`, `nm_desa`, `created_at`, `updated_at`) VALUES
('021801AA', 21801, 'HAURKOLOT', NULL, NULL),
('021801AB', 21801, 'HAURGEULIS', NULL, NULL),
('021801AC', 21801, 'SUKAJATI', NULL, NULL),
('021801AD', 21801, 'WANAKAYA', NULL, NULL),
('021801AE', 21801, 'KARANGTUMARITIS', NULL, NULL),
('021801AF', 21801, 'KERTANEGARA', NULL, NULL),
('021801AG', 21801, 'CIPANCUH', NULL, NULL),
('021801AH', 21801, 'MEKARJATI', NULL, NULL),
('021801AI', 21801, 'SIDADADI', NULL, NULL),
('021801AJ', 21801, 'SUMBERMULYA', NULL, NULL),
('021802AA', 21802, 'SUKASLAMET', NULL, NULL),
('021802AB', 21802, 'TANJUNGKERTA', NULL, NULL),
('021802AC', 21802, 'KROYA', NULL, NULL),
('021802AD', 21802, 'SUMBON', NULL, NULL),
('021802AE', 21802, 'SUKAMELANG', NULL, NULL),
('021802AF', 21802, 'TEMIYANG', NULL, NULL),
('021802AG', 21802, 'TEMIYANGSARI', NULL, NULL),
('021802AH', 21802, 'JAYAMULYA', NULL, NULL),
('021802AI', 21802, 'SUMBERJAYA', NULL, NULL),
('021803AA', 21803, 'KEDUNGDAWA', NULL, NULL),
('021803AB', 21803, 'BABAKANJAYA', NULL, NULL),
('021803AC', 21803, 'GABUSKULON', NULL, NULL),
('021803AD', 21803, 'SEKARMULYA', NULL, NULL),
('021803AE', 21803, 'KEDOKANGABUS', NULL, NULL),
('021803AF', 21803, 'RANCAMULYA', NULL, NULL),
('021803AG', 21803, 'RANCAHAN', NULL, NULL),
('021803AH', 21803, 'GABUSWETAN', NULL, NULL),
('021803AI', 21803, 'DRUNTEN WETAN', NULL, NULL),
('021803AJ', 21803, 'DRUNTEN KULON', NULL, NULL),
('021804AA', 21804, 'LOYANG', NULL, NULL),
('021804AB', 21804, 'AMIS', NULL, NULL),
('021804AC', 21804, 'JATISURA', NULL, NULL),
('021804AD', 21804, 'JAMBAK', NULL, NULL),
('021804AE', 21804, 'CIKEDUNG', NULL, NULL),
('021804AF', 21804, 'CIKEDUNG LOR', NULL, NULL),
('021804AG', 21804, 'MUNDAKJAYA', NULL, NULL),
('021805AA', 21805, 'TUNGGULPAYUNG', NULL, NULL),
('021805AB', 21805, 'TUGU', NULL, NULL),
('021805AC', 21805, 'NUNUK', NULL, NULL),
('021805AD', 21805, 'TEMPEL', NULL, NULL),
('021805AE', 21805, 'PANGAUBAN', NULL, NULL),
('021805AF', 21805, 'TELAGASARI', NULL, NULL),
('021805AG', 21805, 'LANGGENGSARI', NULL, NULL),
('021805AH', 21805, 'TAMANSARI', NULL, NULL),
('021805AI', 21805, 'LELEA', NULL, NULL),
('021805AJ', 21805, 'CEMPEH', NULL, NULL),
('021805AK', 21805, 'TEMPELKULON', NULL, NULL),
('021806AA', 21806, 'MULYASARI', NULL, NULL),
('021806AB', 21806, 'BANGODUA', NULL, NULL),
('021806AC', 21806, 'BEDUYUT', NULL, NULL),
('021806AD', 21806, 'KARANGGETAS', NULL, NULL),
('021806AE', 21806, 'TEGALGIRANG', NULL, NULL),
('021806AF', 21806, 'WANASARI', NULL, NULL),
('021806AG', 21806, 'MALANGSARI', NULL, NULL),
('021806AH', 21806, 'RANCASARI', NULL, NULL),
('021807AA', 21807, 'BANGKALOA ILIR', NULL, NULL),
('021807AB', 21807, 'WIDASARI', NULL, NULL),
('021807AC', 21807, 'KALENSARI', NULL, NULL),
('021807AD', 21807, 'BUNDER', NULL, NULL),
('021807AE', 21807, 'UJUNGARIS', NULL, NULL),
('021807AF', 21807, 'KONGSIJAYA', NULL, NULL),
('021807AG', 21807, 'UJUNGJAYA', NULL, NULL),
('021807AH', 21807, 'UJUNGPENDOKJAYA', NULL, NULL),
('021807AI', 21807, 'LEUWIGEDE', NULL, NULL),
('021807AJ', 21807, 'KASMARAN', NULL, NULL),
('021808AA', 21808, 'TULUNGAGUNG', NULL, NULL),
('021808AB', 21808, 'JENGKOK', NULL, NULL),
('021808AC', 21808, 'TEGALWIRANGRONG', NULL, NULL),
('021808AD', 21808, 'MANGUNTARA', NULL, NULL),
('021808AE', 21808, 'JAMBE', NULL, NULL),
('021808AF', 21808, 'LEMAHAYU', NULL, NULL),
('021808AG', 21808, 'TENAJAR KIDUL', NULL, NULL),
('021808AH', 21808, 'KERTASEMAYA', NULL, NULL),
('021808AI', 21808, 'KLIWED', NULL, NULL),
('021808AJ', 21808, 'TENAJAR', NULL, NULL),
('021808AK', 21808, 'LARANGANJAMBE', NULL, NULL),
('021808AL', 21808, 'TENAJAR LOR', NULL, NULL),
('021808AM', 21808, 'SUKAWERA', NULL, NULL),
('021809AA', 21809, 'PURWAJAYA', NULL, NULL),
('021809AB', 21809, 'KAPRINGAN', NULL, NULL),
('021809AC', 21809, 'SINGAKERTA', NULL, NULL),
('021809AD', 21809, 'DUKUHJATI', NULL, NULL),
('021809AE', 21809, 'TEGALMULYA', NULL, NULL),
('021809AF', 21809, 'KEDUNGWUNGU', NULL, NULL),
('021809AG', 21809, 'SRENGSENG', NULL, NULL),
('021809AH', 21809, 'LUWUNGGESIK', NULL, NULL),
('021809AI', 21809, 'KALIANYAR', NULL, NULL),
('021809AJ', 21809, 'KRANGKENG', NULL, NULL),
('021809AK', 21809, 'TANJAKAN', NULL, NULL),
('021810AA', 21810, 'KAPLONGANLOR', NULL, NULL),
('021810AB', 21810, 'TANJUNGPURA', NULL, NULL),
('021810AC', 21810, 'TANJUNGSARI', NULL, NULL),
('021810AD', 21810, 'PRINGGACALA', NULL, NULL),
('021810AE', 21810, 'BENDA', NULL, NULL),
('021810AF', 21810, 'SENDANG', NULL, NULL),
('021810AG', 21810, 'KARANGAMPEL KIDUL', NULL, NULL),
('021810AH', 21810, 'KARANGAMPEL', NULL, NULL),
('021810AI', 21810, 'DUKUHJERUK', NULL, NULL),
('021810AJ', 21810, 'DUKUHTENGAH', NULL, NULL),
('021810AK', 21810, 'MUNDU', NULL, NULL),
('021811AA', 21811, 'SEGERAN KIDUL', NULL, NULL),
('021811AB', 21811, 'SEGERAN', NULL, NULL),
('021811AC', 21811, 'JUNTIWEDEN', NULL, NULL),
('021811AD', 21811, 'JUNTIKEBON', NULL, NULL),
('021811AE', 21811, 'DADAP', NULL, NULL),
('021811AF', 21811, 'JUNTINYUAT', NULL, NULL),
('021811AG', 21811, 'JUNTIKEDOKAN', NULL, NULL),
('021811AH', 21811, 'PONDOH', NULL, NULL),
('021811AI', 21811, 'SAMBIMAYA', NULL, NULL),
('021811AJ', 21811, 'TINUMPUK', NULL, NULL),
('021811AK', 21811, 'LOMBANG', NULL, NULL),
('021811AL', 21811, 'LIMBANGAN', NULL, NULL),
('021812AA', 21812, 'SLEMAN', NULL, NULL),
('021812AB', 21812, 'TAMBI', NULL, NULL),
('021812AC', 21812, 'SUDIKAMPIRAN', NULL, NULL),
('021812AD', 21812, 'TAMBI LOR', NULL, NULL),
('021812AE', 21812, 'SLEMAN LOR', NULL, NULL),
('021812AF', 21812, 'MAJASARI', NULL, NULL),
('021812AG', 21812, 'MAJASIH', NULL, NULL),
('021812AH', 21812, 'SLIYEG', NULL, NULL),
('021812AI', 21812, 'GADINGAN', NULL, NULL),
('021812AJ', 21812, 'MEKARGADING', NULL, NULL),
('021812AK', 21812, 'SLIYEGLOR', NULL, NULL),
('021812AL', 21812, 'TUGU KIDUL', NULL, NULL),
('021812AM', 21812, 'TUGU', NULL, NULL),
('021812AN', 21812, 'LONGOK', NULL, NULL),
('021813AA', 21813, 'SUKALILA', NULL, NULL),
('021813AB', 21813, 'PILANGSARI', NULL, NULL),
('021813AC', 21813, 'JATIBARANG BARU', NULL, NULL),
('021813AD', 21813, 'BULAK', NULL, NULL),
('021813AE', 21813, 'BULAK LOR', NULL, NULL),
('021813AF', 21813, 'JATIBARANG', NULL, NULL),
('021813AG', 21813, 'KEBULEN', NULL, NULL),
('021813AH', 21813, 'PAWIDEAN', NULL, NULL),
('021813AI', 21813, 'JATISAWIT', NULL, NULL),
('021813AJ', 21813, 'JATISAWIT LOR', NULL, NULL),
('021813AK', 21813, 'KRASAK', NULL, NULL),
('021813AL', 21813, 'KALIMATI', NULL, NULL),
('021813AM', 21813, 'MALANGSEMIRANG', NULL, NULL),
('021813AN', 21813, 'LOBENER', NULL, NULL),
('021813AO', 21813, 'LOBENER LOR', NULL, NULL),
('021814AA', 21814, 'TEGALSEMBADRA', NULL, NULL),
('021814AB', 21814, 'SUKAREJA', NULL, NULL),
('021814AC', 21814, 'SUKAURIP', NULL, NULL),
('021814AD', 21814, 'RAWADALEM', NULL, NULL),
('021814AE', 21814, 'GELARMENDALA', NULL, NULL),
('021814AF', 21814, 'TEGALURUNG', NULL, NULL),
('021814AG', 21814, 'BALONGAN', NULL, NULL),
('021814AH', 21814, 'SUDIMAMPIR', NULL, NULL),
('021814AI', 21814, 'SUDIMAMPIRLOR', NULL, NULL),
('021814AJ', 21814, 'MAJAKERTA', NULL, NULL),
('021815AA', 21815, 'TELUKAGUNG', NULL, NULL),
('021815AB', 21815, 'PLUMBON', NULL, NULL),
('021815AC', 21815, 'DUKUH', NULL, NULL),
('021815AD', 21815, 'PEKANDANGAN JAYA', NULL, NULL),
('021815AE', 21815, 'SINGARAJA', NULL, NULL),
('021815AF', 21815, 'SINGAJAYA', NULL, NULL),
('021815AG', 21815, 'PEKANDANGAN', NULL, NULL),
('021815AH', 21815, 'BOJONGSARI', NULL, NULL),
('021815AI', 21815, 'KEPANDEAN', NULL, NULL),
('021815AJ', 21815, 'KARANGMALANG', NULL, NULL),
('021815AK', 21815, 'KARANGANYAR', NULL, NULL),
('021815AL', 21815, 'LEMAHMEKAR', NULL, NULL),
('021815AM', 21815, 'LEMAHABANG', NULL, NULL),
('021815AN', 21815, 'MARGADADI', NULL, NULL),
('021815AO', 21815, 'PAOMAN', NULL, NULL),
('021815AP', 21815, 'KARANGSONG', NULL, NULL),
('021815AQ', 21815, 'PABEANUDIK', NULL, NULL),
('021815AR', 21815, 'TAMBAK', NULL, NULL),
('021816AA', 21816, 'PANYINDANGAN KULON', NULL, NULL),
('021816AB', 21816, 'RAMBATAN WETAN', NULL, NULL),
('021816AC', 21816, 'PANYINDANGAN WETAN', NULL, NULL),
('021816AD', 21816, 'KENANGA', NULL, NULL),
('021816AE', 21816, 'TERUSAN', NULL, NULL),
('021816AF', 21816, 'DERMAYU', NULL, NULL),
('021816AG', 21816, 'SINDANG', NULL, NULL),
('021816AH', 21816, 'PENGANJANG', NULL, NULL),
('021816AI', 21816, 'BABADAN', NULL, NULL),
('021816AJ', 21816, 'WANANTARA', NULL, NULL),
('021817AA', 21817, 'KIAJARAN KULON', NULL, NULL),
('021817AB', 21817, 'KIJARAN WETAN', NULL, NULL),
('021817AC', 21817, 'LANJAN', NULL, NULL),
('021817AD', 21817, 'LANGUT', NULL, NULL),
('021817AE', 21817, 'LARANGAN', NULL, NULL),
('021817AF', 21817, 'WARU', NULL, NULL),
('021817AG', 21817, 'LEGOK', NULL, NULL),
('021817AH', 21817, 'BOJONGSLAWI', NULL, NULL),
('021817AI', 21817, 'LOHBENER', NULL, NULL),
('021817AJ', 21817, 'PAMAYAHAN', NULL, NULL),
('021817AK', 21817, 'SINDANGKERTA', NULL, NULL),
('021817AL', 21817, 'RAMBATAN KULON', NULL, NULL),
('021818AA', 21818, 'RANJENG', NULL, NULL),
('021818AB', 21818, 'KRIMUN', NULL, NULL),
('021818AC', 21818, 'PUNTANG', NULL, NULL),
('021818AD', 21818, 'PEGAGAN', NULL, NULL),
('021818AE', 21818, 'RAJAIYANG', NULL, NULL),
('021818AF', 21818, 'JANGGA', NULL, NULL),
('021818AG', 21818, 'JUMBLENG', NULL, NULL),
('021818AH', 21818, 'PANGKALAN', NULL, NULL),
('021818AJ', 21818, 'LOSARANG', NULL, NULL),
('021818AK', 21818, 'MUNTUR', NULL, NULL),
('021818AL', 21818, 'SANTING', NULL, NULL),
('021818AM', 21818, 'CEMARA KULON', NULL, NULL),
('021819AA', 21819, 'CURUG', NULL, NULL),
('021819AB', 21819, 'PRANTI', NULL, NULL),
('021819AC', 21819, 'WIRAKANAN', NULL, NULL),
('021819AD', 21819, 'KARANGMULYA', NULL, NULL),
('021819AE', 21819, 'KARANGANYAR', NULL, NULL),
('021819AF', 21819, 'WIRAPANJUNAN', NULL, NULL),
('021819AG', 21819, 'PAREAN GIRANG', NULL, NULL),
('021819AH', 21819, 'BULAK', NULL, NULL),
('021819AI', 21819, 'ILIR', NULL, NULL),
('021819AJ', 21819, 'SOGE', NULL, NULL),
('021819AK', 21819, 'ERETAN WETAN', NULL, NULL),
('021819AL', 21819, 'ERETAN KULON', NULL, NULL),
('021819AM', 21819, 'KERTAWINANGUN', NULL, NULL),
('021820AA', 21820, 'CIPEDANG', NULL, NULL),
('021820AB', 21820, 'SIDAMULYA', NULL, NULL),
('021820AC', 21820, 'MARGAMULYA', NULL, NULL),
('021820AD', 21820, 'KERTAJAYA', NULL, NULL),
('021820AE', 21820, 'BONGAS', NULL, NULL),
('021820AF', 21820, 'CIPAAT', NULL, NULL),
('021820AG', 21820, 'KERTAMULYA', NULL, NULL),
('021820AH', 21820, 'PLAWANGAN', NULL, NULL),
('021821AA', 21821, 'MANGUNJAYA', NULL, NULL),
('021821AB', 21821, 'BUGISTUA', NULL, NULL),
('021821AC', 21821, 'BUGIS', NULL, NULL),
('021821AD', 21821, 'SALAMDARMA', NULL, NULL),
('021821AE', 21821, 'KEDUNGWUNGU', NULL, NULL),
('021821AF', 21821, 'WANGUK', NULL, NULL),
('021821AG', 21821, 'LEMPUYANG', NULL, NULL),
('021821AH', 21821, 'KOPYAH', NULL, NULL),
('021821AI', 21821, 'ANJATAN BARU', NULL, NULL),
('021821AJ', 21821, 'ANJATAN', NULL, NULL),
('021821AK', 21821, 'CILANDAK', NULL, NULL),
('021821AL', 21821, 'CILANDAK LOR', NULL, NULL),
('021821AM', 21821, 'ANJATAN UTARA', NULL, NULL),
('021822AA', 21822, 'BOGOR', NULL, NULL),
('021822AB', 21822, 'SUKRA', NULL, NULL),
('021822AC', 21822, 'UJUNGGEBANG', NULL, NULL),
('021822AD', 21822, 'TEGALTAMAN', NULL, NULL),
('021822AE', 21822, 'SUKRAWETAN', NULL, NULL),
('021822AF', 21822, 'SUMURADEM', NULL, NULL),
('021822AG', 21822, 'SUMURADEM TIMUR', NULL, NULL),
('021822AH', 21822, 'KARANGLAYUNG', NULL, NULL),
('021823AA', 21823, 'SUKASARI', NULL, NULL),
('021823AB', 21823, 'ARAHAN KIDUL', NULL, NULL),
('021823AC', 21823, 'ARAHAN LOR', NULL, NULL),
('021823AD', 21823, 'LINGGAJATI', NULL, NULL),
('021823AE', 21823, 'TAWANGSARI', NULL, NULL),
('021823AF', 21823, 'SUKADADI', NULL, NULL),
('021823AG', 21823, 'PRANGGONG', NULL, NULL),
('021823AH', 21823, 'CIDEMPET', NULL, NULL),
('021824AA', 21824, 'CANGKRING', NULL, NULL),
('021824AB', 21824, 'CANTIGI KULON', NULL, NULL),
('021824AC', 21824, 'CANTIGI WETAN', NULL, NULL),
('021824AD', 21824, 'PANYINGKIRAN LOR', NULL, NULL),
('021824AE', 21824, 'PANYINGKIRAN KIDUL', NULL, NULL),
('021824AF', 21824, 'LAMARANTARUNG', NULL, NULL),
('021824AG', 21824, 'CEMARA', NULL, NULL),
('021825AA', 21825, 'BANTARWARU', NULL, NULL),
('021825AB', 21825, 'SANCA', NULL, NULL),
('021825AC', 21825, 'MEKARJAYA', NULL, NULL),
('021825AD', 21825, 'GANTAR', NULL, NULL),
('021825AE', 21825, 'SITURAJA', NULL, NULL),
('021825AF', 21825, 'BALERAJA', NULL, NULL),
('021825AG', 21825, 'MEKARWARU', NULL, NULL),
('021826AA', 21826, 'CIKAWUNG', NULL, NULL),
('021826AB', 21826, 'JATIMUNGGUL', NULL, NULL),
('021826AC', 21826, 'JATIMULYA', NULL, NULL),
('021826AD', 21826, 'PLOSOKEREP', NULL, NULL),
('021826AE', 21826, 'RAJASINGA', NULL, NULL),
('021826AF', 21826, 'KARANGASEM', NULL, NULL),
('021826AG', 21826, 'CIBERENG', NULL, NULL),
('021826AH', 21826, 'KENDAYAKAN', NULL, NULL),
('021826AI', 21826, 'MANGGUNGAN', NULL, NULL),
('021827AA', 21827, 'CIBEBER', NULL, NULL),
('021827AB', 21827, 'BONDAN', NULL, NULL),
('021827AC', 21827, 'GUNUNGSARI', NULL, NULL),
('021827AD', 21827, 'SUKAGUMIWANG', NULL, NULL),
('021827AE', 21827, 'TERSANA', NULL, NULL),
('021827AF', 21827, 'CADANGPINGGAN', NULL, NULL),
('021827AG', 21827, 'GEDANGAN', NULL, NULL),
('021828AA', 21828, 'KEDOKANBUNDER WETAN', NULL, NULL),
('021828AB', 21828, 'KAPLONGAN', NULL, NULL),
('021828AC', 21828, 'KEDOKAN AGUNG', NULL, NULL),
('021828AD', 21828, 'KEDOKANBUNDER', NULL, NULL),
('021828AE', 21828, 'JAYAWINANGUN', NULL, NULL),
('021828AF', 21828, 'CANGKINGAN', NULL, NULL),
('021828AG', 21828, 'JAYALAKSANA', NULL, NULL),
('021829AA', 21829, 'PAGIRIKAN', NULL, NULL),
('021829AB', 21829, 'PASEKAN', NULL, NULL),
('021829AC', 21829, 'BRONDONG', NULL, NULL),
('021829AD', 21829, 'PABEANILIR', NULL, NULL),
('021829AE', 21829, 'TOTORAN', NULL, NULL),
('021829AF', 21829, 'KARANGANYAR', NULL, NULL),
('021830AA', 21830, 'BODAS', NULL, NULL),
('021830AB', 21830, 'GADEL', NULL, NULL),
('021830AC', 21830, 'RANCAJAWAT', NULL, NULL),
('021830AD', 21830, 'KERTICALA', NULL, NULL),
('021830AE', 21830, 'SUKAMULYA', NULL, NULL),
('021830AF', 21830, 'KARANGKERTA', NULL, NULL),
('021830AG', 21830, 'CANGKO', NULL, NULL),
('021830AH', 21830, 'PAGEDANGAN', NULL, NULL),
('021830AI', 21830, 'SUKAPERNA', NULL, NULL),
('021830AJ', 21830, 'SUKADANA', NULL, NULL),
('021830AK', 21830, 'TUKDANA', NULL, NULL),
('021830AL', 21830, 'LAJER', NULL, NULL),
('021830AM', 21830, 'MEKARSARI', NULL, NULL),
('021831AA', 21831, 'LIMPAS', NULL, NULL),
('021831AB', 21831, 'PATROL', NULL, NULL),
('021831AC', 21831, 'ARJASARI', NULL, NULL),
('021831AD', 21831, 'SUKAHAJI', NULL, NULL),
('021831AE', 21831, 'BUGEL', NULL, NULL),
('021831AF', 21831, 'PATROLLOR', NULL, NULL),
('021831AG', 21831, 'PATROL BARU', NULL, NULL),
('021831AH', 21831, 'MEKARSARI', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL,
  `nm_kategori` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nm_kategori`) VALUES
(2, 'BB/U'),
(3, 'PB/U'),
(4, 'TB/U'),
(5, 'BB/PB'),
(6, 'BB/TB');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id_kecamatan` int(11) NOT NULL,
  `nm_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kecamatan`
--

INSERT INTO `kecamatan` (`id_kecamatan`, `nm_kecamatan`, `password_kecamatan`, `created_at`, `updated_at`) VALUES
(21801, 'HAURGEULIS', '', NULL, NULL),
(21802, 'KROYA', '', NULL, NULL),
(21803, 'GABUSWETAN', '', NULL, NULL),
(21804, 'CIKEDUNG', '', NULL, NULL),
(21805, 'LELEA', '', NULL, NULL),
(21806, 'BANGODUA', '', NULL, NULL),
(21807, 'WIDASARI', '', NULL, NULL),
(21808, 'KERTASEMAYA', '', NULL, NULL),
(21809, 'KRANGKENG', '', NULL, NULL),
(21810, 'KARANGAMPEL', '', NULL, NULL),
(21811, 'JUNTINYUAT', '', NULL, NULL),
(21812, 'SLIYEG', '', NULL, NULL),
(21813, 'JATIBARANG', '', NULL, NULL),
(21814, 'BALONGAN', '', NULL, NULL),
(21815, 'INDRAMAYU', '', NULL, NULL),
(21816, 'SINDANG', '', NULL, NULL),
(21817, 'LOHBENER', '', NULL, NULL),
(21818, 'LOSARANG', '', NULL, NULL),
(21819, 'KANDANGHAUR', '', NULL, NULL),
(21820, 'BONGAS', '', NULL, NULL),
(21821, 'ANJATAN', '', NULL, NULL),
(21822, 'SUKRA', '', NULL, NULL),
(21823, 'ARAHAN', '', NULL, NULL),
(21824, 'CANTIGI', '', NULL, NULL),
(21825, 'GANTAR', '', NULL, NULL),
(21826, 'TERISI', '', NULL, NULL),
(21827, 'SUKAGUMIWANG', '', NULL, NULL),
(21828, 'KEDOKAN BUNDER', '', NULL, NULL),
(21829, 'PASEKAN', '', NULL, NULL),
(21830, 'TUKDANA', '', NULL, NULL),
(21831, 'PATROL', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_11_234114_create_status_gizi', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2020_02_07_154755_create_kecamatan_table', 1),
(6, '2020_02_07_155840_create_desa_table', 1),
(7, '2020_02_13_095658_create_standar_pertumbuhan', 1),
(8, '2020_02_28_164432_create_posyandu', 1),
(9, '2020_02_28_224912_create_pasien', 1),
(10, '2020_02_28_230416_create_pemeriksaan', 1),
(11, '2020_02_28_232213_create_rule', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int(11) NOT NULL,
  `nik` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_kk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anak_ke` int(11) NOT NULL,
  `nm_pasien` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_ekonomi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bb_lahir` double(8,2) NOT NULL,
  `tb_lahir` double(8,2) NOT NULL,
  `nm_ortu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nik_ayah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tlp_ortu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemeriksaan`
--

CREATE TABLE `pemeriksaan` (
  `no_pemeriksaan` int(100) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_posyandu` int(11) NOT NULL,
  `bb_tb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bb_u` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bb_pb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tb_u` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pb_u` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pemeriksaan` date NOT NULL,
  `umur` int(11) NOT NULL,
  `asi_eks` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vit_a` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cara_ukur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posyandu`
--

CREATE TABLE `posyandu` (
  `id_posyandu` int(11) NOT NULL,
  `nm_posyandu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_posyandu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `posyandu`
--

INSERT INTO `posyandu` (`id_posyandu`, `nm_posyandu`, `password_posyandu`, `id_desa`, `created_at`, `updated_at`) VALUES
(1, 'Melati Putih', 'pos1', '021801AA', NULL, NULL),
(2, 'Mawar Merah', 'pos2', '021801AA', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rule`
--

CREATE TABLE `rule` (
  `id_rule` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_status_gizi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rule_pemeriksaan`
--

CREATE TABLE `rule_pemeriksaan` (
  `no_pemeriksaan` int(100) NOT NULL,
  `id_rule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `standar_pertumbuhan`
--

CREATE TABLE `standar_pertumbuhan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sdmintiga` double(8,2) NOT NULL,
  `sdmindua` double(8,2) NOT NULL,
  `sdminsatu` double(8,2) NOT NULL,
  `median` double(8,2) NOT NULL,
  `sdplussatu` double(8,2) NOT NULL,
  `sdplusdua` double(8,2) NOT NULL,
  `sdplustiga` double(8,2) NOT NULL,
  `tipe_tabel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `standar_pertumbuhan`
--

INSERT INTO `standar_pertumbuhan` (`id`, `parameter`, `sdmintiga`, `sdmindua`, `sdminsatu`, `median`, `sdplussatu`, `sdplusdua`, `sdplustiga`, `tipe_tabel`, `jenis_kelamin`, `created_at`, `updated_at`) VALUES
(1, '45', 1.90, 2.00, 2.20, 2.40, 2.70, 3.00, 3.30, 'BB/PB', 'laki-laki', NULL, NULL),
(2, '45.5', 1.90, 2.10, 2.30, 2.50, 2.80, 3.10, 3.40, 'BB/PB', 'laki-laki', NULL, NULL),
(3, '46', 2.00, 2.20, 2.40, 2.60, 2.90, 3.10, 3.50, 'BB/PB', 'laki-laki', NULL, NULL),
(4, '46.5', 2.10, 2.30, 2.50, 2.70, 3.00, 3.20, 3.60, 'BB/PB', 'laki-laki', NULL, NULL),
(5, '47', 2.10, 2.30, 2.50, 2.80, 3.00, 3.30, 3.70, 'BB/PB', 'laki-laki', NULL, NULL),
(6, '47.5', 2.20, 2.40, 2.60, 2.90, 3.10, 3.40, 3.80, 'BB/PB', 'laki-laki', NULL, NULL),
(7, '48', 2.30, 2.50, 2.70, 2.90, 3.20, 3.60, 3.90, 'BB/PB', 'laki-laki', NULL, NULL),
(8, '48.5', 2.30, 2.60, 2.80, 3.00, 3.30, 3.70, 4.00, 'BB/PB', 'laki-laki', NULL, NULL),
(9, '49', 2.40, 2.60, 2.90, 3.10, 3.40, 3.80, 4.20, 'BB/PB', 'laki-laki', NULL, NULL),
(10, '49.5', 2.50, 2.70, 3.00, 3.20, 3.50, 3.90, 4.30, 'BB/PB', 'laki-laki', NULL, NULL),
(11, '50', 2.60, 2.80, 3.00, 3.30, 3.60, 4.00, 4.40, 'BB/PB', 'laki-laki', NULL, NULL),
(12, '50.5', 2.70, 2.90, 3.10, 3.40, 3.80, 4.10, 4.50, 'BB/PB', 'laki-laki', NULL, NULL),
(13, '51', 2.70, 3.00, 3.20, 3.50, 3.90, 4.20, 4.70, 'BB/PB', 'laki-laki', NULL, NULL),
(14, '51.5', 2.80, 3.10, 3.30, 3.60, 4.00, 4.40, 4.80, 'BB/PB', 'laki-laki', NULL, NULL),
(15, '52', 2.90, 3.20, 3.40, 3.80, 4.10, 4.50, 5.00, 'BB/PB', 'laki-laki', NULL, NULL),
(16, '52.5', 3.00, 3.30, 3.50, 3.90, 4.20, 4.60, 5.10, 'BB/PB', 'laki-laki', NULL, NULL),
(17, '53', 3.10, 3.40, 3.60, 4.00, 4.40, 4.80, 5.30, 'BB/PB', 'laki-laki', NULL, NULL),
(18, '53.5', 3.20, 3.50, 3.70, 4.10, 4.50, 4.90, 5.40, 'BB/PB', 'laki-laki', NULL, NULL),
(19, '54', 3.30, 3.60, 3.80, 4.30, 4.70, 5.10, 5.60, 'BB/PB', 'laki-laki', NULL, NULL),
(20, '54.5', 3.40, 3.70, 4.00, 4.40, 4.80, 5.30, 5.80, 'BB/PB', 'laki-laki', NULL, NULL),
(21, '55', 3.60, 3.80, 4.20, 4.50, 5.00, 5.40, 6.00, 'BB/PB', 'laki-laki', NULL, NULL),
(22, '55.5', 3.70, 4.00, 4.30, 4.70, 5.10, 5.60, 6.10, 'BB/PB', 'laki-laki', NULL, NULL),
(23, '56', 3.80, 4.10, 4.40, 4.80, 5.30, 5.80, 6.30, 'BB/PB', 'laki-laki', NULL, NULL),
(24, '56.5', 3.90, 4.20, 4.60, 5.00, 5.40, 5.90, 6.50, 'BB/PB', 'laki-laki', NULL, NULL),
(25, '57', 4.00, 4.30, 4.70, 5.10, 5.60, 6.10, 6.70, 'BB/PB', 'laki-laki', NULL, NULL),
(26, '57.5', 4.10, 4.50, 4.90, 5.30, 5.70, 6.30, 6.90, 'BB/PB', 'laki-laki', NULL, NULL),
(27, '58', 4.30, 4.60, 5.00, 5.40, 5.90, 6.40, 7.10, 'BB/PB', 'laki-laki', NULL, NULL),
(28, '58.5', 4.40, 4.70, 5.10, 5.60, 6.10, 6.60, 7.20, 'BB/PB', 'laki-laki', NULL, NULL),
(29, '59', 4.50, 4.80, 5.30, 5.70, 6.20, 6.80, 7.40, 'BB/PB', 'laki-laki', NULL, NULL),
(30, '59.5', 4.60, 5.00, 5.40, 5.90, 6.40, 7.00, 7.60, 'BB/PB', 'laki-laki', NULL, NULL),
(31, '60', 4.70, 5.10, 5.50, 6.00, 6.50, 7.10, 7.80, 'BB/PB', 'laki-laki', NULL, NULL),
(32, '60.5', 4.80, 5.20, 5.60, 6.10, 6.70, 7.30, 8.00, 'BB/PB', 'laki-laki', NULL, NULL),
(33, '61', 4.90, 5.30, 5.80, 6.30, 6.80, 7.40, 8.10, 'BB/PB', 'laki-laki', NULL, NULL),
(34, '61.5', 5.00, 5.40, 5.90, 6.40, 7.00, 7.60, 8.30, 'BB/PB', 'laki-laki', NULL, NULL),
(35, '62', 5.10, 5.60, 6.00, 6.50, 7.10, 7.70, 8.50, 'BB/PB', 'laki-laki', NULL, NULL),
(36, '62.5', 5.20, 5.70, 6.10, 6.70, 7.20, 7.90, 8.60, 'BB/PB', 'laki-laki', NULL, NULL),
(37, '63', 5.30, 5.80, 6.20, 6.80, 7.40, 8.00, 8.80, 'BB/PB', 'laki-laki', NULL, NULL),
(38, '63.5', 5.40, 5.90, 6.40, 6.90, 7.50, 8.20, 8.90, 'BB/PB', 'laki-laki', NULL, NULL),
(39, '64', 5.50, 6.00, 6.50, 7.00, 7.60, 8.30, 9.10, 'BB/PB', 'laki-laki', NULL, NULL),
(40, '64.5', 5.60, 6.10, 6.60, 7.10, 7.80, 8.50, 9.30, 'BB/PB', 'laki-laki', NULL, NULL),
(41, '65', 5.70, 6.20, 6.70, 7.30, 7.90, 8.60, 9.40, 'BB/PB', 'laki-laki', NULL, NULL),
(42, '65.5', 5.80, 6.30, 6.80, 7.40, 8.00, 8.70, 9.60, 'BB/PB', 'laki-laki', NULL, NULL),
(43, '66', 5.90, 6.40, 6.90, 7.50, 8.20, 8.90, 9.70, 'BB/PB', 'laki-laki', NULL, NULL),
(44, '66.5', 6.00, 6.50, 7.00, 7.60, 8.30, 9.00, 9.90, 'BB/PB', 'laki-laki', NULL, NULL),
(45, '67', 6.10, 6.60, 7.10, 7.70, 8.40, 9.20, 10.00, 'BB/PB', 'laki-laki', NULL, NULL),
(46, '67.5', 6.20, 6.70, 7.20, 7.90, 8.50, 9.30, 10.20, 'BB/PB', 'laki-laki', NULL, NULL),
(47, '68', 6.30, 6.80, 7.30, 8.00, 8.70, 9.40, 10.30, 'BB/PB', 'laki-laki', NULL, NULL),
(48, '68.5', 6.40, 6.90, 7.50, 8.10, 8.80, 9.60, 10.50, 'BB/PB', 'laki-laki', NULL, NULL),
(49, '69', 6.50, 7.00, 7.60, 8.20, 8.90, 9.70, 10.60, 'BB/PB', 'laki-laki', NULL, NULL),
(50, '69.5', 6.60, 7.10, 7.70, 8.30, 9.00, 9.80, 10.80, 'BB/PB', 'laki-laki', NULL, NULL),
(51, '70', 6.60, 7.20, 7.80, 8.40, 9.20, 10.00, 10.90, 'BB/PB', 'laki-laki', NULL, NULL),
(52, '70.5', 6.70, 7.30, 7.90, 8.50, 9.30, 10.10, 11.10, 'BB/PB', 'laki-laki', NULL, NULL),
(53, '71', 6.80, 7.40, 8.00, 8.60, 9.40, 10.20, 11.20, 'BB/PB', 'laki-laki', NULL, NULL),
(54, '71.5', 6.90, 7.50, 8.10, 8.80, 9.50, 10.40, 11.30, 'BB/PB', 'laki-laki', NULL, NULL),
(55, '72', 7.00, 7.60, 8.20, 8.90, 9.60, 10.50, 11.50, 'BB/PB', 'laki-laki', NULL, NULL),
(56, '72.5', 7.10, 7.60, 8.30, 9.00, 9.80, 10.60, 11.60, 'BB/PB', 'laki-laki', NULL, NULL),
(57, '73', 7.20, 7.70, 8.40, 9.10, 9.90, 10.80, 11.80, 'BB/PB', 'laki-laki', NULL, NULL),
(58, '73.5', 7.20, 7.80, 8.50, 9.20, 10.00, 10.90, 11.90, 'BB/PB', 'laki-laki', NULL, NULL),
(59, '74', 7.30, 7.90, 8.60, 9.30, 10.10, 11.00, 12.10, 'BB/PB', 'laki-laki', NULL, NULL),
(60, '74.5', 7.40, 8.00, 8.70, 9.40, 10.20, 11.20, 12.20, 'BB/PB', 'laki-laki', NULL, NULL),
(61, '75', 7.50, 8.10, 8.80, 9.50, 10.30, 11.30, 12.30, 'BB/PB', 'laki-laki', NULL, NULL),
(62, '75.5', 7.60, 8.20, 8.80, 9.60, 10.40, 11.40, 12.50, 'BB/PB', 'laki-laki', NULL, NULL),
(63, '76', 7.60, 8.30, 8.90, 9.70, 10.60, 11.50, 12.60, 'BB/PB', 'laki-laki', NULL, NULL),
(64, '76.5', 7.70, 8.30, 9.00, 9.80, 10.70, 11.60, 12.70, 'BB/PB', 'laki-laki', NULL, NULL),
(65, '77', 7.80, 8.40, 9.10, 9.90, 10.80, 11.70, 12.80, 'BB/PB', 'laki-laki', NULL, NULL),
(66, '77.5', 7.90, 8.50, 9.20, 10.00, 10.90, 11.90, 13.00, 'BB/PB', 'laki-laki', NULL, NULL),
(67, '78', 7.90, 8.60, 9.30, 10.10, 11.00, 12.00, 13.10, 'BB/PB', 'laki-laki', NULL, NULL),
(68, '78.5', 8.00, 8.70, 9.40, 10.20, 11.10, 12.10, 13.20, 'BB/PB', 'laki-laki', NULL, NULL),
(69, '79', 8.10, 8.70, 9.50, 10.30, 11.20, 12.20, 13.30, 'BB/PB', 'laki-laki', NULL, NULL),
(70, '79.5', 8.20, 8.80, 9.50, 10.40, 11.30, 12.30, 13.40, 'BB/PB', 'laki-laki', NULL, NULL),
(71, '80', 8.20, 8.90, 9.60, 10.40, 11.40, 12.40, 13.60, 'BB/PB', 'laki-laki', NULL, NULL),
(72, '80.5', 8.30, 9.00, 9.70, 10.50, 11.50, 12.50, 13.70, 'BB/PB', 'laki-laki', NULL, NULL),
(73, '81', 8.40, 9.10, 9.80, 10.60, 11.60, 12.60, 13.80, 'BB/PB', 'laki-laki', NULL, NULL),
(74, '81.5', 8.50, 9.10, 9.90, 10.70, 11.70, 12.70, 13.90, 'BB/PB', 'laki-laki', NULL, NULL),
(75, '82', 8.50, 9.20, 10.00, 10.80, 11.80, 12.80, 14.00, 'BB/PB', 'laki-laki', NULL, NULL),
(76, '82.5', 8.60, 9.30, 10.10, 10.90, 11.90, 13.00, 14.20, 'BB/PB', 'laki-laki', NULL, NULL),
(77, '83', 8.70, 9.40, 10.20, 11.00, 12.00, 13.10, 14.30, 'BB/PB', 'laki-laki', NULL, NULL),
(78, '83.5', 8.80, 9.50, 10.30, 11.20, 12.10, 13.20, 14.40, 'BB/PB', 'laki-laki', NULL, NULL),
(79, '84', 8.90, 9.60, 10.40, 11.30, 12.20, 13.30, 14.60, 'BB/PB', 'laki-laki', NULL, NULL),
(80, '84.5', 9.00, 9.70, 10.50, 11.40, 12.40, 13.50, 14.70, 'BB/PB', 'laki-laki', NULL, NULL),
(81, '85', 9.10, 9.80, 10.60, 11.50, 12.50, 13.60, 14.90, 'BB/PB', 'laki-laki', NULL, NULL),
(82, '85.5', 9.20, 9.90, 10.70, 11.60, 12.60, 13.70, 15.00, 'BB/PB', 'laki-laki', NULL, NULL),
(83, '86', 9.30, 10.00, 10.80, 11.70, 12.80, 13.90, 15.20, 'BB/PB', 'laki-laki', NULL, NULL),
(84, '86.5', 9.40, 10.10, 11.00, 11.90, 12.90, 14.00, 15.30, 'BB/PB', 'laki-laki', NULL, NULL),
(85, '87', 9.50, 10.20, 11.10, 12.00, 13.00, 14.20, 15.50, 'BB/PB', 'laki-laki', NULL, NULL),
(86, '87.5', 9.60, 10.40, 11.20, 12.10, 13.20, 14.30, 15.60, 'BB/PB', 'laki-laki', NULL, NULL),
(87, '88', 9.70, 10.50, 11.30, 12.20, 13.30, 14.50, 15.80, 'BB/PB', 'laki-laki', NULL, NULL),
(88, '88.5', 9.80, 10.60, 11.40, 12.40, 13.40, 14.60, 15.90, 'BB/PB', 'laki-laki', NULL, NULL),
(89, '89', 9.90, 10.70, 11.50, 12.50, 13.50, 14.70, 16.10, 'BB/PB', 'laki-laki', NULL, NULL),
(90, '89.5', 10.00, 10.80, 11.60, 12.60, 13.70, 14.90, 16.20, 'BB/PB', 'laki-laki', NULL, NULL),
(91, '90', 10.10, 10.90, 11.70, 12.70, 13.80, 15.00, 16.40, 'BB/PB', 'laki-laki', NULL, NULL),
(92, '90.5', 10.20, 11.00, 11.80, 12.80, 13.90, 15.10, 16.50, 'BB/PB', 'laki-laki', NULL, NULL),
(93, '91', 10.30, 11.10, 12.00, 13.00, 14.10, 15.30, 16.70, 'BB/PB', 'laki-laki', NULL, NULL),
(94, '91.5', 10.40, 11.20, 12.10, 13.10, 14.20, 15.40, 16.80, 'BB/PB', 'laki-laki', NULL, NULL),
(95, '92', 10.50, 11.30, 12.20, 13.20, 14.30, 15.60, 17.00, 'BB/PB', 'laki-laki', NULL, NULL),
(96, '92.5', 10.60, 11.40, 12.30, 13.30, 14.40, 15.70, 17.10, 'BB/PB', 'laki-laki', NULL, NULL),
(97, '93', 10.70, 11.50, 12.40, 13.40, 14.60, 15.80, 17.30, 'BB/PB', 'laki-laki', NULL, NULL),
(98, '93.5', 10.70, 11.60, 12.50, 13.50, 14.70, 16.00, 17.40, 'BB/PB', 'laki-laki', NULL, NULL),
(99, '94', 10.80, 11.70, 12.60, 13.70, 14.80, 16.10, 17.60, 'BB/PB', 'laki-laki', NULL, NULL),
(100, '94.5', 10.90, 11.80, 12.70, 13.80, 14.90, 16.30, 17.70, 'BB/PB', 'laki-laki', NULL, NULL),
(101, '95', 11.00, 11.90, 12.80, 13.90, 15.10, 16.40, 17.90, 'BB/PB', 'laki-laki', NULL, NULL),
(102, '95.5', 11.10, 12.00, 12.90, 14.00, 15.20, 16.50, 18.00, 'BB/PB', 'laki-laki', NULL, NULL),
(103, '96', 11.20, 12.10, 13.10, 14.10, 15.30, 16.70, 18.20, 'BB/PB', 'laki-laki', NULL, NULL),
(104, '96.5', 11.30, 12.20, 13.20, 14.30, 15.50, 16.80, 18.40, 'BB/PB', 'laki-laki', NULL, NULL),
(105, '97', 11.40, 12.30, 13.30, 14.40, 15.60, 17.00, 18.50, 'BB/PB', 'laki-laki', NULL, NULL),
(106, '97.5', 11.50, 12.40, 13.40, 14.50, 15.70, 17.10, 18.70, 'BB/PB', 'laki-laki', NULL, NULL),
(107, '98', 11.60, 12.50, 13.50, 14.60, 15.90, 17.30, 18.90, 'BB/PB', 'laki-laki', NULL, NULL),
(108, '98.5', 11.70, 12.60, 13.60, 14.80, 16.00, 17.50, 19.10, 'BB/PB', 'laki-laki', NULL, NULL),
(109, '99', 11.80, 12.70, 13.70, 14.90, 16.20, 17.60, 19.20, 'BB/PB', 'laki-laki', NULL, NULL),
(110, '99.5', 11.90, 12.80, 13.90, 15.00, 16.30, 17.80, 19.40, 'BB/PB', 'laki-laki', NULL, NULL),
(111, '100', 12.00, 12.90, 14.00, 15.20, 16.50, 18.00, 19.60, 'BB/PB', 'laki-laki', NULL, NULL),
(112, '100.5', 12.10, 13.00, 14.10, 15.30, 16.60, 18.10, 19.80, 'BB/PB', 'laki-laki', NULL, NULL),
(113, '101', 12.20, 13.20, 14.20, 15.40, 16.80, 18.30, 20.00, 'BB/PB', 'laki-laki', NULL, NULL),
(114, '101.5', 12.30, 13.30, 14.40, 15.60, 16.90, 18.50, 20.20, 'BB/PB', 'laki-laki', NULL, NULL),
(115, '102', 12.40, 13.40, 14.50, 15.70, 17.10, 18.70, 20.40, 'BB/PB', 'laki-laki', NULL, NULL),
(116, '102.5', 12.50, 13.50, 14.60, 15.90, 17.30, 18.80, 20.60, 'BB/PB', 'laki-laki', NULL, NULL),
(117, '103', 12.60, 13.60, 14.80, 16.00, 17.40, 19.00, 20.80, 'BB/PB', 'laki-laki', NULL, NULL),
(118, '103.5', 12.70, 13.70, 14.90, 16.20, 17.60, 19.20, 21.00, 'BB/PB', 'laki-laki', NULL, NULL),
(119, '104', 12.80, 13.90, 15.00, 16.30, 17.80, 19.40, 21.20, 'BB/PB', 'laki-laki', NULL, NULL),
(120, '104.5', 12.90, 14.00, 15.20, 16.50, 17.90, 19.60, 21.50, 'BB/PB', 'laki-laki', NULL, NULL),
(121, '105', 13.00, 14.10, 15.30, 16.60, 18.10, 19.80, 21.70, 'BB/PB', 'laki-laki', NULL, NULL),
(122, '105.5', 13.20, 14.20, 15.40, 16.80, 18.30, 20.00, 21.90, 'BB/PB', 'laki-laki', NULL, NULL),
(123, '106', 13.30, 14.40, 15.60, 16.90, 18.50, 20.20, 22.10, 'BB/PB', 'laki-laki', NULL, NULL),
(124, '106.5', 13.40, 14.50, 15.70, 17.10, 18.60, 20.40, 22.40, 'BB/PB', 'laki-laki', NULL, NULL),
(125, '107', 13.50, 14.60, 15.90, 17.30, 18.80, 20.60, 22.60, 'BB/PB', 'laki-laki', NULL, NULL),
(126, '107.5', 13.60, 14.70, 16.00, 17.40, 19.00, 20.80, 22.80, 'BB/PB', 'laki-laki', NULL, NULL),
(127, '108', 13.70, 14.90, 16.20, 17.60, 19.20, 21.00, 23.10, 'BB/PB', 'laki-laki', NULL, NULL),
(128, '108.5', 13.80, 15.00, 16.30, 17.80, 19.40, 21.20, 23.30, 'BB/PB', 'laki-laki', NULL, NULL),
(129, '109', 14.00, 15.10, 16.50, 17.90, 19.60, 21.40, 23.60, 'BB/PB', 'laki-laki', NULL, NULL),
(130, '109.5', 14.10, 15.30, 16.60, 18.10, 19.80, 21.70, 23.80, 'BB/PB', 'laki-laki', NULL, NULL),
(131, '110', 14.20, 15.40, 16.80, 18.30, 20.00, 21.90, 24.10, 'BB/PB', 'laki-laki', NULL, NULL),
(132, '45', 1.90, 2.10, 2.30, 2.50, 2.70, 3.00, 3.30, 'BB/PB', 'perempuan', NULL, NULL),
(133, '45.5', 2.00, 2.10, 2.30, 2.50, 2.80, 3.10, 3.40, 'BB/PB', 'perempuan', NULL, NULL),
(134, '46', 2.00, 2.20, 2.40, 2.60, 2.90, 3.20, 3.50, 'BB/PB', 'perempuan', NULL, NULL),
(135, '46.5', 2.10, 2.30, 2.50, 2.70, 3.00, 3.30, 3.60, 'BB/PB', 'perempuan', NULL, NULL),
(136, '47', 2.20, 2.40, 2.60, 2.80, 3.10, 3.40, 3.70, 'BB/PB', 'perempuan', NULL, NULL),
(137, '47.5', 2.20, 2.40, 2.60, 2.90, 3.20, 3.50, 3.80, 'BB/PB', 'perempuan', NULL, NULL),
(138, '48', 2.30, 2.50, 2.70, 3.00, 3.30, 3.60, 4.00, 'BB/PB', 'perempuan', NULL, NULL),
(139, '48.5', 2.40, 2.60, 2.80, 3.10, 3.40, 3.70, 4.10, 'BB/PB', 'perempuan', NULL, NULL),
(140, '49', 2.40, 2.60, 2.90, 3.20, 3.50, 3.80, 4.20, 'BB/PB', 'perempuan', NULL, NULL),
(141, '49.5', 2.50, 2.70, 3.00, 3.30, 3.60, 3.90, 4.30, 'BB/PB', 'perempuan', NULL, NULL),
(142, '50', 2.60, 2.80, 3.10, 3.40, 3.70, 4.00, 4.50, 'BB/PB', 'perempuan', NULL, NULL),
(143, '50.5', 2.70, 2.90, 3.20, 3.50, 3.80, 4.20, 4.60, 'BB/PB', 'perempuan', NULL, NULL),
(144, '51', 2.80, 3.00, 3.30, 3.60, 3.90, 4.30, 4.80, 'BB/PB', 'perempuan', NULL, NULL),
(145, '51.5', 2.80, 3.10, 3.40, 3.70, 4.00, 4.40, 4.90, 'BB/PB', 'perempuan', NULL, NULL),
(146, '52', 2.90, 3.20, 3.50, 3.80, 4.20, 4.60, 5.10, 'BB/PB', 'perempuan', NULL, NULL),
(147, '52.5', 3.00, 3.30, 3.60, 3.90, 4.30, 4.70, 5.20, 'BB/PB', 'perempuan', NULL, NULL),
(148, '53', 3.10, 3.40, 3.70, 4.00, 4.40, 4.90, 5.40, 'BB/PB', 'perempuan', NULL, NULL),
(149, '53.5', 3.20, 3.50, 3.80, 4.20, 4.60, 5.00, 5.50, 'BB/PB', 'perempuan', NULL, NULL),
(150, '54', 3.30, 3.60, 3.90, 4.30, 4.70, 5.20, 5.70, 'BB/PB', 'perempuan', NULL, NULL),
(151, '54.5', 3.40, 3.70, 4.00, 4.40, 4.80, 5.30, 5.90, 'BB/PB', 'perempuan', NULL, NULL),
(152, '55', 3.50, 3.80, 4.20, 4.50, 5.00, 5.50, 6.10, 'BB/PB', 'perempuan', NULL, NULL),
(153, '55.5', 3.60, 3.90, 4.30, 4.70, 5.10, 5.70, 6.30, 'BB/PB', 'perempuan', NULL, NULL),
(154, '56', 3.70, 4.00, 4.40, 4.80, 5.30, 5.80, 6.40, 'BB/PB', 'perempuan', NULL, NULL),
(155, '56.5', 3.80, 4.10, 4.50, 5.00, 5.40, 6.00, 6.60, 'BB/PB', 'perempuan', NULL, NULL),
(156, '57', 3.90, 4.30, 4.60, 5.10, 5.60, 6.10, 6.80, 'BB/PB', 'perempuan', NULL, NULL),
(157, '57.5', 4.00, 4.40, 4.80, 5.20, 5.70, 6.30, 7.00, 'BB/PB', 'perempuan', NULL, NULL),
(158, '58', 4.10, 4.50, 4.90, 5.40, 5.90, 6.50, 7.10, 'BB/PB', 'perempuan', NULL, NULL),
(159, '58.5', 4.20, 4.60, 4.00, 5.50, 6.00, 6.60, 7.30, 'BB/PB', 'perempuan', NULL, NULL),
(160, '59', 4.30, 4.70, 5.10, 5.60, 6.20, 6.80, 7.50, 'BB/PB', 'perempuan', NULL, NULL),
(161, '59.5', 4.40, 4.80, 5.30, 5.70, 6.30, 6.90, 7.70, 'BB/PB', 'perempuan', NULL, NULL),
(162, '60', 4.50, 4.90, 5.40, 5.90, 6.40, 7.10, 7.80, 'BB/PB', 'perempuan', NULL, NULL),
(163, '60.5', 4.60, 5.00, 5.50, 6.00, 6.60, 7.30, 8.00, 'BB/PB', 'perempuan', NULL, NULL),
(164, '61', 4.70, 5.10, 5.60, 6.10, 6.70, 7.30, 8.20, 'BB/PB', 'perempuan', NULL, NULL),
(165, '61.5', 4.80, 5.20, 5.70, 6.30, 6.90, 7.60, 8.40, 'BB/PB', 'perempuan', NULL, NULL),
(166, '62', 4.90, 5.30, 5.80, 6.40, 7.00, 7.70, 8.50, 'BB/PB', 'perempuan', NULL, NULL),
(167, '62.5', 5.00, 5.40, 5.90, 6.50, 7.10, 7.80, 8.70, 'BB/PB', 'perempuan', NULL, NULL),
(168, '63', 5.10, 5.50, 6.00, 6.60, 7.30, 8.00, 8.80, 'BB/PB', 'perempuan', NULL, NULL),
(169, '63.5', 5.20, 5.60, 6.20, 6.70, 7.40, 8.10, 9.00, 'BB/PB', 'perempuan', NULL, NULL),
(170, '64', 5.30, 5.70, 6.30, 6.90, 7.50, 8.30, 9.10, 'BB/PB', 'perempuan', NULL, NULL),
(171, '64.5', 5.40, 5.80, 6.40, 7.00, 7.60, 8.40, 9.30, 'BB/PB', 'perempuan', NULL, NULL),
(172, '65', 5.50, 5.90, 6.50, 7.10, 7.80, 8.60, 9.50, 'BB/PB', 'perempuan', NULL, NULL),
(173, '65.5', 5.50, 6.00, 6.60, 7.20, 7.90, 8.70, 9.60, 'BB/PB', 'perempuan', NULL, NULL),
(174, '66', 5.60, 6.10, 6.70, 7.30, 8.00, 8.80, 9.80, 'BB/PB', 'perempuan', NULL, NULL),
(175, '66.5', 5.70, 6.20, 6.80, 7.40, 8.10, 9.00, 9.90, 'BB/PB', 'perempuan', NULL, NULL),
(176, '67', 5.80, 6.30, 6.90, 7.50, 8.30, 9.10, 10.00, 'BB/PB', 'perempuan', NULL, NULL),
(177, '67.5', 5.90, 6.40, 7.00, 7.60, 8.40, 9.20, 10.20, 'BB/PB', 'perempuan', NULL, NULL),
(178, '68', 6.00, 6.50, 7.10, 7.70, 8.50, 9.40, 10.30, 'BB/PB', 'perempuan', NULL, NULL),
(179, '68.5', 6.10, 6.60, 7.20, 7.90, 8.60, 9.50, 10.50, 'BB/PB', 'perempuan', NULL, NULL),
(180, '69', 6.10, 6.70, 7.30, 8.00, 8.70, 9.60, 10.60, 'BB/PB', 'perempuan', NULL, NULL),
(181, '69.5', 6.20, 6.80, 7.40, 8.10, 8.80, 9.70, 10.70, 'BB/PB', 'perempuan', NULL, NULL),
(182, '70', 6.30, 6.90, 7.50, 8.20, 9.00, 9.90, 10.90, 'BB/PB', 'perempuan', NULL, NULL),
(183, '70.5', 6.40, 6.90, 7.60, 8.30, 9.10, 10.00, 11.00, 'BB/PB', 'perempuan', NULL, NULL),
(184, '71', 6.50, 7.00, 7.70, 8.40, 9.20, 10.10, 11.10, 'BB/PB', 'perempuan', NULL, NULL),
(185, '71.5', 6.50, 7.10, 7.70, 8.50, 9.30, 10.20, 11.30, 'BB/PB', 'perempuan', NULL, NULL),
(186, '72', 6.60, 7.20, 7.80, 8.60, 9.40, 10.30, 11.40, 'BB/PB', 'perempuan', NULL, NULL),
(187, '72.5', 6.70, 7.30, 7.90, 8.70, 9.50, 10.50, 11.50, 'BB/PB', 'perempuan', NULL, NULL),
(188, '73', 6.80, 7.40, 8.00, 8.80, 9.60, 10.60, 11.70, 'BB/PB', 'perempuan', NULL, NULL),
(189, '73.5', 6.90, 7.40, 8.10, 8.90, 9.70, 10.70, 11.80, 'BB/PB', 'perempuan', NULL, NULL),
(190, '74', 6.90, 7.50, 8.20, 9.00, 9.80, 10.80, 11.90, 'BB/PB', 'perempuan', NULL, NULL),
(191, '74.5', 7.00, 7.60, 8.30, 9.10, 9.90, 10.90, 12.00, 'BB/PB', 'perempuan', NULL, NULL),
(192, '75', 7.10, 7.70, 8.40, 9.10, 10.00, 11.00, 12.20, 'BB/PB', 'perempuan', NULL, NULL),
(193, '75.5', 7.10, 7.80, 8.50, 9.20, 10.10, 11.10, 12.30, 'BB/PB', 'perempuan', NULL, NULL),
(194, '76', 7.20, 7.80, 8.50, 9.30, 10.20, 11.20, 12.40, 'BB/PB', 'perempuan', NULL, NULL),
(195, '76.5', 7.30, 7.90, 8.60, 9.40, 10.30, 11.40, 12.50, 'BB/PB', 'perempuan', NULL, NULL),
(196, '77', 7.40, 8.00, 8.70, 9.50, 10.40, 11.50, 12.60, 'BB/PB', 'perempuan', NULL, NULL),
(197, '77.5', 7.40, 8.10, 8.80, 9.60, 10.50, 11.60, 12.80, 'BB/PB', 'perempuan', NULL, NULL),
(198, '78', 7.50, 8.20, 8.90, 9.70, 10.60, 11.70, 12.90, 'BB/PB', 'perempuan', NULL, NULL),
(199, '78.5', 7.60, 8.20, 9.00, 9.80, 10.70, 11.80, 13.00, 'BB/PB', 'perempuan', NULL, NULL),
(200, '79', 7.70, 8.30, 9.10, 9.90, 10.80, 11.90, 13.10, 'BB/PB', 'perempuan', NULL, NULL),
(201, '79.5', 7.70, 8.40, 9.10, 10.00, 10.90, 12.00, 13.30, 'BB/PB', 'perempuan', NULL, NULL),
(202, '80', 7.80, 8.50, 9.20, 10.10, 11.00, 12.10, 13.40, 'BB/PB', 'perempuan', NULL, NULL),
(203, '80.5', 7.90, 8.60, 9.30, 10.20, 11.20, 12.30, 13.50, 'BB/PB', 'perempuan', NULL, NULL),
(204, '81', 8.00, 8.70, 9.40, 10.30, 11.30, 12.40, 13.70, 'BB/PB', 'perempuan', NULL, NULL),
(205, '81.5', 8.10, 8.80, 9.50, 10.40, 11.40, 12.50, 13.80, 'BB/PB', 'perempuan', NULL, NULL),
(206, '82', 8.10, 8.80, 9.60, 10.50, 11.50, 12.60, 13.90, 'BB/PB', 'perempuan', NULL, NULL),
(207, '82.5', 8.20, 8.90, 9.70, 10.60, 11.60, 12.80, 14.10, 'BB/PB', 'perempuan', NULL, NULL),
(208, '83', 8.30, 9.00, 9.80, 10.70, 11.80, 12.90, 14.20, 'BB/PB', 'perempuan', NULL, NULL),
(209, '83.5', 8.40, 9.10, 9.90, 10.90, 11.90, 13.10, 14.40, 'BB/PB', 'perempuan', NULL, NULL),
(210, '84', 8.50, 9.20, 10.10, 11.00, 12.00, 13.20, 14.50, 'BB/PB', 'perempuan', NULL, NULL),
(211, '84.5', 8.60, 9.30, 10.20, 11.10, 12.10, 13.30, 14.70, 'BB/PB', 'perempuan', NULL, NULL),
(212, '85', 8.70, 9.40, 10.30, 11.20, 12.30, 13.50, 14.90, 'BB/PB', 'perempuan', NULL, NULL),
(213, '85.5', 8.80, 9.50, 10.40, 11.30, 12.40, 13.60, 15.00, 'BB/PB', 'perempuan', NULL, NULL),
(214, '86', 8.90, 9.70, 10.50, 11.50, 12.60, 13.80, 15.20, 'BB/PB', 'perempuan', NULL, NULL),
(215, '86.5', 9.00, 9.80, 10.60, 11.60, 12.70, 13.90, 15.40, 'BB/PB', 'perempuan', NULL, NULL),
(216, '87', 9.10, 9.90, 10.70, 11.70, 12.80, 14.10, 15.50, 'BB/PB', 'perempuan', NULL, NULL),
(217, '87.5', 9.20, 10.00, 10.90, 11.80, 13.00, 14.20, 15.70, 'BB/PB', 'perempuan', NULL, NULL),
(218, '88', 9.30, 10.10, 11.00, 12.00, 13.10, 14.40, 15.90, 'BB/PB', 'perempuan', NULL, NULL),
(219, '88.5', 9.40, 10.20, 11.10, 12.10, 13.20, 14.50, 16.00, 'BB/PB', 'perempuan', NULL, NULL),
(220, '89', 9.50, 10.30, 11.20, 12.20, 13.40, 14.70, 16.20, 'BB/PB', 'perempuan', NULL, NULL),
(221, '89.5', 9.60, 10.40, 11.30, 12.30, 13.50, 14.80, 16.40, 'BB/PB', 'perempuan', NULL, NULL),
(222, '90', 9.70, 10.50, 11.40, 12.50, 13.70, 15.00, 16.50, 'BB/PB', 'perempuan', NULL, NULL),
(223, '90.5', 9.80, 10.60, 11.50, 12.60, 13.80, 15.10, 16.70, 'BB/PB', 'perempuan', NULL, NULL),
(224, '91', 9.90, 10.70, 11.70, 12.70, 13.90, 15.30, 16.90, 'BB/PB', 'perempuan', NULL, NULL),
(225, '91.5', 10.00, 10.80, 11.80, 12.80, 14.10, 15.50, 17.00, 'BB/PB', 'perempuan', NULL, NULL),
(226, '92', 10.10, 10.90, 11.90, 13.00, 14.20, 15.60, 17.20, 'BB/PB', 'perempuan', NULL, NULL),
(227, '92.5', 10.10, 11.00, 12.00, 13.10, 14.30, 15.80, 17.40, 'BB/PB', 'perempuan', NULL, NULL),
(228, '93', 10.20, 11.10, 12.10, 13.20, 14.50, 15.90, 17.50, 'BB/PB', 'perempuan', NULL, NULL),
(229, '93.5', 10.30, 11.20, 12.20, 13.30, 14.60, 16.10, 17.70, 'BB/PB', 'perempuan', NULL, NULL),
(230, '94', 10.40, 11.30, 12.30, 13.50, 14.70, 16.20, 17.90, 'BB/PB', 'perempuan', NULL, NULL),
(231, '94.5', 10.50, 11.40, 12.40, 13.60, 14.90, 16.40, 18.00, 'BB/PB', 'perempuan', NULL, NULL),
(232, '95', 10.60, 11.50, 12.60, 13.70, 15.00, 16.50, 18.20, 'BB/PB', 'perempuan', NULL, NULL),
(233, '95.5', 10.70, 11.60, 12.70, 13.80, 15.20, 16.70, 18.40, 'BB/PB', 'perempuan', NULL, NULL),
(234, '96', 10.80, 11.70, 12.80, 14.00, 15.30, 16.80, 18.60, 'BB/PB', 'perempuan', NULL, NULL),
(235, '96.5', 10.90, 11.80, 12.90, 14.10, 15.40, 17.00, 18.70, 'BB/PB', 'perempuan', NULL, NULL),
(236, '97', 11.00, 12.00, 13.00, 14.20, 15.60, 17.10, 18.90, 'BB/PB', 'perempuan', NULL, NULL),
(237, '97.5', 11.10, 12.10, 13.10, 14.40, 15.70, 17.30, 19.10, 'BB/PB', 'perempuan', NULL, NULL),
(238, '98', 11.20, 12.20, 13.30, 14.50, 15.90, 17.50, 19.30, 'BB/PB', 'perempuan', NULL, NULL),
(239, '98.5', 11.30, 12.30, 13.40, 14.60, 16.00, 17.60, 19.50, 'BB/PB', 'perempuan', NULL, NULL),
(240, '99', 11.40, 12.40, 13.50, 14.80, 16.20, 17.80, 19.60, 'BB/PB', 'perempuan', NULL, NULL),
(241, '99.5', 11.50, 12.50, 13.60, 14.90, 16.30, 18.00, 19.80, 'BB/PB', 'perempuan', NULL, NULL),
(242, '100', 11.60, 12.60, 13.70, 15.00, 16.50, 18.10, 20.00, 'BB/PB', 'perempuan', NULL, NULL),
(243, '100.5', 11.70, 12.70, 13.90, 15.20, 16.60, 18.30, 20.20, 'BB/PB', 'perempuan', NULL, NULL),
(244, '101', 11.80, 12.80, 14.00, 15.30, 16.80, 18.50, 20.40, 'BB/PB', 'perempuan', NULL, NULL),
(245, '101.5', 11.90, 13.00, 14.10, 15.50, 17.00, 18.70, 20.60, 'BB/PB', 'perempuan', NULL, NULL),
(246, '102', 12.00, 13.10, 14.30, 15.60, 17.10, 18.90, 20.80, 'BB/PB', 'perempuan', NULL, NULL),
(247, '102.5', 12.10, 13.20, 14.40, 15.80, 17.30, 19.00, 21.00, 'BB/PB', 'perempuan', NULL, NULL),
(248, '103', 12.30, 13.30, 14.50, 15.90, 17.50, 19.20, 21.30, 'BB/PB', 'perempuan', NULL, NULL),
(249, '103.5', 12.40, 13.50, 14.70, 16.10, 17.60, 19.50, 21.50, 'BB/PB', 'perempuan', NULL, NULL),
(250, '104', 12.50, 13.60, 14.80, 16.20, 17.80, 19.60, 21.70, 'BB/PB', 'perempuan', NULL, NULL),
(251, '104.5', 12.60, 13.70, 15.00, 16.40, 18.00, 19.80, 21.90, 'BB/PB', 'perempuan', NULL, NULL),
(252, '105', 12.70, 13.80, 15.10, 16.50, 18.20, 20.00, 22.20, 'BB/PB', 'perempuan', NULL, NULL),
(253, '105.5', 12.80, 14.00, 15.30, 16.70, 18.40, 20.20, 22.40, 'BB/PB', 'perempuan', NULL, NULL),
(254, '106', 13.00, 14.10, 15.40, 16.90, 18.50, 20.50, 22.60, 'BB/PB', 'perempuan', NULL, NULL),
(255, '106.5', 13.10, 14.30, 15.60, 17.10, 18.70, 20.70, 22.90, 'BB/PB', 'perempuan', NULL, NULL),
(256, '107', 13.20, 14.40, 15.70, 17.20, 18.90, 20.90, 23.10, 'BB/PB', 'perempuan', NULL, NULL),
(257, '107.5', 13.30, 14.50, 15.90, 17.40, 19.10, 21.10, 23.40, 'BB/PB', 'perempuan', NULL, NULL),
(258, '108', 13.50, 14.70, 16.00, 17.60, 19.30, 21.30, 23.60, 'BB/PB', 'perempuan', NULL, NULL),
(259, '108.5', 13.60, 14.80, 16.20, 17.80, 19.50, 21.60, 23.90, 'BB/PB', 'perempuan', NULL, NULL),
(260, '109', 13.70, 15.00, 16.40, 18.00, 19.70, 21.80, 24.20, 'BB/PB', 'perempuan', NULL, NULL),
(261, '109.5', 13.90, 15.10, 16.50, 18.10, 20.00, 22.00, 24.40, 'BB/PB', 'perempuan', NULL, NULL),
(262, '110', 14.00, 15.30, 16.70, 18.30, 20.20, 22.30, 24.70, 'BB/PB', 'perempuan', NULL, NULL),
(263, '0', 2.10, 2.50, 2.90, 3.30, 3.90, 4.40, 5.00, 'BB/U', 'laki-laki', NULL, NULL),
(264, '1', 2.90, 3.40, 3.90, 3.50, 5.10, 5.80, 6.60, 'BB/U', 'laki-laki', NULL, NULL),
(265, '2', 3.80, 4.30, 4.90, 5.60, 6.30, 7.10, 8.00, 'BB/U', 'laki-laki', NULL, NULL),
(266, '3', 4.40, 5.00, 5.70, 6.40, 7.20, 8.00, 9.00, 'BB/U', 'laki-laki', NULL, NULL),
(267, '4', 4.90, 5.60, 6.20, 7.00, 7.80, 8.70, 9.70, 'BB/U', 'laki-laki', NULL, NULL),
(268, '5', 5.30, 6.00, 6.70, 7.50, 8.40, 9.30, 10.40, 'BB/U', 'laki-laki', NULL, NULL),
(269, '6', 5.70, 6.40, 7.10, 7.90, 8.80, 9.80, 10.90, 'BB/U', 'laki-laki', NULL, NULL),
(270, '7', 5.90, 6.70, 7.40, 8.30, 9.20, 10.30, 11.40, 'BB/U', 'laki-laki', NULL, NULL),
(271, '8', 6.20, 6.90, 7.70, 8.60, 9.60, 10.70, 11.90, 'BB/U', 'laki-laki', NULL, NULL),
(272, '9', 6.40, 7.10, 8.00, 8.90, 9.90, 11.00, 12.30, 'BB/U', 'laki-laki', NULL, NULL),
(273, '10', 6.60, 7.40, 8.20, 9.20, 10.20, 11.40, 12.70, 'BB/U', 'laki-laki', NULL, NULL),
(274, '11', 6.80, 7.60, 8.40, 9.40, 10.50, 11.70, 13.00, 'BB/U', 'laki-laki', NULL, NULL),
(275, '12', 6.90, 7.70, 8.60, 9.60, 10.80, 12.00, 13.30, 'BB/U', 'laki-laki', NULL, NULL),
(276, '13', 7.10, 7.90, 8.80, 9.90, 11.00, 12.30, 13.70, 'BB/U', 'laki-laki', NULL, NULL),
(277, '14', 7.20, 8.10, 9.00, 10.10, 11.30, 12.60, 14.00, 'BB/U', 'laki-laki', NULL, NULL),
(278, '15', 7.40, 8.30, 9.20, 10.30, 11.50, 12.80, 14.30, 'BB/U', 'laki-laki', NULL, NULL),
(279, '16', 7.50, 8.40, 9.40, 10.50, 11.70, 13.10, 14.60, 'BB/U', 'laki-laki', NULL, NULL),
(280, '17', 7.70, 8.60, 9.60, 10.70, 12.00, 13.40, 14.90, 'BB/U', 'laki-laki', NULL, NULL),
(281, '18', 7.80, 8.80, 9.80, 10.90, 12.20, 13.70, 15.30, 'BB/U', 'laki-laki', NULL, NULL),
(282, '19', 8.00, 8.90, 10.00, 11.10, 12.50, 13.90, 15.60, 'BB/U', 'laki-laki', NULL, NULL),
(283, '20', 8.10, 9.10, 10.10, 11.30, 12.70, 14.20, 15.90, 'BB/U', 'laki-laki', NULL, NULL),
(284, '21', 8.20, 9.20, 10.30, 11.50, 12.90, 14.50, 16.20, 'BB/U', 'laki-laki', NULL, NULL),
(285, '22', 8.40, 9.40, 10.50, 11.80, 13.20, 14.70, 16.50, 'BB/U', 'laki-laki', NULL, NULL),
(286, '23', 8.50, 9.50, 10.70, 12.00, 13.40, 15.00, 16.80, 'BB/U', 'laki-laki', NULL, NULL),
(287, '24', 8.60, 9.70, 10.80, 12.20, 13.60, 15.30, 17.10, 'BB/U', 'laki-laki', NULL, NULL),
(288, '25', 8.80, 9.80, 11.00, 12.40, 13.90, 15.50, 17.50, 'BB/U', 'laki-laki', NULL, NULL),
(289, '26', 8.90, 10.00, 11.20, 12.50, 14.10, 15.80, 17.80, 'BB/U', 'laki-laki', NULL, NULL),
(290, '27', 9.00, 10.10, 11.30, 12.70, 14.30, 16.10, 18.10, 'BB/U', 'laki-laki', NULL, NULL),
(291, '28', 9.10, 10.20, 11.50, 12.90, 14.50, 16.30, 18.40, 'BB/U', 'laki-laki', NULL, NULL),
(292, '29', 9.20, 10.40, 11.70, 13.10, 14.80, 16.60, 18.70, 'BB/U', 'laki-laki', NULL, NULL),
(293, '30', 9.40, 10.50, 11.80, 13.30, 15.00, 16.90, 19.00, 'BB/U', 'laki-laki', NULL, NULL),
(294, '31', 9.50, 10.70, 12.00, 13.50, 15.20, 17.10, 19.30, 'BB/U', 'laki-laki', NULL, NULL),
(295, '32', 9.60, 10.80, 12.10, 13.70, 15.40, 17.40, 19.60, 'BB/U', 'laki-laki', NULL, NULL),
(296, '33', 9.70, 10.90, 12.30, 13.80, 15.60, 17.60, 19.90, 'BB/U', 'laki-laki', NULL, NULL),
(297, '34', 9.80, 11.00, 12.40, 14.00, 15.80, 17.80, 20.20, 'BB/U', 'laki-laki', NULL, NULL),
(298, '35', 9.90, 11.20, 12.60, 14.20, 16.00, 18.10, 20.40, 'BB/U', 'laki-laki', NULL, NULL),
(299, '36', 10.00, 11.30, 12.70, 14.30, 16.20, 18.30, 20.70, 'BB/U', 'laki-laki', NULL, NULL),
(300, '37', 10.10, 11.40, 12.90, 14.50, 16.40, 18.60, 21.00, 'BB/U', 'laki-laki', NULL, NULL),
(301, '38', 10.20, 11.50, 13.00, 14.70, 16.60, 18.80, 21.30, 'BB/U', 'laki-laki', NULL, NULL),
(302, '39', 10.30, 11.60, 13.10, 14.80, 16.80, 19.00, 21.60, 'BB/U', 'laki-laki', NULL, NULL),
(303, '40', 10.40, 11.80, 13.30, 15.00, 17.00, 19.30, 21.90, 'BB/U', 'laki-laki', NULL, NULL),
(304, '41', 10.50, 11.90, 13.40, 15.20, 17.20, 19.50, 22.10, 'BB/U', 'laki-laki', NULL, NULL),
(305, '42', 10.60, 12.00, 13.60, 15.30, 17.40, 19.70, 22.40, 'BB/U', 'laki-laki', NULL, NULL),
(306, '43', 10.70, 12.10, 13.70, 15.50, 17.60, 20.00, 22.70, 'BB/U', 'laki-laki', NULL, NULL),
(307, '44', 10.80, 12.20, 13.80, 15.70, 17.80, 20.20, 23.00, 'BB/U', 'laki-laki', NULL, NULL),
(308, '45', 10.90, 12.40, 14.00, 15.80, 18.00, 20.50, 23.30, 'BB/U', 'laki-laki', NULL, NULL),
(309, '46', 11.00, 12.50, 14.10, 16.00, 18.20, 20.70, 23.60, 'BB/U', 'laki-laki', NULL, NULL),
(310, '47', 11.10, 12.60, 14.30, 16.20, 18.40, 20.90, 23.90, 'BB/U', 'laki-laki', NULL, NULL),
(311, '48', 11.20, 12.70, 14.40, 16.30, 18.60, 21.20, 24.20, 'BB/U', 'laki-laki', NULL, NULL),
(312, '49', 11.30, 12.80, 14.50, 16.50, 18.80, 21.40, 24.50, 'BB/U', 'laki-laki', NULL, NULL),
(313, '50', 11.40, 12.90, 14.70, 16.70, 19.00, 21.70, 24.80, 'BB/U', 'laki-laki', NULL, NULL),
(314, '51', 11.50, 13.10, 14.80, 16.80, 19.20, 21.90, 25.10, 'BB/U', 'laki-laki', NULL, NULL),
(315, '52', 11.60, 13.20, 15.00, 17.00, 19.40, 22.20, 25.40, 'BB/U', 'laki-laki', NULL, NULL),
(316, '53', 11.70, 13.30, 15.10, 17.20, 19.60, 22.40, 25.70, 'BB/U', 'laki-laki', NULL, NULL),
(317, '54', 11.80, 13.40, 15.20, 17.30, 19.80, 22.70, 26.00, 'BB/U', 'laki-laki', NULL, NULL),
(318, '55', 11.90, 13.50, 15.40, 17.50, 20.00, 22.90, 26.30, 'BB/U', 'laki-laki', NULL, NULL),
(319, '56', 12.00, 13.60, 15.50, 17.70, 20.20, 23.20, 26.60, 'BB/U', 'laki-laki', NULL, NULL),
(320, '57', 12.10, 13.70, 15.60, 17.80, 20.40, 23.40, 26.90, 'BB/U', 'laki-laki', NULL, NULL),
(321, '58', 12.20, 13.80, 15.80, 18.00, 20.60, 23.70, 27.20, 'BB/U', 'laki-laki', NULL, NULL),
(322, '59', 12.30, 14.00, 15.90, 18.20, 20.80, 23.90, 27.60, 'BB/U', 'laki-laki', NULL, NULL),
(323, '60', 12.40, 14.10, 16.00, 18.30, 21.00, 24.20, 27.90, 'BB/U', 'laki-laki', NULL, NULL),
(324, '0', 2.00, 2.40, 2.80, 3.20, 3.70, 4.20, 4.80, 'BB/U', 'perempuan', NULL, NULL),
(325, '1', 2.70, 3.20, 3.60, 4.20, 4.80, 5.50, 6.20, 'BB/U', 'perempuan', NULL, NULL),
(326, '2', 3.40, 3.90, 4.50, 5.10, 5.80, 0.50, 7.50, 'BB/U', 'perempuan', NULL, NULL),
(327, '3', 4.00, 4.50, 5.20, 5.80, 6.60, 7.50, 8.50, 'BB/U', 'perempuan', NULL, NULL),
(328, '4', 4.40, 5.00, 5.70, 6.40, 7.30, 8.20, 9.30, 'BB/U', 'perempuan', NULL, NULL),
(329, '5', 4.80, 5.40, 6.10, 6.90, 7.80, 8.80, 10.00, 'BB/U', 'perempuan', NULL, NULL),
(330, '6', 5.10, 5.70, 6.50, 7.30, 8.20, 9.30, 10.60, 'BB/U', 'perempuan', NULL, NULL),
(331, '7', 5.30, 6.00, 6.80, 7.60, 8.60, 9.80, 11.10, 'BB/U', 'perempuan', NULL, NULL),
(332, '8', 5.60, 6.30, 7.00, 7.90, 9.00, 10.20, 11.60, 'BB/U', 'perempuan', NULL, NULL),
(333, '9', 5.80, 6.50, 7.30, 8.20, 9.30, 10.50, 12.00, 'BB/U', 'perempuan', NULL, NULL),
(334, '10', 5.90, 6.70, 7.50, 8.50, 9.60, 10.90, 12.40, 'BB/U', 'perempuan', NULL, NULL),
(335, '11', 6.10, 6.90, 7.70, 8.70, 9.90, 11.20, 12.80, 'BB/U', 'perempuan', NULL, NULL),
(336, '12', 6.30, 7.00, 7.90, 8.90, 10.10, 11.50, 13.10, 'BB/U', 'perempuan', NULL, NULL),
(337, '13', 6.40, 7.20, 8.10, 9.20, 10.40, 11.80, 13.50, 'BB/U', 'perempuan', NULL, NULL),
(338, '14', 6.60, 7.40, 8.30, 9.40, 10.60, 12.10, 13.80, 'BB/U', 'perempuan', NULL, NULL),
(339, '15', 6.70, 7.60, 8.50, 9.60, 10.90, 12.30, 14.10, 'BB/U', 'perempuan', NULL, NULL),
(340, '16', 6.90, 7.70, 8.70, 9.80, 11.10, 12.50, 14.50, 'BB/U', 'perempuan', NULL, NULL),
(341, '17', 7.00, 7.90, 8.90, 10.00, 11.40, 12.80, 14.80, 'BB/U', 'perempuan', NULL, NULL),
(342, '18', 7.20, 8.10, 9.10, 10.20, 11.60, 13.20, 15.10, 'BB/U', 'perempuan', NULL, NULL),
(343, '19', 7.30, 8.20, 9.20, 10.40, 11.80, 13.50, 15.40, 'BB/U', 'perempuan', NULL, NULL),
(344, '20', 7.50, 8.40, 9.40, 10.60, 12.10, 13.70, 15.70, 'BB/U', 'perempuan', NULL, NULL),
(345, '21', 7.60, 8.60, 9.60, 10.90, 12.30, 14.00, 16.00, 'BB/U', 'perempuan', NULL, NULL),
(346, '22', 7.80, 8.70, 9.80, 11.10, 12.50, 14.30, 16.40, 'BB/U', 'perempuan', NULL, NULL),
(347, '23', 7.90, 8.90, 10.00, 11.30, 12.80, 14.60, 16.70, 'BB/U', 'perempuan', NULL, NULL),
(348, '24', 8.10, 9.00, 10.20, 11.50, 13.00, 14.80, 17.00, 'BB/U', 'perempuan', NULL, NULL),
(349, '25', 8.20, 9.20, 10.30, 11.70, 13.30, 15.10, 17.30, 'BB/U', 'perempuan', NULL, NULL),
(350, '26', 8.40, 9.40, 10.50, 11.90, 13.50, 15.40, 17.70, 'BB/U', 'perempuan', NULL, NULL),
(351, '27', 8.50, 9.50, 10.70, 12.10, 13.70, 15.60, 18.00, 'BB/U', 'perempuan', NULL, NULL),
(352, '28', 8.60, 9.70, 10.90, 12.30, 14.00, 16.00, 18.30, 'BB/U', 'perempuan', NULL, NULL),
(353, '29', 8.80, 9.80, 11.10, 12.50, 14.20, 16.20, 18.70, 'BB/U', 'perempuan', NULL, NULL),
(354, '30', 8.90, 10.00, 11.20, 12.70, 14.40, 16.50, 19.00, 'BB/U', 'perempuan', NULL, NULL),
(355, '31', 9.00, 10.10, 11.40, 12.90, 14.70, 16.80, 19.30, 'BB/U', 'perempuan', NULL, NULL),
(356, '32', 9.10, 10.30, 11.60, 13.10, 14.90, 17.10, 19.60, 'BB/U', 'perempuan', NULL, NULL),
(357, '33', 9.30, 10.40, 11.70, 13.30, 15.10, 17.30, 20.00, 'BB/U', 'perempuan', NULL, NULL),
(358, '34', 9.40, 10.50, 11.90, 13.50, 15.40, 17.60, 20.30, 'BB/U', 'perempuan', NULL, NULL),
(359, '35', 9.50, 10.70, 12.00, 13.70, 15.60, 17.90, 20.60, 'BB/U', 'perempuan', NULL, NULL),
(360, '36', 9.60, 10.80, 12.20, 13.90, 15.80, 18.10, 20.90, 'BB/U', 'perempuan', NULL, NULL),
(361, '37', 9.70, 10.90, 12.40, 14.00, 16.00, 18.40, 21.30, 'BB/U', 'perempuan', NULL, NULL),
(362, '38', 9.80, 11.10, 12.50, 14.20, 16.30, 18.70, 21.60, 'BB/U', 'perempuan', NULL, NULL),
(363, '39', 9.90, 11.20, 12.70, 14.40, 16.50, 19.00, 22.00, 'BB/U', 'perempuan', NULL, NULL),
(364, '40', 10.10, 11.30, 12.80, 14.60, 16.70, 19.20, 22.30, 'BB/U', 'perempuan', NULL, NULL),
(365, '41', 10.20, 11.50, 13.00, 14.80, 16.90, 19.50, 22.70, 'BB/U', 'perempuan', NULL, NULL),
(366, '42', 10.30, 11.60, 13.10, 15.00, 17.20, 19.80, 23.00, 'BB/U', 'perempuan', NULL, NULL),
(367, '43', 10.40, 11.70, 13.30, 15.20, 17.40, 20.10, 23.40, 'BB/U', 'perempuan', NULL, NULL),
(368, '44', 10.50, 11.80, 13.40, 15.30, 17.60, 20.40, 23.70, 'BB/U', 'perempuan', NULL, NULL),
(369, '45', 10.60, 12.00, 13.60, 15.50, 17.80, 20.70, 24.10, 'BB/U', 'perempuan', NULL, NULL),
(370, '46', 10.70, 12.10, 13.70, 15.70, 18.10, 20.90, 24.50, 'BB/U', 'perempuan', NULL, NULL),
(371, '47', 10.80, 12.20, 13.90, 15.90, 18.30, 21.20, 24.80, 'BB/U', 'perempuan', NULL, NULL),
(372, '48', 10.90, 12.30, 14.00, 16.10, 18.50, 21.50, 25.20, 'BB/U', 'perempuan', NULL, NULL),
(373, '49', 11.00, 12.40, 14.20, 16.30, 18.80, 21.80, 25.50, 'BB/U', 'perempuan', NULL, NULL),
(374, '50', 11.10, 12.60, 14.30, 16.40, 19.00, 22.10, 25.90, 'BB/U', 'perempuan', NULL, NULL),
(375, '51', 11.20, 12.70, 14.50, 16.60, 19.20, 22.40, 26.30, 'BB/U', 'perempuan', NULL, NULL),
(376, '52', 11.30, 12.80, 14.60, 16.80, 19.40, 22.60, 26.60, 'BB/U', 'perempuan', NULL, NULL),
(377, '53', 11.40, 12.90, 14.80, 17.00, 19.70, 22.90, 27.00, 'BB/U', 'perempuan', NULL, NULL),
(378, '54', 11.50, 13.00, 14.90, 17.20, 19.90, 23.20, 27.40, 'BB/U', 'perempuan', NULL, NULL),
(379, '55', 11.60, 13.20, 15.10, 17.30, 20.10, 23.50, 27.70, 'BB/U', 'perempuan', NULL, NULL),
(380, '56', 11.70, 13.30, 15.20, 17.50, 20.30, 23.80, 28.10, 'BB/U', 'perempuan', NULL, NULL),
(381, '57', 11.80, 13.40, 15.30, 17.70, 20.60, 24.10, 28.50, 'BB/U', 'perempuan', NULL, NULL),
(382, '58', 11.90, 13.50, 15.50, 17.90, 20.80, 24.40, 28.80, 'BB/U', 'perempuan', NULL, NULL),
(383, '59', 12.00, 13.60, 15.60, 18.00, 21.00, 24.60, 29.20, 'BB/U', 'perempuan', NULL, NULL),
(384, '60', 12.10, 13.70, 15.80, 18.20, 21.20, 24.90, 29.50, 'BB/U', 'perempuan', NULL, NULL),
(385, '0', 44.20, 46.10, 48.00, 49.90, 51.80, 53.70, 55.60, 'PB/U', 'laki-laki', NULL, NULL),
(386, '1', 48.90, 50.80, 52.80, 54.70, 56.70, 58.60, 60.60, 'PB/U', 'laki-laki', NULL, NULL),
(387, '2', 52.40, 54.40, 56.40, 58.40, 60.40, 62.40, 64.40, 'PB/U', 'laki-laki', NULL, NULL),
(388, '3', 55.30, 57.30, 59.40, 61.40, 63.50, 65.50, 67.60, 'PB/U', 'laki-laki', NULL, NULL),
(389, '4', 57.60, 59.70, 61.80, 63.90, 66.00, 68.00, 70.10, 'PB/U', 'laki-laki', NULL, NULL),
(390, '5', 59.60, 61.70, 63.80, 65.90, 68.00, 70.10, 72.20, 'PB/U', 'laki-laki', NULL, NULL),
(391, '6', 61.20, 63.30, 65.50, 67.60, 69.80, 71.90, 74.00, 'PB/U', 'laki-laki', NULL, NULL),
(392, '7', 62.70, 64.80, 67.00, 69.20, 71.30, 73.50, 75.70, 'PB/U', 'laki-laki', NULL, NULL),
(393, '8', 64.00, 66.20, 68.40, 70.60, 72.80, 75.00, 77.20, 'PB/U', 'laki-laki', NULL, NULL),
(394, '9', 65.20, 67.50, 69.70, 72.00, 74.20, 76.50, 78.70, 'PB/U', 'laki-laki', NULL, NULL),
(395, '10', 66.40, 68.70, 71.00, 74.40, 75.60, 77.90, 80.10, 'PB/U', 'laki-laki', NULL, NULL),
(396, '11', 67.60, 69.90, 72.20, 74.50, 76.90, 79.20, 81.50, 'PB/U', 'laki-laki', NULL, NULL),
(397, '12', 68.60, 71.00, 73.40, 75.70, 78.10, 80.50, 82.90, 'PB/U', 'laki-laki', NULL, NULL),
(398, '13', 69.60, 72.10, 74.50, 76.90, 79.30, 81.80, 84.20, 'PB/U', 'laki-laki', NULL, NULL),
(399, '14', 70.60, 73.10, 75.60, 78.00, 80.50, 83.00, 85.50, 'PB/U', 'laki-laki', NULL, NULL),
(400, '15', 71.60, 74.10, 76.60, 79.10, 81.70, 84.20, 86.70, 'PB/U', 'laki-laki', NULL, NULL),
(401, '16', 72.50, 75.00, 77.60, 80.20, 82.80, 85.40, 88.00, 'PB/U', 'laki-laki', NULL, NULL),
(402, '17', 73.30, 76.00, 78.60, 81.20, 83.90, 86.50, 89.20, 'PB/U', 'laki-laki', NULL, NULL),
(403, '18', 74.20, 76.90, 79.60, 82.30, 85.00, 87.70, 90.40, 'PB/U', 'laki-laki', NULL, NULL),
(404, '19', 75.00, 77.70, 80.50, 83.20, 86.00, 88.80, 91.50, 'PB/U', 'laki-laki', NULL, NULL),
(405, '20', 75.80, 78.60, 81.40, 84.20, 87.00, 89.80, 92.60, 'PB/U', 'laki-laki', NULL, NULL),
(406, '21', 76.50, 79.40, 82.30, 85.10, 88.00, 90.90, 93.80, 'PB/U', 'laki-laki', NULL, NULL),
(407, '22', 77.20, 80.20, 83.10, 86.00, 89.00, 91.90, 94.90, 'PB/U', 'laki-laki', NULL, NULL),
(408, '23', 78.00, 81.00, 83.90, 86.90, 89.90, 92.90, 95.90, 'PB/U', 'laki-laki', NULL, NULL),
(409, '24', 78.70, 81.70, 84.80, 87.80, 90.90, 93.90, 97.00, 'PB/U', 'laki-laki', NULL, NULL),
(410, '0', 43.60, 45.40, 47.30, 49.10, 51.00, 52.90, 54.70, 'PB/U', 'perempuan', NULL, NULL),
(411, '1', 47.80, 49.80, 51.70, 53.70, 55.60, 57.60, 59.50, 'PB/U', 'perempuan', NULL, NULL),
(412, '2', 51.00, 53.00, 55.00, 57.10, 59.10, 61.10, 63.20, 'PB/U', 'perempuan', NULL, NULL),
(413, '3', 53.50, 55.60, 57.70, 59.80, 61.90, 64.00, 66.10, 'PB/U', 'perempuan', NULL, NULL),
(414, '4', 55.60, 57.80, 59.90, 62.10, 64.30, 66.40, 68.60, 'PB/U', 'perempuan', NULL, NULL),
(415, '5', 57.40, 59.60, 61.80, 64.00, 66.20, 68.50, 70.70, 'PB/U', 'perempuan', NULL, NULL),
(416, '6', 58.90, 61.20, 63.50, 65.70, 68.00, 70.30, 72.50, 'PB/U', 'perempuan', NULL, NULL),
(417, '7', 60.30, 62.70, 65.00, 67.30, 69.60, 71.90, 74.20, 'PB/U', 'perempuan', NULL, NULL),
(418, '8', 61.70, 64.00, 66.40, 68.70, 71.10, 73.50, 75.80, 'PB/U', 'perempuan', NULL, NULL),
(419, '9', 62.90, 65.30, 67.70, 70.10, 72.60, 75.00, 77.40, 'PB/U', 'perempuan', NULL, NULL),
(420, '10', 64.10, 66.50, 69.00, 71.50, 73.90, 76.40, 78.90, 'PB/U', 'perempuan', NULL, NULL),
(421, '11', 65.20, 67.70, 70.30, 72.80, 75.30, 77.80, 80.30, 'PB/U', 'perempuan', NULL, NULL),
(422, '12', 66.30, 68.90, 71.40, 74.00, 76.60, 79.20, 81.70, 'PB/U', 'perempuan', NULL, NULL),
(423, '13', 67.30, 70.00, 72.60, 75.20, 77.80, 80.50, 83.10, 'PB/U', 'perempuan', NULL, NULL),
(424, '14', 68.30, 71.00, 73.70, 76.40, 79.10, 81.70, 84.40, 'PB/U', 'perempuan', NULL, NULL),
(425, '15', 69.30, 72.00, 74.80, 77.50, 80.20, 83.00, 85.70, 'PB/U', 'perempuan', NULL, NULL),
(426, '16', 70.20, 73.00, 75.80, 78.60, 81.40, 84.20, 87.00, 'PB/U', 'perempuan', NULL, NULL),
(427, '17', 71.10, 74.00, 76.80, 79.70, 82.50, 85.40, 88.20, 'PB/U', 'perempuan', NULL, NULL),
(428, '18', 72.00, 74.90, 77.80, 80.70, 83.60, 86.50, 89.40, 'PB/U', 'perempuan', NULL, NULL),
(429, '19', 72.80, 75.80, 78.80, 81.70, 84.70, 87.60, 90.60, 'PB/U', 'perempuan', NULL, NULL),
(430, '20', 73.70, 76.70, 79.70, 82.70, 85.70, 88.70, 91.70, 'PB/U', 'perempuan', NULL, NULL),
(431, '21', 74.50, 77.50, 80.60, 83.70, 86.70, 89.80, 92.90, 'PB/U', 'perempuan', NULL, NULL),
(432, '22', 75.20, 78.40, 81.50, 84.60, 87.70, 90.80, 94.00, 'PB/U', 'perempuan', NULL, NULL),
(433, '23', 76.00, 79.20, 82.30, 85.50, 88.70, 91.90, 95.00, 'PB/U', 'perempuan', NULL, NULL),
(434, '24', 76.70, 80.00, 83.20, 86.40, 89.60, 92.90, 96.10, 'PB/U', 'perempuan', NULL, NULL),
(435, '65', 5.90, 6.30, 6.90, 7.40, 8.10, 8.80, 9.60, 'BB/TB', 'laki-laki', NULL, NULL),
(436, '65.5', 6.00, 6.40, 7.00, 7.60, 8.20, 8.90, 9.80, 'BB/TB', 'laki-laki', NULL, NULL),
(437, '66', 6.10, 6.50, 7.10, 7.70, 8.30, 9.10, 9.90, 'BB/TB', 'laki-laki', NULL, NULL),
(438, '66.5', 6.10, 6.60, 7.20, 7.80, 8.50, 9.20, 10.10, 'BB/TB', 'laki-laki', NULL, NULL),
(439, '67', 6.20, 6.70, 7.30, 7.90, 8.60, 9.40, 10.20, 'BB/TB', 'laki-laki', NULL, NULL),
(440, '67.5', 6.30, 6.80, 7.40, 8.00, 8.70, 9.50, 10.40, 'BB/TB', 'laki-laki', NULL, NULL),
(441, '68', 6.40, 6.90, 7.50, 8.10, 8.80, 9.60, 10.50, 'BB/TB', 'laki-laki', NULL, NULL),
(442, '68.5', 6.50, 7.00, 7.60, 8.20, 9.00, 9.80, 10.70, 'BB/TB', 'laki-laki', NULL, NULL),
(443, '69', 6.60, 7.10, 7.70, 8.40, 9.10, 9.90, 10.80, 'BB/TB', 'laki-laki', NULL, NULL),
(444, '69.5', 6.70, 7.20, 7.80, 8.50, 9.20, 10.00, 11.00, 'BB/TB', 'laki-laki', NULL, NULL),
(445, '70', 6.70, 7.30, 7.90, 8.60, 9.30, 10.20, 11.10, 'BB/TB', 'laki-laki', NULL, NULL),
(446, '70.5', 6.90, 7.40, 8.00, 8.70, 9.50, 10.30, 11.30, 'BB/TB', 'laki-laki', NULL, NULL),
(447, '71', 6.90, 7.50, 8.10, 8.80, 9.60, 10.40, 11.40, 'BB/TB', 'laki-laki', NULL, NULL),
(448, '71.5', 7.00, 7.60, 8.20, 8.90, 9.70, 10.60, 11.60, 'BB/TB', 'laki-laki', NULL, NULL),
(449, '72', 7.10, 7.70, 8.30, 9.00, 9.80, 10.70, 11.70, 'BB/TB', 'laki-laki', NULL, NULL),
(450, '72.5', 7.20, 7.80, 8.40, 9.10, 9.90, 10.80, 11.80, 'BB/TB', 'laki-laki', NULL, NULL),
(451, '73', 7.30, 7.90, 8.50, 9.20, 10.00, 11.00, 12.00, 'BB/TB', 'laki-laki', NULL, NULL),
(452, '73.5', 7.40, 7.90, 8.60, 9.30, 10.20, 11.10, 12.10, 'BB/TB', 'laki-laki', NULL, NULL),
(453, '74', 7.40, 8.00, 8.70, 9.40, 10.30, 11.20, 12.20, 'BB/TB', 'laki-laki', NULL, NULL),
(454, '74.5', 7.50, 8.10, 8.80, 9.50, 10.40, 11.30, 12.40, 'BB/TB', 'laki-laki', NULL, NULL),
(455, '75', 7.60, 8.20, 8.90, 9.60, 10.50, 11.40, 12.50, 'BB/TB', 'laki-laki', NULL, NULL),
(456, '75.5', 7.70, 8.30, 9.00, 9.70, 10.60, 11.60, 12.60, 'BB/TB', 'laki-laki', NULL, NULL),
(457, '76', 7.70, 8.40, 9.10, 9.80, 10.70, 11.70, 12.80, 'BB/TB', 'laki-laki', NULL, NULL),
(458, '76.5', 7.80, 8.50, 9.20, 9.90, 10.80, 11.80, 12.90, 'BB/TB', 'laki-laki', NULL, NULL),
(459, '77', 7.90, 8.50, 9.20, 10.00, 10.90, 11.90, 13.00, 'BB/TB', 'laki-laki', NULL, NULL),
(460, '77.5', 8.00, 8.60, 9.30, 10.10, 11.00, 12.00, 13.10, 'BB/TB', 'laki-laki', NULL, NULL),
(461, '78', 8.00, 8.70, 9.40, 10.20, 11.10, 12.10, 13.30, 'BB/TB', 'laki-laki', NULL, NULL),
(462, '78.5', 8.10, 8.80, 9.50, 10.30, 11.20, 12.20, 13.40, 'BB/TB', 'laki-laki', NULL, NULL),
(463, '79', 8.20, 8.80, 9.60, 10.40, 11.30, 12.30, 13.50, 'BB/TB', 'laki-laki', NULL, NULL),
(464, '79.5', 8.30, 8.90, 9.70, 10.50, 11.40, 12.40, 13.60, 'BB/TB', 'laki-laki', NULL, NULL),
(465, '80', 8.30, 9.00, 9.70, 10.60, 11.50, 12.60, 13.70, 'BB/TB', 'laki-laki', NULL, NULL),
(466, '80.5', 8.40, 9.10, 9.80, 10.70, 11.60, 12.70, 13.80, 'BB/TB', 'laki-laki', NULL, NULL),
(467, '81', 8.50, 9.20, 9.90, 10.80, 11.70, 12.80, 14.00, 'BB/TB', 'laki-laki', NULL, NULL),
(468, '81.5', 8.60, 9.30, 10.00, 10.90, 11.80, 12.90, 14.10, 'BB/TB', 'laki-laki', NULL, NULL),
(469, '82', 8.70, 9.30, 10.10, 11.00, 11.90, 1.30, 14.20, 'BB/TB', 'laki-laki', NULL, NULL),
(470, '82.5', 8.70, 9.40, 10.20, 11.10, 12.10, 13.10, 14.40, 'BB/TB', 'laki-laki', NULL, NULL),
(471, '83', 8.80, 9.50, 10.30, 11.20, 12.20, 13.30, 14.50, 'BB/TB', 'laki-laki', NULL, NULL),
(472, '83.5', 8.90, 9.60, 10.40, 11.30, 12.30, 13.40, 14.60, 'BB/TB', 'laki-laki', NULL, NULL),
(473, '84', 9.00, 9.70, 10.50, 11.40, 12.40, 13.50, 14.80, 'BB/TB', 'laki-laki', NULL, NULL),
(474, '84.5', 9.10, 9.90, 10.70, 11.50, 12.50, 13.70, 14.90, 'BB/TB', 'laki-laki', NULL, NULL),
(475, '85', 9.20, 10.00, 10.80, 11.70, 12.70, 13.80, 15.10, 'BB/TB', 'laki-laki', NULL, NULL),
(476, '85.5', 9.30, 10.10, 10.90, 11.80, 12.80, 13.90, 15.20, 'BB/TB', 'laki-laki', NULL, NULL),
(477, '86', 9.40, 10.20, 11.00, 11.90, 12.90, 14.10, 15.40, 'BB/TB', 'laki-laki', NULL, NULL),
(478, '86.5', 9.50, 10.30, 11.10, 12.00, 13.10, 14.20, 15.50, 'BB/TB', 'laki-laki', NULL, NULL),
(479, '87', 9.60, 10.40, 11.20, 12.20, 13.20, 14.40, 15.70, 'BB/TB', 'laki-laki', NULL, NULL),
(480, '87.5', 9.70, 10.50, 11.30, 12.30, 13.30, 14.50, 15.80, 'BB/TB', 'laki-laki', NULL, NULL),
(481, '88', 9.80, 10.60, 11.50, 12.40, 13.50, 14.70, 16.00, 'BB/TB', 'laki-laki', NULL, NULL),
(482, '88.5', 9.00, 10.70, 11.60, 12.50, 13.60, 14.80, 16.10, 'BB/TB', 'laki-laki', NULL, NULL),
(483, '89', 10.00, 10.80, 11.70, 12.60, 13.70, 14.90, 16.30, 'BB/TB', 'laki-laki', NULL, NULL),
(484, '89.5', 10.10, 10.90, 11.80, 12.80, 13.90, 15.10, 16.40, 'BB/TB', 'laki-laki', NULL, NULL),
(485, '90', 10.20, 11.00, 11.90, 12.90, 14.00, 15.20, 16.60, 'BB/TB', 'laki-laki', NULL, NULL),
(486, '90.5', 10.30, 11.10, 12.00, 13.00, 14.10, 15.30, 16.70, 'BB/TB', 'laki-laki', NULL, NULL),
(487, '91', 10.40, 11.20, 12.10, 13.10, 14.20, 15.50, 16.90, 'BB/TB', 'laki-laki', NULL, NULL),
(488, '91.5', 10.50, 11.30, 12.20, 13.20, 14.40, 15.60, 17.00, 'BB/TB', 'laki-laki', NULL, NULL),
(489, '92', 10.60, 11.40, 12.30, 13.40, 14.50, 15.80, 17.20, 'BB/TB', 'laki-laki', NULL, NULL),
(490, '92.5', 10.70, 11.50, 12.40, 13.50, 14.60, 15.90, 17.30, 'BB/TB', 'laki-laki', NULL, NULL),
(491, '93', 10.80, 11.60, 12.60, 13.60, 14.70, 16.00, 17.50, 'BB/TB', 'laki-laki', NULL, NULL),
(492, '93.5', 10.90, 11.70, 12.70, 13.70, 14.90, 16.20, 17.60, 'BB/TB', 'laki-laki', NULL, NULL),
(493, '94', 11.00, 11.80, 12.80, 13.80, 15.00, 16.30, 17.80, 'BB/TB', 'laki-laki', NULL, NULL),
(494, '94.5', 11.10, 11.90, 12.90, 13.90, 15.10, 16.50, 17.90, 'BB/TB', 'laki-laki', NULL, NULL),
(495, '95', 11.10, 12.00, 13.00, 14.10, 15.30, 16.60, 18.10, 'BB/TB', 'laki-laki', NULL, NULL),
(496, '95.5', 11.20, 12.10, 13.10, 14.20, 15.40, 16.70, 18.30, 'BB/TB', 'laki-laki', NULL, NULL),
(497, '96', 11.30, 12.20, 13.20, 14.30, 15.50, 16.90, 18.40, 'BB/TB', 'laki-laki', NULL, NULL),
(498, '96.5', 11.40, 12.30, 13.30, 14.40, 15.70, 17.00, 18.60, 'BB/TB', 'laki-laki', NULL, NULL),
(499, '97', 11.50, 12.40, 13.40, 14.60, 15.80, 17.20, 18.80, 'BB/TB', 'laki-laki', NULL, NULL),
(500, '97.5', 11.60, 12.50, 13.60, 14.70, 15.90, 17.40, 18.90, 'BB/TB', 'laki-laki', NULL, NULL),
(501, '98', 11.70, 12.60, 13.70, 14.80, 16.10, 17.50, 19.10, 'BB/TB', 'laki-laki', NULL, NULL),
(502, '98.5', 11.80, 12.80, 13.80, 14.90, 16.20, 17.70, 19.30, 'BB/TB', 'laki-laki', NULL, NULL),
(503, '99', 11.90, 12.90, 13.90, 15.10, 16.40, 17.90, 19.50, 'BB/TB', 'laki-laki', NULL, NULL),
(504, '99.5', 12.00, 13.00, 14.00, 15.20, 16.50, 18.00, 19.70, 'BB/TB', 'laki-laki', NULL, NULL),
(505, '100', 12.10, 13.10, 14.20, 15.40, 16.70, 18.20, 19.90, 'BB/TB', 'laki-laki', NULL, NULL),
(506, '100.5', 12.20, 13.20, 14.30, 15.50, 16.90, 18.40, 20.10, 'BB/TB', 'laki-laki', NULL, NULL),
(507, '101', 12.30, 13.30, 14.40, 15.60, 17.00, 18.50, 20.30, 'BB/TB', 'laki-laki', NULL, NULL),
(508, '101.5', 12.40, 13.40, 14.50, 15.80, 17.20, 18.70, 20.50, 'BB/TB', 'laki-laki', NULL, NULL),
(509, '102', 12.50, 13.60, 14.70, 15.90, 17.30, 18.90, 20.70, 'BB/TB', 'laki-laki', NULL, NULL),
(510, '102.5', 12.60, 13.70, 14.80, 16.10, 17.50, 19.10, 20.90, 'BB/TB', 'laki-laki', NULL, NULL),
(511, '103', 12.80, 13.80, 14.90, 16.20, 17.70, 19.30, 21.10, 'BB/TB', 'laki-laki', NULL, NULL),
(512, '103.5', 12.90, 13.90, 15.10, 16.40, 17.80, 19.50, 21.30, 'BB/TB', 'laki-laki', NULL, NULL),
(513, '104', 13.00, 14.00, 15.20, 16.50, 18.00, 19.70, 21.60, 'BB/TB', 'laki-laki', NULL, NULL),
(514, '104.5', 13.10, 14.20, 15.40, 16.70, 18.20, 19.90, 21.80, 'BB/TB', 'laki-laki', NULL, NULL),
(515, '105', 13.20, 14.30, 15.50, 16.80, 18.40, 20.10, 22.00, 'BB/TB', 'laki-laki', NULL, NULL),
(516, '105.5', 13.30, 14.40, 15.60, 17.00, 18.50, 20.30, 22.20, 'BB/TB', 'laki-laki', NULL, NULL),
(517, '106', 13.40, 14.50, 15.80, 17.20, 18.70, 20.50, 22.50, 'BB/TB', 'laki-laki', NULL, NULL),
(518, '106.5', 13.50, 14.70, 15.90, 17.30, 18.90, 20.70, 22.70, 'BB/TB', 'laki-laki', NULL, NULL),
(519, '107', 13.70, 14.80, 16.10, 17.50, 19.10, 20.90, 22.90, 'BB/TB', 'laki-laki', NULL, NULL),
(520, '107.5', 13.80, 14.90, 16.20, 17.70, 19.30, 21.10, 23.20, 'BB/TB', 'laki-laki', NULL, NULL),
(521, '108', 13.90, 15.10, 16.40, 17.80, 19.50, 21.30, 23.40, 'BB/TB', 'laki-laki', NULL, NULL),
(522, '108.5', 14.00, 15.20, 16.50, 18.00, 19.70, 21.50, 23.70, 'BB/TB', 'laki-laki', NULL, NULL),
(523, '109', 14.10, 15.30, 16.70, 18.20, 19.80, 21.80, 23.90, 'BB/TB', 'laki-laki', NULL, NULL),
(524, '109.5', 14.30, 15.50, 16.80, 18.30, 20.00, 22.00, 24.20, 'BB/TB', 'laki-laki', NULL, NULL),
(525, '110', 14.40, 15.60, 17.00, 18.50, 20.20, 22.20, 24.40, 'BB/TB', 'laki-laki', NULL, NULL),
(526, '110.5', 14.50, 15.80, 17.10, 18.70, 20.40, 22.40, 24.70, 'BB/TB', 'laki-laki', NULL, NULL),
(527, '111', 14.60, 15.90, 17.30, 18.90, 20.70, 22.70, 25.00, 'BB/TB', 'laki-laki', NULL, NULL),
(528, '111.5', 14.80, 16.00, 17.50, 19.10, 20.90, 22.90, 25.20, 'BB/TB', 'laki-laki', NULL, NULL),
(529, '112', 14.90, 16.20, 17.60, 19.20, 21.10, 23.10, 25.50, 'BB/TB', 'laki-laki', NULL, NULL),
(530, '112.5', 15.00, 16.30, 17.80, 19.40, 21.30, 23.40, 25.80, 'BB/TB', 'laki-laki', NULL, NULL),
(531, '113', 15.20, 16.50, 18.00, 19.60, 21.50, 23.60, 26.00, 'BB/TB', 'laki-laki', NULL, NULL),
(532, '113.5', 15.30, 16.60, 18.10, 19.80, 21.70, 23.90, 26.30, 'BB/TB', 'laki-laki', NULL, NULL),
(533, '114', 15.40, 16.80, 18.30, 20.00, 21.90, 24.10, 26.60, 'BB/TB', 'laki-laki', NULL, NULL),
(534, '114.5', 15.60, 16.90, 18.50, 20.20, 22.10, 24.40, 26.90, 'BB/TB', 'laki-laki', NULL, NULL),
(535, '115', 15.70, 17.10, 18.60, 20.40, 22.40, 24.60, 27.20, 'BB/TB', 'laki-laki', NULL, NULL),
(536, '115.5', 15.80, 17.20, 18.80, 20.60, 22.60, 24.90, 27.50, 'BB/TB', 'laki-laki', NULL, NULL),
(537, '116', 16.00, 17.40, 19.00, 20.80, 22.80, 25.10, 27.80, 'BB/TB', 'laki-laki', NULL, NULL),
(538, '116.5', 16.10, 17.50, 19.20, 21.00, 23.00, 25.40, 28.00, 'BB/TB', 'laki-laki', NULL, NULL),
(539, '117', 16.20, 17.70, 19.30, 21.20, 23.30, 25.60, 28.30, 'BB/TB', 'laki-laki', NULL, NULL),
(540, '117.5', 16.40, 17.90, 19.50, 21.40, 23.50, 25.90, 28.60, 'BB/TB', 'laki-laki', NULL, NULL),
(541, '118', 16.50, 18.00, 19.70, 21.60, 23.70, 26.10, 28.90, 'BB/TB', 'laki-laki', NULL, NULL),
(542, '118.5', 16.70, 18.20, 19.90, 21.80, 23.90, 26.40, 29.20, 'BB/TB', 'laki-laki', NULL, NULL);
INSERT INTO `standar_pertumbuhan` (`id`, `parameter`, `sdmintiga`, `sdmindua`, `sdminsatu`, `median`, `sdplussatu`, `sdplusdua`, `sdplustiga`, `tipe_tabel`, `jenis_kelamin`, `created_at`, `updated_at`) VALUES
(543, '119', 16.80, 18.30, 20.00, 22.00, 24.10, 26.60, 29.50, 'BB/TB', 'laki-laki', NULL, NULL),
(544, '119.5', 16.90, 18.50, 20.20, 22.20, 24.40, 26.90, 29.80, 'BB/TB', 'laki-laki', NULL, NULL),
(545, '120', 17.10, 18.60, 20.40, 22.40, 24.60, 27.20, 30.10, 'BB/TB', 'laki-laki', NULL, NULL),
(546, '65', 5.60, 6.10, 6.60, 7.20, 7.90, 8.70, 9.70, 'BB/TB', 'perempuan', NULL, NULL),
(547, '65.5', 5.70, 6.20, 6.70, 7.40, 8.10, 8.90, 9.80, 'BB/TB', 'perempuan', NULL, NULL),
(548, '66', 5.80, 6.30, 6.80, 7.50, 8.20, 9.00, 10.00, 'BB/TB', 'perempuan', NULL, NULL),
(549, '66.5', 5.80, 6.40, 6.90, 7.60, 8.30, 9.10, 10.10, 'BB/TB', 'perempuan', NULL, NULL),
(550, '67', 5.90, 6.40, 7.00, 7.70, 8.40, 9.30, 10.20, 'BB/TB', 'perempuan', NULL, NULL),
(551, '67.5', 6.00, 6.50, 7.10, 7.80, 8.50, 9.40, 10.40, 'BB/TB', 'perempuan', NULL, NULL),
(552, '68', 6.10, 6.60, 7.20, 7.90, 8.70, 9.50, 10.50, 'BB/TB', 'perempuan', NULL, NULL),
(553, '68.5', 6.20, 6.70, 7.30, 8.00, 8.80, 9.70, 10.70, 'BB/TB', 'perempuan', NULL, NULL),
(554, '69', 6.30, 6.80, 7.40, 8.10, 8.90, 9.80, 10.80, 'BB/TB', 'perempuan', NULL, NULL),
(555, '69.5', 6.40, 6.90, 7.50, 8.20, 9.00, 9.90, 10.90, 'BB/TB', 'perempuan', NULL, NULL),
(556, '70', 6.40, 7.00, 7.60, 8.30, 9.10, 10.00, 11.10, 'BB/TB', 'perempuan', NULL, NULL),
(557, '70.5', 6.50, 7.10, 7.70, 8.40, 9.20, 10.10, 11.20, 'BB/TB', 'perempuan', NULL, NULL),
(558, '71', 6.60, 7.10, 7.80, 8.50, 9.30, 10.30, 11.30, 'BB/TB', 'perempuan', NULL, NULL),
(559, '71.5', 6.70, 7.20, 7.90, 8.60, 9.40, 10.40, 11.50, 'BB/TB', 'perempuan', NULL, NULL),
(560, '72', 6.70, 7.30, 8.00, 8.70, 9.50, 10.50, 11.60, 'BB/TB', 'perempuan', NULL, NULL),
(561, '72.5', 6.80, 7.40, 8.10, 8.80, 9.70, 10.60, 11.70, 'BB/TB', 'perempuan', NULL, NULL),
(562, '73', 6.90, 7.50, 8.10, 8.90, 9.80, 10.70, 11.80, 'BB/TB', 'perempuan', NULL, NULL),
(563, '73.5', 7.00, 7.60, 8.20, 9.00, 9.90, 10.80, 12.00, 'BB/TB', 'perempuan', NULL, NULL),
(564, '74', 7.00, 7.60, 8.30, 9.10, 10.00, 11.00, 12.10, 'BB/TB', 'perempuan', NULL, NULL),
(565, '74.5', 7.10, 7.70, 8.40, 9.20, 10.10, 11.10, 12.20, 'BB/TB', 'perempuan', NULL, NULL),
(566, '75', 7.20, 7.80, 8.50, 9.30, 10.20, 11.20, 12.30, 'BB/TB', 'perempuan', NULL, NULL),
(567, '75.5', 7.20, 7.90, 8.60, 9.40, 10.30, 11.30, 12.50, 'BB/TB', 'perempuan', NULL, NULL),
(568, '76', 7.30, 8.00, 8.70, 9.50, 10.40, 11.40, 12.60, 'BB/TB', 'perempuan', NULL, NULL),
(569, '76.5', 7.40, 8.00, 8.70, 9.60, 10.50, 11.50, 12.70, 'BB/TB', 'perempuan', NULL, NULL),
(570, '77', 7.50, 8.10, 8.80, 9.60, 10.60, 11.60, 12.80, 'BB/TB', 'perempuan', NULL, NULL),
(571, '77.5', 7.50, 8.20, 8.90, 9.70, 10.70, 11.70, 12.90, 'BB/TB', 'perempuan', NULL, NULL),
(572, '78', 7.60, 8.30, 9.00, 9.80, 10.80, 11.80, 13.10, 'BB/TB', 'perempuan', NULL, NULL),
(573, '78.5', 7.70, 8.40, 9.10, 9.90, 10.90, 12.00, 13.20, 'BB/TB', 'perempuan', NULL, NULL),
(574, '79', 7.80, 8.40, 9.20, 10.00, 11.00, 12.10, 13.30, 'BB/TB', 'perempuan', NULL, NULL),
(575, '79.5', 7.80, 8.50, 9.30, 10.10, 11.10, 12.20, 13.40, 'BB/TB', 'perempuan', NULL, NULL),
(576, '80', 7.90, 8.60, 9.40, 10.20, 11.20, 12.30, 13.60, 'BB/TB', 'perempuan', NULL, NULL),
(577, '80.5', 8.00, 8.70, 9.50, 10.30, 11.30, 12.40, 13.70, 'BB/TB', 'perempuan', NULL, NULL),
(578, '81', 8.10, 8.80, 9.60, 10.40, 11.40, 12.60, 13.90, 'BB/TB', 'perempuan', NULL, NULL),
(579, '81.5', 8.20, 8.90, 9.70, 10.60, 11.60, 12.70, 14.00, 'BB/TB', 'perempuan', NULL, NULL),
(580, '82', 8.30, 9.00, 9.80, 10.70, 11.70, 12.80, 14.10, 'BB/TB', 'perempuan', NULL, NULL),
(581, '82.5', 8.40, 9.10, 9.90, 10.80, 11.80, 13.00, 14.30, 'BB/TB', 'perempuan', NULL, NULL),
(582, '83', 8.50, 9.20, 10.00, 10.90, 11.90, 13.10, 14.50, 'BB/TB', 'perempuan', NULL, NULL),
(583, '83.5', 8.50, 9.30, 10.10, 11.00, 12.10, 13.30, 14.60, 'BB/TB', 'perempuan', NULL, NULL),
(584, '84', 8.60, 9.40, 10.20, 11.10, 12.20, 13.40, 14.80, 'BB/TB', 'perempuan', NULL, NULL),
(585, '84.5', 8.70, 9.50, 10.30, 11.30, 12.30, 13.50, 14.90, 'BB/TB', 'perempuan', NULL, NULL),
(586, '85', 8.80, 9.60, 10.40, 11.40, 12.50, 13.70, 15.10, 'BB/TB', 'perempuan', NULL, NULL),
(587, '85.5', 8.90, 9.70, 10.60, 11.50, 12.60, 13.80, 15.30, 'BB/TB', 'perempuan', NULL, NULL),
(588, '86', 9.00, 9.80, 10.70, 11.60, 12.70, 14.00, 15.40, 'BB/TB', 'perempuan', NULL, NULL),
(589, '86.5', 9.10, 9.90, 10.80, 11.80, 12.90, 14.20, 15.60, 'BB/TB', 'perempuan', NULL, NULL),
(590, '87', 9.20, 10.00, 10.90, 11.90, 13.00, 14.30, 15.80, 'BB/TB', 'perempuan', NULL, NULL),
(591, '87.5', 9.30, 10.10, 11.00, 12.00, 13.20, 14.50, 15.90, 'BB/TB', 'perempuan', NULL, NULL),
(592, '88', 9.40, 10.20, 11.10, 12.10, 13.30, 14.60, 16.10, 'BB/TB', 'perempuan', NULL, NULL),
(593, '88.5', 9.50, 10.30, 11.20, 12.30, 13.40, 14.80, 16.30, 'BB/TB', 'perempuan', NULL, NULL),
(594, '89', 9.60, 10.40, 11.40, 12.40, 13.60, 14.90, 16.40, 'BB/TB', 'perempuan', NULL, NULL),
(595, '89.5', 9.70, 10.50, 11.50, 12.50, 13.70, 15.10, 16.60, 'BB/TB', 'perempuan', NULL, NULL),
(596, '90', 9.80, 10.60, 11.60, 12.60, 13.80, 15.20, 16.80, 'BB/TB', 'perempuan', NULL, NULL),
(597, '90.5', 9.90, 10.70, 11.70, 12.80, 14.00, 15.40, 16.90, 'BB/TB', 'perempuan', NULL, NULL),
(598, '91', 10.00, 10.90, 11.80, 12.90, 14.10, 15.50, 17.10, 'BB/TB', 'perempuan', NULL, NULL),
(599, '91.5', 10.10, 11.00, 11.90, 13.00, 14.30, 15.70, 17.30, 'BB/TB', 'perempuan', NULL, NULL),
(600, '92', 10.20, 11.10, 12.00, 13.10, 14.40, 15.80, 17.40, 'BB/TB', 'perempuan', NULL, NULL),
(601, '92.5', 10.30, 11.20, 12.10, 13.30, 14.50, 16.00, 17.60, 'BB/TB', 'perempuan', NULL, NULL),
(602, '93', 10.40, 11.30, 12.30, 13.40, 14.70, 16.10, 17.80, 'BB/TB', 'perempuan', NULL, NULL),
(603, '93.5', 10.50, 11.40, 12.40, 13.50, 14.80, 16.30, 17.90, 'BB/TB', 'perempuan', NULL, NULL),
(604, '94', 10.60, 11.50, 12.50, 13.60, 14.90, 16.40, 18.10, 'BB/TB', 'perempuan', NULL, NULL),
(605, '94.5', 10.70, 11.60, 12.60, 13.80, 15.10, 16.60, 18.30, 'BB/TB', 'perempuan', NULL, NULL),
(606, '95', 10.80, 11.70, 12.70, 13.90, 15.20, 16.70, 18.50, 'BB/TB', 'perempuan', NULL, NULL),
(607, '95.5', 10.80, 11.80, 12.80, 14.00, 15.40, 16.90, 18.60, 'BB/TB', 'perempuan', NULL, NULL),
(608, '96', 10.90, 11.90, 12.90, 14.10, 15.50, 17.00, 18.80, 'BB/TB', 'perempuan', NULL, NULL),
(609, '96.5', 11.00, 12.00, 13.10, 14.30, 15.60, 17.20, 19.00, 'BB/TB', 'perempuan', NULL, NULL),
(610, '97', 11.10, 12.10, 13.20, 14.40, 15.80, 17.40, 19.20, 'BB/TB', 'perempuan', NULL, NULL),
(611, '97.5', 11.20, 12.20, 13.30, 14.50, 15.90, 17.50, 19.30, 'BB/TB', 'perempuan', NULL, NULL),
(612, '98', 11.30, 12.30, 13.40, 14.70, 16.10, 17.70, 19.50, 'BB/TB', 'perempuan', NULL, NULL),
(613, '98.5', 11.40, 12.40, 13.50, 14.80, 16.20, 17.90, 19.70, 'BB/TB', 'perempuan', NULL, NULL),
(614, '99', 11.50, 12.50, 13.70, 14.90, 16.40, 18.00, 19.90, 'BB/TB', 'perempuan', NULL, NULL),
(615, '99.5', 11.60, 12.70, 13.80, 15.10, 16.50, 18.20, 20.10, 'BB/TB', 'perempuan', NULL, NULL),
(616, '100', 11.70, 12.80, 13.90, 15.20, 16.70, 18.40, 20.30, 'BB/TB', 'perempuan', NULL, NULL),
(617, '100.5', 11.90, 12.90, 14.10, 15.40, 16.90, 18.60, 20.50, 'BB/TB', 'perempuan', NULL, NULL),
(618, '101', 12.00, 13.00, 14.20, 15.50, 17.00, 18.70, 20.70, 'BB/TB', 'perempuan', NULL, NULL),
(619, '101.5', 12.10, 13.10, 14.30, 15.70, 17.20, 18.90, 20.90, 'BB/TB', 'perempuan', NULL, NULL),
(620, '102', 12.20, 13.30, 14.50, 15.80, 17.40, 19.10, 21.10, 'BB/TB', 'perempuan', NULL, NULL),
(621, '102.5', 12.30, 13.40, 14.60, 16.00, 17.50, 19.30, 21.40, 'BB/TB', 'perempuan', NULL, NULL),
(622, '103', 12.40, 13.50, 14.70, 16.10, 17.70, 19.50, 21.60, 'BB/TB', 'perempuan', NULL, NULL),
(623, '103.5', 12.50, 13.60, 14.90, 16.30, 17.90, 19.70, 21.80, 'BB/TB', 'perempuan', NULL, NULL),
(624, '104', 12.60, 13.80, 15.00, 16.40, 18.10, 19.90, 22.00, 'BB/TB', 'perempuan', NULL, NULL),
(625, '104.5', 12.80, 13.90, 15.20, 16.60, 18.20, 20.10, 22.30, 'BB/TB', 'perempuan', NULL, NULL),
(626, '105', 12.90, 14.00, 15.30, 16.80, 18.40, 20.30, 22.50, 'BB/TB', 'perempuan', NULL, NULL),
(627, '105.5', 13.00, 14.20, 15.50, 16.90, 18.60, 20.50, 22.70, 'BB/TB', 'perempuan', NULL, NULL),
(628, '106', 13.10, 14.30, 15.60, 17.10, 18.80, 20.80, 23.00, 'BB/TB', 'perempuan', NULL, NULL),
(629, '106.5', 13.30, 14.50, 15.80, 17.30, 19.00, 21.00, 23.20, 'BB/TB', 'perempuan', NULL, NULL),
(630, '107', 13.40, 14.60, 15.90, 17.50, 19.20, 21.20, 23.50, 'BB/TB', 'perempuan', NULL, NULL),
(631, '107.5', 13.50, 14.70, 16.10, 17.70, 19.40, 21.40, 23.70, 'BB/TB', 'perempuan', NULL, NULL),
(632, '108', 13.70, 14.90, 16.30, 17.80, 19.60, 21.70, 24.00, 'BB/TB', 'perempuan', NULL, NULL),
(633, '108.5', 13.80, 15.00, 16.40, 18.00, 19.80, 21.90, 24.30, 'BB/TB', 'perempuan', NULL, NULL),
(634, '109', 13.90, 15.20, 16.60, 18.20, 20.00, 22.10, 24.50, 'BB/TB', 'perempuan', NULL, NULL),
(635, '109.5', 14.10, 15.40, 16.80, 18.40, 20.30, 22.40, 24.80, 'BB/TB', 'perempuan', NULL, NULL),
(636, '110', 14.20, 15.50, 17.00, 18.60, 20.50, 22.60, 25.10, 'BB/TB', 'perempuan', NULL, NULL),
(637, '110.5', 14.40, 15.70, 17.10, 18.80, 20.70, 22.90, 25.40, 'BB/TB', 'perempuan', NULL, NULL),
(638, '111', 14.50, 15.80, 17.30, 19.00, 20.90, 23.10, 25.70, 'BB/TB', 'perempuan', NULL, NULL),
(639, '111.5', 14.70, 16.00, 17.50, 19.20, 21.20, 23.40, 26.00, 'BB/TB', 'perempuan', NULL, NULL),
(640, '112', 14.80, 16.20, 17.70, 19.40, 21.40, 23.60, 26.20, 'BB/TB', 'perempuan', NULL, NULL),
(641, '112.5', 15.00, 16.30, 17.90, 19.60, 21.60, 23.90, 26.50, 'BB/TB', 'perempuan', NULL, NULL),
(642, '113', 15.10, 16.50, 18.00, 19.80, 21.80, 24.20, 26.80, 'BB/TB', 'perempuan', NULL, NULL),
(643, '113.5', 15.30, 16.70, 18.20, 20.00, 22.10, 24.40, 27.10, 'BB/TB', 'perempuan', NULL, NULL),
(644, '114', 15.40, 16.80, 18.40, 20.20, 22.30, 24.70, 27.40, 'BB/TB', 'perempuan', NULL, NULL),
(645, '114.5', 15.60, 17.00, 18.60, 20.50, 22.60, 25.00, 27.80, 'BB/TB', 'perempuan', NULL, NULL),
(646, '115', 15.70, 17.20, 18.80, 20.70, 22.80, 25.20, 28.10, 'BB/TB', 'perempuan', NULL, NULL),
(647, '115.5', 15.90, 17.30, 19.00, 20.90, 23.00, 25.50, 28.40, 'BB/TB', 'perempuan', NULL, NULL),
(648, '116', 16.00, 17.50, 19.20, 21.10, 23.30, 25.80, 28.70, 'BB/TB', 'perempuan', NULL, NULL),
(649, '116.5', 16.20, 17.70, 19.40, 21.30, 23.50, 26.10, 29.00, 'BB/TB', 'perempuan', NULL, NULL),
(650, '117', 16.30, 17.80, 19.60, 21.50, 23.80, 26.30, 29.30, 'BB/TB', 'perempuan', NULL, NULL),
(651, '117.5', 16.50, 18.00, 19.80, 12.70, 24.00, 26.60, 29.60, 'BB/TB', 'perempuan', NULL, NULL),
(652, '118', 16.60, 18.20, 19.90, 22.00, 24.20, 26.90, 29.90, 'BB/TB', 'perempuan', NULL, NULL),
(653, '118.5', 16.80, 18.40, 20.10, 22.20, 24.50, 27.20, 30.30, 'BB/TB', 'perempuan', NULL, NULL),
(654, '119', 16.90, 18.50, 20.30, 22.40, 24.70, 27.40, 30.60, 'BB/TB', 'perempuan', NULL, NULL),
(655, '119.5', 17.10, 18.70, 20.50, 22.60, 25.00, 27.70, 30.90, 'BB/TB', 'perempuan', NULL, NULL),
(656, '120', 17.30, 18.90, 20.70, 22.80, 25.20, 28.00, 31.20, 'BB/TB', 'perempuan', NULL, NULL),
(657, '24', 78.00, 81.00, 84.10, 87.10, 90.20, 93.20, 96.30, 'TB/U', 'laki-laki', NULL, NULL),
(658, '25', 78.60, 81.70, 84.90, 88.00, 91.10, 94.20, 97.30, 'TB/U', 'laki-laki', NULL, NULL),
(659, '26', 79.30, 82.50, 85.60, 88.80, 92.00, 95.20, 98.30, 'TB/U', 'laki-laki', NULL, NULL),
(660, '27', 79.90, 83.10, 86.40, 89.60, 92.90, 96.10, 99.30, 'TB/U', 'laki-laki', NULL, NULL),
(661, '28', 80.50, 83.80, 87.10, 90.40, 93.70, 97.00, 100.30, 'TB/U', 'laki-laki', NULL, NULL),
(662, '29', 81.10, 84.50, 87.80, 91.20, 94.50, 97.90, 101.20, 'TB/U', 'laki-laki', NULL, NULL),
(663, '30', 81.70, 85.10, 88.50, 91.90, 95.30, 98.70, 102.10, 'TB/U', 'laki-laki', NULL, NULL),
(664, '31', 82.30, 85.70, 89.20, 92.70, 96.10, 99.60, 103.00, 'TB/U', 'laki-laki', NULL, NULL),
(665, '32', 82.80, 86.40, 89.90, 93.40, 96.90, 100.40, 103.90, 'TB/U', 'laki-laki', NULL, NULL),
(666, '33', 83.40, 86.90, 90.50, 94.10, 97.60, 101.20, 104.80, 'TB/U', 'laki-laki', NULL, NULL),
(667, '34', 83.90, 87.50, 91.10, 94.80, 98.40, 102.00, 105.60, 'TB/U', 'laki-laki', NULL, NULL),
(668, '35', 84.40, 88.10, 91.80, 95.40, 99.10, 102.70, 106.40, 'TB/U', 'laki-laki', NULL, NULL),
(669, '36', 85.00, 88.70, 92.40, 96.10, 99.80, 103.50, 107.20, 'TB/U', 'laki-laki', NULL, NULL),
(670, '37', 85.50, 89.20, 93.00, 96.70, 100.50, 104.20, 108.00, 'TB/U', 'laki-laki', NULL, NULL),
(671, '38', 86.00, 89.80, 93.60, 97.40, 101.20, 105.00, 108.80, 'TB/U', 'laki-laki', NULL, NULL),
(672, '39', 86.50, 90.30, 94.20, 98.00, 101.80, 105.70, 109.50, 'TB/U', 'laki-laki', NULL, NULL),
(673, '40', 87.00, 90.90, 94.70, 98.60, 102.50, 106.40, 110.30, 'TB/U', 'laki-laki', NULL, NULL),
(674, '41', 87.50, 91.40, 95.30, 99.20, 103.20, 107.10, 111.00, 'TB/U', 'laki-laki', NULL, NULL),
(675, '42', 88.00, 91.90, 95.90, 99.90, 103.80, 107.80, 111.70, 'TB/U', 'laki-laki', NULL, NULL),
(676, '43', 88.40, 92.40, 96.40, 100.40, 104.50, 108.50, 112.50, 'TB/U', 'laki-laki', NULL, NULL),
(677, '44', 88.90, 93.00, 97.00, 101.00, 105.10, 109.10, 113.20, 'TB/U', 'laki-laki', NULL, NULL),
(678, '45', 89.40, 93.50, 97.50, 101.60, 105.70, 109.80, 113.90, 'TB/U', 'laki-laki', NULL, NULL),
(679, '46', 89.80, 94.00, 98.10, 102.20, 106.30, 110.40, 114.60, 'TB/U', 'laki-laki', NULL, NULL),
(680, '47', 90.30, 94.40, 98.60, 102.80, 106.90, 111.10, 115.20, 'TB/U', 'laki-laki', NULL, NULL),
(681, '48', 90.70, 94.90, 99.10, 103.30, 107.50, 111.70, 115.90, 'TB/U', 'laki-laki', NULL, NULL),
(682, '49', 91.20, 95.40, 99.70, 103.90, 108.10, 112.40, 116.60, 'TB/U', 'laki-laki', NULL, NULL),
(683, '50', 91.60, 95.90, 100.20, 104.40, 108.70, 113.00, 117.30, 'TB/U', 'laki-laki', NULL, NULL),
(684, '51', 92.10, 96.40, 100.70, 105.00, 109.30, 113.60, 117.90, 'TB/U', 'laki-laki', NULL, NULL),
(685, '52', 92.50, 96.90, 101.20, 105.60, 109.90, 114.20, 118.60, 'TB/U', 'laki-laki', NULL, NULL),
(686, '53', 93.00, 97.40, 101.70, 106.10, 110.50, 114.90, 119.20, 'TB/U', 'laki-laki', NULL, NULL),
(687, '54', 93.40, 97.80, 102.30, 106.70, 111.10, 115.50, 119.90, 'TB/U', 'laki-laki', NULL, NULL),
(688, '55', 93.90, 98.30, 102.80, 107.20, 111.70, 116.10, 120.60, 'TB/U', 'laki-laki', NULL, NULL),
(689, '56', 94.30, 98.80, 103.30, 107.80, 112.30, 116.70, 121.20, 'TB/U', 'laki-laki', NULL, NULL),
(690, '57', 94.70, 99.30, 103.80, 108.30, 112.80, 117.40, 121.90, 'TB/U', 'laki-laki', NULL, NULL),
(691, '58', 95.20, 99.70, 104.30, 108.90, 113.40, 118.00, 122.60, 'TB/U', 'laki-laki', NULL, NULL),
(692, '59', 95.60, 100.20, 104.80, 109.40, 114.00, 118.60, 123.20, 'TB/U', 'laki-laki', NULL, NULL),
(693, '60', 96.10, 100.70, 105.30, 110.00, 114.60, 119.20, 123.90, 'TB/U', 'laki-laki', NULL, NULL),
(694, '24', 76.00, 79.30, 82.50, 85.70, 88.90, 92.20, 95.40, 'TB/U', 'perempuan', NULL, NULL),
(695, '25', 76.80, 80.00, 83.30, 86.60, 89.90, 93.10, 96.40, 'TB/U', 'perempuan', NULL, NULL),
(696, '26', 77.50, 80.80, 84.10, 87.40, 90.80, 94.10, 97.40, 'TB/U', 'perempuan', NULL, NULL),
(697, '27', 78.10, 81.50, 84.90, 88.30, 91.70, 95.00, 98.40, 'TB/U', 'perempuan', NULL, NULL),
(698, '28', 78.80, 82.20, 85.70, 89.10, 92.50, 96.00, 99.40, 'TB/U', 'perempuan', NULL, NULL),
(699, '29', 79.50, 82.90, 86.40, 89.90, 93.40, 96.90, 100.30, 'TB/U', 'perempuan', NULL, NULL),
(700, '30', 80.10, 83.60, 87.10, 90.70, 94.20, 97.70, 101.30, 'TB/U', 'perempuan', NULL, NULL),
(701, '31', 80.70, 84.30, 87.90, 91.40, 95.00, 98.60, 102.20, 'TB/U', 'perempuan', NULL, NULL),
(702, '32', 81.30, 84.90, 88.60, 92.20, 95.80, 99.40, 103.10, 'TB/U', 'perempuan', NULL, NULL),
(703, '33', 81.90, 85.60, 89.30, 92.90, 96.60, 100.30, 103.90, 'TB/U', 'perempuan', NULL, NULL),
(704, '34', 82.50, 86.20, 89.90, 93.60, 97.40, 101.10, 104.80, 'TB/U', 'perempuan', NULL, NULL),
(705, '35', 83.10, 86.80, 90.60, 94.40, 98.10, 101.90, 105.60, 'TB/U', 'perempuan', NULL, NULL),
(706, '36', 83.60, 87.40, 91.20, 95.10, 98.90, 102.70, 106.50, 'TB/U', 'perempuan', NULL, NULL),
(707, '37', 84.20, 88.00, 91.90, 95.70, 99.60, 103.40, 107.30, 'TB/U', 'perempuan', NULL, NULL),
(708, '38', 84.70, 88.60, 92.50, 96.40, 100.30, 104.20, 108.10, 'TB/U', 'perempuan', NULL, NULL),
(709, '39', 85.30, 89.20, 93.10, 97.10, 101.00, 105.00, 108.90, 'TB/U', 'perempuan', NULL, NULL),
(710, '40', 85.80, 89.80, 93.80, 97.70, 101.70, 105.70, 109.70, 'TB/U', 'perempuan', NULL, NULL),
(711, '41', 86.30, 90.40, 94.40, 98.40, 102.40, 106.40, 110.50, 'TB/U', 'perempuan', NULL, NULL),
(712, '42', 86.80, 90.90, 95.00, 99.00, 103.10, 107.20, 111.30, 'TB/U', 'perempuan', NULL, NULL),
(713, '43', 87.40, 91.50, 95.60, 99.70, 103.80, 107.90, 112.00, 'TB/U', 'perempuan', NULL, NULL),
(714, '44', 87.90, 92.00, 96.20, 100.30, 104.50, 108.60, 112.70, 'TB/U', 'perempuan', NULL, NULL),
(715, '45', 88.40, 92.50, 96.70, 100.90, 105.10, 109.30, 113.50, 'TB/U', 'perempuan', NULL, NULL),
(716, '46', 88.90, 93.10, 97.30, 101.50, 105.80, 110.00, 114.20, 'TB/U', 'perempuan', NULL, NULL),
(717, '47', 89.30, 93.60, 97.90, 102.10, 106.40, 110.70, 114.90, 'TB/U', 'perempuan', NULL, NULL),
(718, '48', 89.80, 94.10, 98.40, 102.70, 107.00, 111.30, 115.70, 'TB/U', 'perempuan', NULL, NULL),
(719, '49', 90.30, 94.60, 99.00, 103.30, 107.70, 112.00, 116.40, 'TB/U', 'perempuan', NULL, NULL),
(720, '50', 90.70, 95.10, 99.50, 103.90, 108.30, 112.70, 117.10, 'TB/U', 'perempuan', NULL, NULL),
(721, '51', 91.20, 95.60, 100.10, 104.50, 108.90, 113.30, 117.70, 'TB/U', 'perempuan', NULL, NULL),
(722, '52', 91.70, 96.10, 100.60, 105.00, 109.50, 114.00, 118.40, 'TB/U', 'perempuan', NULL, NULL),
(723, '53', 92.10, 96.60, 101.10, 105.60, 110.10, 114.60, 119.10, 'TB/U', 'perempuan', NULL, NULL),
(724, '54', 92.60, 97.10, 101.60, 106.20, 110.70, 115.20, 119.80, 'TB/U', 'perempuan', NULL, NULL),
(725, '55', 93.00, 97.60, 102.20, 106.70, 111.30, 115.90, 120.40, 'TB/U', 'perempuan', NULL, NULL),
(726, '56', 93.40, 98.10, 102.70, 107.30, 111.90, 116.50, 121.10, 'TB/U', 'perempuan', NULL, NULL),
(727, '57', 93.90, 98.50, 103.20, 107.80, 112.50, 117.10, 121.80, 'TB/U', 'perempuan', NULL, NULL),
(728, '58', 94.30, 99.00, 103.70, 108.40, 113.00, 117.70, 122.40, 'TB/U', 'perempuan', NULL, NULL),
(729, '59', 94.70, 99.50, 104.20, 108.90, 113.60, 118.30, 123.10, 'TB/U', 'perempuan', NULL, NULL),
(730, '60', 95.20, 99.90, 104.70, 109.40, 114.20, 118.90, 123.70, 'TB/U', 'perempuan', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_gizi`
--

CREATE TABLE `status_gizi` (
  `id_status_gizi` int(11) NOT NULL,
  `status_gizi` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `status_gizi`
--

INSERT INTO `status_gizi` (`id_status_gizi`, `status_gizi`) VALUES
(1, 'Sangat Kurang'),
(2, 'Kurang'),
(3, 'Lebih'),
(4, 'Sangat Pendek'),
(5, 'Pendek'),
(6, 'Sangat Kurus'),
(7, 'Kurus'),
(8, 'Risiko Gemuk'),
(9, 'Gemuk'),
(10, 'ObesitaS'),
(11, 'Normal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'rezaadmin', '$2y$10$PL/5/FzgAhMTqhbvwi8JEuTeTsPloBc669Co2EcONNaqSm7FdOb6S', NULL, '2020-02-29 06:01:17', '2020-02-29 06:01:17');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id_desa`),
  ADD KEY `desa_id_desa_index` (`id_desa`),
  ADD KEY `desa_id_kecamatan_foreign` (`id_kecamatan`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`),
  ADD KEY `kecamatan_id_kecamatan_index` (`id_kecamatan`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`),
  ADD KEY `pasien_id_pasien_index` (`id_pasien`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD KEY `pemeriksaan_no_pemeriksaan_index` (`no_pemeriksaan`),
  ADD KEY `pemeriksaan_id_pasien_foreign` (`id_pasien`),
  ADD KEY `pemeriksaan_id_kecamatan_foreign` (`id_kecamatan`),
  ADD KEY `pemeriksaan_id_posyandu_foreign` (`id_posyandu`);

--
-- Indeks untuk tabel `posyandu`
--
ALTER TABLE `posyandu`
  ADD PRIMARY KEY (`id_posyandu`),
  ADD KEY `posyandu_id_posyandu_index` (`id_posyandu`),
  ADD KEY `posyandu_id_desa_foreign` (`id_desa`);

--
-- Indeks untuk tabel `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`id_rule`),
  ADD KEY `id_kategori` (`id_kategori`),
  ADD KEY `id_status_gizi` (`id_status_gizi`);

--
-- Indeks untuk tabel `rule_pemeriksaan`
--
ALTER TABLE `rule_pemeriksaan`
  ADD KEY `no_pemeriksaan` (`no_pemeriksaan`),
  ADD KEY `id_rule` (`id_rule`);

--
-- Indeks untuk tabel `standar_pertumbuhan`
--
ALTER TABLE `standar_pertumbuhan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `status_gizi`
--
ALTER TABLE `status_gizi`
  ADD PRIMARY KEY (`id_status_gizi`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_id_user_index` (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `posyandu`
--
ALTER TABLE `posyandu`
  MODIFY `id_posyandu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `rule`
--
ALTER TABLE `rule`
  MODIFY `id_rule` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `standar_pertumbuhan`
--
ALTER TABLE `standar_pertumbuhan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1372;

--
-- AUTO_INCREMENT untuk tabel `status_gizi`
--
ALTER TABLE `status_gizi`
  MODIFY `id_status_gizi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `desa`
--
ALTER TABLE `desa`
  ADD CONSTRAINT `desa_id_kecamatan_foreign` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`);

--
-- Ketidakleluasaan untuk tabel `pemeriksaan`
--
ALTER TABLE `pemeriksaan`
  ADD CONSTRAINT `pemeriksaan_id_kecamatan_foreign` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`),
  ADD CONSTRAINT `pemeriksaan_id_pasien_foreign` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`),
  ADD CONSTRAINT `pemeriksaan_id_posyandu_foreign` FOREIGN KEY (`id_posyandu`) REFERENCES `posyandu` (`id_posyandu`);

--
-- Ketidakleluasaan untuk tabel `posyandu`
--
ALTER TABLE `posyandu`
  ADD CONSTRAINT `posyandu_id_desa_foreign` FOREIGN KEY (`id_desa`) REFERENCES `desa` (`id_desa`);

--
-- Ketidakleluasaan untuk tabel `rule`
--
ALTER TABLE `rule`
  ADD CONSTRAINT `rule_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`),
  ADD CONSTRAINT `rule_status_gizi` FOREIGN KEY (`id_status_gizi`) REFERENCES `status_gizi` (`id_status_gizi`);

--
-- Ketidakleluasaan untuk tabel `rule_pemeriksaan`
--
ALTER TABLE `rule_pemeriksaan`
  ADD CONSTRAINT `rule_pemeriksaan_pemeriksaan` FOREIGN KEY (`no_pemeriksaan`) REFERENCES `pemeriksaan` (`no_pemeriksaan`),
  ADD CONSTRAINT `rule_pemeriksaan_rule` FOREIGN KEY (`id_rule`) REFERENCES `rule` (`id_rule`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
