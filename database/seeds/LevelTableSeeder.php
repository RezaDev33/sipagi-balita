<?php

use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO 'level' ('id_level', 'ket_level') VALUES
        ('1', 'Admin'),
        ('2', 'Puskesmas'),
        ('3', 'Posyandu');
        ");
    }
}
