<?php

use Illuminate\Database\Seeder;

class SolusiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
        DB::table('solusi')->insert([
            'status' => 'Sangat Kecil',
            'penyebab' => 'Pola makan yang kurang sehat dan gizi kurang seimbang',
            'solusi' => 'Perbaiki pola makan yang sehat dan gizi seimbang',
        ]);
    }
}
