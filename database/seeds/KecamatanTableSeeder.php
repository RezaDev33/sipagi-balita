<?php

use Illuminate\Database\Seeder;

class KecamatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("INSERT INTO 'kecamatan' ('id_kecamatan', 'nm_kecamatan') VALUES
        ('021801', 'HAURGEULIS'),
        ('021802', 'KROYA'),
        ('021803', 'GABUSWETAN'),
        ('021804', 'CIKEDUNG'),
        ('021805', 'LELEA'),
        ('021806', 'BANGODUA'),
        ('021807', 'WIDASARI'),
        ('021808', 'KERTASEMAYA'),
        ('021809', 'KRANGKENG'),
        ('021810', 'KARANGAMPEL'),
        ('021811', 'JUNTINYUAT'),
        ('021812', 'SLIYEG'),
        ('021813', 'JATIBARANG'),
        ('021814', 'BALONGAN'),
        ('021815', 'INDRAMAYU'),
        ('021816', 'SINDANG'),
        ('021817', 'LOHBENER'),
        ('021818', 'LOSARANG'),
        ('021819', 'KANDANGHAUR'),
        ('021820', 'BONGAS'),
        ('021821', 'ANJATAN'),
        ('021822', 'SUKRA'),
        ('021824', 'CANTIGI'),
        ('021825', 'GANTAR'),
        ('021826', 'TERISI'),
        ('021827', 'SUKAGUMIWANG'),
        ('021828', 'KEDOKAN BUNDER'),
        ('021829', 'PASEKAN'),
        ('021830', 'TUKDANA'),
        ('021831', 'PATROL');
        ");
    }
}
