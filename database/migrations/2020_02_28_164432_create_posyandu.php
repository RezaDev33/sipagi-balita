<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePosyandu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posyandu', function (Blueprint $table) {
            Schema::dropIfExists('posyandu');
            $table->integer('id_posyandu')->autoIncrement();
            $table->string('nm_posyandu');
            $table->string('password_posyandu');
            $table->string('id_desa');

            $table->index('id_posyandu');
            $table->foreign('id_desa')
                ->references('id_desa')->on('desa')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posyandu');
    }
}
