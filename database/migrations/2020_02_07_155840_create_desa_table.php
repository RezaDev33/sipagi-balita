<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desa', function (Blueprint $table) {
            Schema::dropIfExists('desa');
            $table->string('id_desa');
            $table->integer('id_kecamatan');
            $table->string('nm_desa');

            $table->index('id_desa');
            $table->foreign('id_kecamatan')
                ->references('id_kecamatan')->on('kecamatan')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desa');
    }
}
