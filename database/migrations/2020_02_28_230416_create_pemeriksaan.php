<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemeriksaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemeriksaan', function (Blueprint $table) {
            Schema::dropIfExists('pemeriksaan');
            // $table->integer('id_pemeriksaan')->autoIncrement();
            $table->string('no_pemeriksaan'); // kode kecamatan 2 digit, tanggal 2 digit, bulan 2 digit, tahun 2 digit, id_posyandu 4 digit, urutan 3 digit
            $table->integer('id_status_gizi');
            $table->integer('id_pasien');
            $table->integer('id_kecamatan');
            $table->integer('id_posyandu');
            $table->string('bb_tb');
            $table->string('bb_u');
            $table->string('bb_pb');
            $table->string('tb_u');
            $table->string('pb_u');
            $table->string('validasi');
            $table->date('tgl_pemeriksaan');
            $table->integer('umur');
            $table->double('bb');
            $table->double('tb');
            $table->double('pb');
            $table->string('asi_eks');
            $table->string('vit_a');
            $table->string('cara_ukur');

            $table->index('no_pemeriksaan');
            $table->foreign('id_status_gizi')
                ->references('id_status_gizi')->on('status_gizi')
                ->onDelete('restrict');
            $table->foreign('id_user')
                ->references('id_user')->on('users')
                ->onDelete('restrict');
            $table->foreign('id_pasien')
                ->references('id_pasien')->on('pasien')
                ->onDelete('restrict');
            $table->foreign('id_kecamatan')
                ->references('id_kecamatan')->on('kecamatan')
                ->onDelete('restrict');
            $table->foreign('id_posyandu')
                ->references('id_posyandu')->on('posyandu')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemeriksaan');
    }
}
