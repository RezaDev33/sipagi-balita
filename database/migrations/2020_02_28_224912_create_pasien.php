<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePasien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasien', function (Blueprint $table) {
            Schema::dropIfExists('pasien');
            $table->integer('id_pasien')->autoIncrement();
            $table->string('nik');
            $table->string('no_kk');
            $table->integer('anak_ke');
            $table->string('nm_pasien');
            $table->date('tgl_lahir');
            $table->string('jenis_kelamin');
            $table->string('status_ekonomi');
            $table->double('bb_lahir');
            $table->double('tb_lahir');
            $table->string('nm_ortu');
            $table->string('nik_ayah');
            $table->string('tlp_ortu');
            $table->string('imd');
            $table->string('kia');
            $table->text('alamat');
            $table->timestamps();
            
            $table->index('id_pasien');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasien');
    }
}
