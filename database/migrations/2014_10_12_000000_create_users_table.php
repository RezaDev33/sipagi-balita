<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            Schema::dropIfExists('users');
            $table->integer('id_user')->autoIncrement();
            $table->string('username')->unique();
            $table->string('password');
            // $table->integer('id_level');
            $table->rememberToken();

            $table->index('id_user');
            // $table->foreign('id_level')
            //     ->references('id_level')->on('level')
            //     ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
