<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStandarPertumbuhan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standar_pertumbuhan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parameter');
            $table->double('sdmintiga');
            $table->double('sdmindua');
            $table->double('sdminsatu');
            $table->double('median');
            $table->double('sdplussatu');
            $table->double('sdplusdua');
            $table->double('sdplustiga');
            $table->string('tipe_tabel');
            $table->string('jenis_kelamin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('standar_pertumbuhan');
    }
}
