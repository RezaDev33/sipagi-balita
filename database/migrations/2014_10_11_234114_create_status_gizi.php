<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusGizi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_gizi', function (Blueprint $table) {
            Schema::dropIfExists('status_gizi');
            $table->integer('id_status_gizi')->autoIncrement();
            $table->string('status_gizi');

            $table->index('id_status_gizi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_gizi');
    }
}
