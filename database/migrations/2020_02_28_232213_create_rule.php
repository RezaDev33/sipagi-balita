<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rule', function (Blueprint $table) {
            Schema::dropIfExists('rule');
            $table->integer('id_rule')->autoIncrement();
            $table->integer('id_status_gizi');
            $table->string('bb_u');
            $table->string('tb_u');
            $table->string('bb_tb');

            $table->index('id_rule');
            $table->foreign('id_status_gizi')
                ->references('id_status_gizi')->on('status_gizi')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rule');
    }
}
