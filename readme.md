## Sipagi Balita

![logo](/public/img/logo.png "Logo")

Sipagi adalah aplikasi sistem pakar gizi yang bertujuan untuk mendiagnosa masalah gizi pada anak balita seperti Stunting. Aplikasi ini dibuat dengan mengunakan framework [Laravel](https://laravel.com/docs/7.x) dan menerapkan algoritma fuzzy tsukamoto untuk melakukan prediksi status gizi berdasarkan pertumbuhan balita.

### App Preview
![example 1](/public/img/sipagi1.jpg "example 2")
![example 2](/public/img/sipagi2.jpg "example 2")
