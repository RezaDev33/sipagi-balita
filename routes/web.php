<?php

Route::get('/', function () {
    return view('landing');
});

// Route::get('/', function () {
//     return view('auth.login');
// });

// Route::get('/register', function () {
//     return view('auth.register');
// });

Auth::routes();

//JADI INI GROUPING ROUTE, SEHINGGA SEMUA ROUTE YANG ADA DIDALAMNYA
//SECARA OTOMATIS AKAN DIAWALI DENGAN administrator
//CONTOH: /administrator/category ATAU /administrator/barang
Route::group([ 'middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index')->name('home'); 
    //route diagnosis
    Route::resource('diagnosis', 'DiagnosaController')->except(['create', 'show']);
    Route::get('diagnosis/laporan','DiagnosaController@report');
    Route::post('diagnosis/proses', 'DiagnosaController@proses');

    //route diagnosis untuk pasien
    Route::post('diagnosis/pasien', 'DiagnosaPasienController@Index')->name('diagnosis.data');

    //route standar pertumbuhan
    Route::get('bbu','StandarController@indexBbu')->name('bbu.data');
    Route::get('pbu','StandarController@indexPbu')->name('pbu.data');
    Route::get('tbu','StandarController@indexTbu')->name('tbu.data');
    Route::get('bbpb','StandarController@indexBbpb')->name('bbpb.data');
    Route::get('bbtb','StandarController@indexBbtb')->name('bbtb.data');

    //route pasien
    Route::get('/pasien','PasienController@index')->name('pasien.index');
    Route::get('/pasien/add','PasienController@create');
    Route::post('/pasien/store','PasienController@store');
    Route::get('/pasien/edit/{id_pasien}','PasienController@edit');
    Route::post('/pasien/update/{id_pasien}','PasienController@update')->name('update');
    Route::get('/pasien/delete/{id_pasien}','PasienController@delete');
    Route::get('/pasien/detail/{id_pasien}','PasienController@detail');

    //rule
    Route::get('rule', 'RuleController@Index')->name('rule.data');
    Route::post('rule/add', 'RuleController@store')->name('rule.store');
    Route::get('rule/edit/{id}', 'RuleController@edit');
    Route::post('rule/update', 'RuleController@update')->name('rule.update');
    Route::get('rule/destroy/{id}', 'RuleController@destroy');

    //solusi
    Route::get('/solusi','SolusiController@index');
    Route::get('/solusi/edit/{id}','SolusiController@edit');
    Route::put('/solusi/update/{id}','SolusiController@update');

    //rekap status gizi
    Route::get('rekap', 'RekapController@Index')->name('rekap.data');
    Route::get('export', 'RekapController@export')->name('export');

    //user
    Route::resource('user', 'UserController')->except(['show']);
    Route::post('/user/store', 'UserController@store');
    //history
    Route::resource('history', 'HistoryController')->except(['create', 'show']);

    Route::get('/alamat/{kecamatan}','GiziController@a');

    Route::resource('coba', 'PasienController');
});

// Route::group([ 'middleware' => ['auth','checkLevel:admin']], function() {
//     //user
//     Route::resource('user', 'UserController')->except(['show']);
// });
