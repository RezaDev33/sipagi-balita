<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemeriksaan extends Model
{
    protected $table = "pemeriksaan";
    protected $fillable = ['id_pasien','id_kecamatan','id_posyandu','bb_tb','bb_u','bb_pb','tb_u','pb_u','validasi','tgl_pemeriksaan','umur','asi_eks','vit_a','cara_ukur','bb','pb','diagnosis_bbu','diagnosis_pbu','diagnosis_bbpb'];
}
