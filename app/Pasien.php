<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $primaryKey = 'id_pasien';
    protected $table = 'pasien';
    protected $fillable = ['nik','no_kk','anak_ke','nm_pasien','tgl_lahir','status_ekonomi','jenis_kelamin','bb_lahir','tb_lahir','nm_ortu','nik_ayah','imd','kia','tlp_ortu','alamat'];


}
