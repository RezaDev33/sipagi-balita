<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $table = "rule";

    protected $fillable = ['id_status_gizi','id_kategori','batas'];

}
