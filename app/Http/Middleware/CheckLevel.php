<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $level)
    {
        $level_user = DB::table('level')->select('ket_level')->where('id_level', $request->user()->id_level)->first();
        if($level_user->ket_level == $level){
            return $next($request);
        }

        return redirect('/');
    }
}
