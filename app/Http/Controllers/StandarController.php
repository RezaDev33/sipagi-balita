<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use Illuminate\Http\Request;
use DataTables;

class StandarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexBbu(Request $request)
    {
        if($request->ajax())
        {
            $data = Diagnosis::where('tipe_tabel', 'BB/U')->orderByRaw('jenis_kelamin ASC');
            return DataTables::of($data)->make(true);
        }
        return view('standar.bbu');
    }

    public function indexPbu(Request $request)
    {
        if($request->ajax())
        {
            $data = Diagnosis::where('tipe_tabel', 'PB/U')->orderByRaw('jenis_kelamin ASC');
            return DataTables::of($data)->make(true);
        }
        return view('standar.pbu');
    }

    public function indexTbu(Request $request)
    {
        if($request->ajax())
        {
            $data = Diagnosis::where('tipe_tabel', 'TB/U')->orderByRaw('jenis_kelamin ASC');
            return DataTables::of($data)->make(true);
        }
        return view('standar.tbu');
    }

    public function indexBbpb(Request $request)
    {
        if($request->ajax())
        {
            $data = Diagnosis::where('tipe_tabel', 'BB/PB')->orderByRaw('jenis_kelamin ASC');
            return DataTables::of($data)->make(true);
        }
        return view('standar.bbpb');
    }

    public function indexBbtb(Request $request)
    {
        if($request->ajax())
        {
            $data = Diagnosis::where('tipe_tabel', 'BB/TB')->orderByRaw('jenis_kelamin ASC');
            return DataTables::of($data)->make(true);
        }
        return view('standar.bbtb');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function show(Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function edit(Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diagnosis  $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diagnosis $diagnosis)
    {
        //
    }
}
