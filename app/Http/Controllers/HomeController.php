<?php

namespace App\Http\Controllers;

use App\Pemeriksaan;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sangat_kurang = Pemeriksaan::where('diagnosis_bbu','Sangat Kurang')->get();
        $sangat_kurang1 = count($sangat_kurang);

        $kurang = Pemeriksaan::where('diagnosis_bbu','Kurang')->get();
        $kurang1 = count($kurang);

        $lebih = Pemeriksaan::where('diagnosis_bbu','Lebih')->get();
        $lebih1 = count($lebih);

        $sangat_pendek = Pemeriksaan::where('diagnosis_pbu','Sangat Pendek')->get();
        $sangat_pendek1 = count($sangat_pendek);

        $pendek = Pemeriksaan::where('diagnosis_pbu','Pendek')->get();
        $pendek1 = count($pendek);

        $sangat_kurus = Pemeriksaan::where('diagnosis_bbpb','Sangat Kurus')->get();
        $sangat_kurus1 = count($sangat_kurus);

        $kurus = Pemeriksaan::where('diagnosis_bbpb','Kurus')->get();
        $kurus1 = count($kurus);

        $risiko_gemuk = Pemeriksaan::where('diagnosis_bbpb','Risiko Gemuk')->get();
        $risiko_gemuk1 = count($risiko_gemuk);

        $gemuk = Pemeriksaan::where('diagnosis_bbpb','Gemuk')->get();
        $gemuk1 = count($gemuk);

        $obesitas = Pemeriksaan::where('diagnosis_bbpb','Obesitas')->get();
        $obesitas1 = count($obesitas);

        $normal = Pemeriksaan::where('diagnosis_bbu','Normal')->get();
        $normal1 = count($normal);

        return view('home',['sangat_kurang'=>$sangat_kurang1,'kurang'=>$kurang1,'lebih'=>$lebih1,
        'sangat_pendek'=>$sangat_pendek1,'pendek'=>$pendek1,'sangat_kurus'=>$sangat_kurus1,'kurus'=>$kurus1,
        'risiko_gemuk'=>$risiko_gemuk1,'gemuk'=>$gemuk1,'obesitas'=>$obesitas1,'normal'=>$normal1]);
    }
}
