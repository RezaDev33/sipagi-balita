<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diagnosis;

class DiagnosisController extends Controller
{
    public function index(){
        return view('diagnosa/index');
    }

    public function store(Request $request){
        return $request;
    }

    public function proses(){
        // Ini tidak digunakan, bairkan saja untuk contoh
        // Value Input Fuzzy
        $bb = 3.2;
        $tb = 49.8;

        // Data BB/U
        $bbu_min3 = 2.1;
        $bbu_min2 = 2.5;
        $bbu_min1 = 2.9;
        $bbu_median = 3.3;
        $bbu_plus1 = 3.9;
        $bbu_plus2 = 4.4;
        $bbu_plus3 = 5.0;

        // Data TB/U
        $tbu_min3 = 44.2;
        $tbu_min2 = 46.1;
        $tbu_min1 = 48.0;
        $tbu_median = 49.9;
        $tbu_plus1 = 51.8;
        $tbu_plus2 = 53.7;
        $tbu_plus3 = 55.6;

        // Kelainan
        $sangat_pendek = 0.1;
        $sangat_kurus = 0.2;
        $gizi_buruk = 0.3;
        $stunting = 0.4;
        $indikasi_gizi_buruk = 0.5;
        $indikasi_stunting = 0.6;
        $normal = 0.7;
        $obesitas = 0.8;
        $unknow = 0.9;

        // Himpunan Fuzzy
        // Himpunan IF TB/U
        $himpuan_tbu_min3 = $this->trapesiumTurun($tbu_min3,$tbu_min2,$tb);
        $himpuan_tbu_min2 = $this->segitiga($tbu_min3,$tbu_min2,$tbu_min1,$tb);
        $himpuan_tbu_min1 = $this->segitiga($tbu_min2,$tbu_min1,$tbu_median,$tb);
        $himpuan_tbu_median = $this->segitiga($tbu_min1,$tbu_median,$tbu_plus1,$tb);
        $himpuan_tbu_plus1 = $this->segitiga($tbu_median,$tbu_plus1,$tbu_plus2,$tb);
        $himpuan_tbu_plus2 = $this->segitiga($tbu_plus1,$tbu_plus2,$tbu_plus3,$tb);
        $himpuan_tbu_plus3 = $this->trapesiumNaik($tbu_plus2,$tbu_plus3,$tb);

        // Himpunan IF BB/U
        $himpuan_bbu_min3 = $this->trapesiumTurun($bbu_min3,$bbu_min2,$bb);
        $himpuan_bbu_min2 = $this->segitiga($bbu_min3,$bbu_min2,$bbu_min1,$bb);
        $himpuan_bbu_min1 = $this->segitiga($bbu_min2,$bbu_min1,$bbu_median,$bb);
        $himpuan_bbu_median = $this->segitiga($bbu_min1,$bbu_median,$bbu_plus1,$bb);
        $himpuan_bbu_plus1 = $this->segitiga($bbu_median,$bbu_plus1,$bbu_plus2,$bb);
        $himpuan_bbu_plus2 = $this->segitiga($bbu_plus1,$bbu_plus2,$bbu_plus3,$bb);
        $himpuan_bbu_plus3 = $this->trapesiumNaik($bbu_plus2,$bbu_plus3,$bb);
       
        // Rule 1
        // IF TB/U = -3 SD and BB/U = -3 SD THEN Sangat Pendek 
        $ruleif[0] = min($himpuan_tbu_min3,$himpuan_bbu_min3);
        $rulethen[0] = $this->trapesiumTurunZ($sangat_pendek,$sangat_kurus,$ruleif[0]);

        // Rule 2
        // IF TB/U = -3 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[1] = min($himpuan_tbu_min3,$himpuan_bbu_min2);
        $rulethen[1] = $this->segitigaZ($indikasi_gizi_buruk,$gizi_buruk,$indikasi_stunting,$ruleif[1]);

        // Rule 3
        // IF TB/U = -3 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[2] = min($himpuan_tbu_min3,$himpuan_bbu_min1);
        $rulethen[2] = $this->segitigaZ($sangat_kurus,$indikasi_gizi_buruk,$gizi_buruk,$ruleif[2]);

        // Rule 4
        // IF TB/U = -3 SD and BB/U = median SD THEN Sangat Pendek
        $ruleif[3] = min($himpuan_tbu_min3,$himpuan_bbu_median);
        $rulethen[3] = $this->trapesiumTurunZ($sangat_pendek,$sangat_kurus,$ruleif[3]);

        // Rule 5
        // IF TB/U = -3 SD and BB/U = +1 SD THEN Obesitas
        $ruleif[4] = min($himpuan_tbu_min3,$himpuan_bbu_plus1);
        $rulethen[4] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[4]);
        
        // Rule 6
        // IF TB/U = -3 SD and BB/U = +2 SD THEN Obesitas
        $ruleif[5] = min($himpuan_tbu_min3,$himpuan_bbu_plus2);
        $rulethen[5] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[5]);
        
        // Rule 7
        // IF TB/U = -3 SD and BB/U = +3 SD THEN Obesitas
        $ruleif[6] = min($himpuan_tbu_min3,$himpuan_bbu_plus3);
        $rulethen[6] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[6]);

        // Rule 8
        // IF TB/U = -2 SD and BB/U = -3 SD THEN Stunting
        $ruleif[7] = min($himpuan_tbu_min2,$himpuan_bbu_min3);
        $rulethen[7] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[7]);

        // Rule 9
        // IF TB/U = -2 SD and BB/U = -2 SD THEN Stunting
        $ruleif[8] = min($himpuan_tbu_min2,$himpuan_bbu_min2);
        $rulethen[8] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[8]);

        // Rule 10
        // IF TB/U = -2 SD and BB/U = -1 SD THEN Stunting
        $ruleif[9] = min($himpuan_tbu_min2,$himpuan_bbu_min1);
        $rulethen[9] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[9]);

        // Rule 11
        // IF TB/U = -2 SD and BB/U = median SD THEN Stunting
        $ruleif[10] = min($himpuan_tbu_min2,$himpuan_bbu_median);
        $rulethen[10] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[10]);

        // Rule 12
        // IF TB/U = -2 SD and BB/U = +1 SD THEN Stunting
        $ruleif[11] = min($himpuan_tbu_min2,$himpuan_bbu_plus1);
        $rulethen[11] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[11]);

        // Rule 13
        // IF TB/U = -2 SD and BB/U = +2 SD THEN Stunting
        $ruleif[12] = min($himpuan_tbu_min2,$himpuan_bbu_plus2);
        $rulethen[12] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[12]);

        // Rule 14
        // IF TB/U = -2 SD and BB/U = +3 SD THEN Stunting
        $ruleif[13] = min($himpuan_tbu_min2,$himpuan_bbu_plus3);
        $rulethen[13] = $this->segitigaZ($indikasi_stunting,$stunting,$normal,$ruleif[13]);

        // Rule 15
        // IF TB/U = -1 SD and BB/U = -3 SD THEN Indikasi Stunting
        $ruleif[14] = min($himpuan_tbu_min1,$himpuan_bbu_min3);
        $rulethen[14] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[14]);

        // Rule 16
        // IF TB/U = -1 SD and BB/U = -2 SD THEN Indikasi Stunting
        $ruleif[15] = min($himpuan_tbu_min1,$himpuan_bbu_min2);
        $rulethen[15] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[15]);

        // Rule 17
        // IF TB/U = -1 SD and BB/U = -1 SD THEN Indikasi Stunting
        $ruleif[16] = min($himpuan_tbu_min1,$himpuan_bbu_min1);
        $rulethen[16] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[16]);
        
        // Rule 18
        // IF TB/U = -1 SD and BB/U = median SD THEN Indikasi Stunting
        $ruleif[17] = min($himpuan_tbu_min1,$himpuan_bbu_median);
        $rulethen[17] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[17]);

        // Rule 19
        // IF TB/U = -1 SD and BB/U = +1 SD THEN Indikasi Stunting
        $ruleif[18] = min($himpuan_tbu_min1,$himpuan_bbu_plus1);
        $rulethen[18] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[18]);

        // Rule 20
        // IF TB/U = -1 SD and BB/U = +2 SD THEN Indikasi Stunting
        $ruleif[19] = min($himpuan_tbu_min1,$himpuan_bbu_plus2);
        $rulethen[19] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[19]);

        // Rule 21
        // IF TB/U = -1 SD and BB/U = +3 SD THEN Indikasi Stunting
        $ruleif[20] = min($himpuan_tbu_min1,$himpuan_bbu_plus3);
        $rulethen[20] = $this->segitigaZ($gizi_buruk,$indikasi_stunting,$stunting,$ruleif[20]);

        // Rule 22
        // IF TB/U = median SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[21] = min($himpuan_tbu_median,$himpuan_bbu_min3);
        $rulethen[21] = $this->segitigaZ($sangat_pendek,$sangat_kurus,$indikasi_gizi_buruk,$ruleif[21]);

        // Rule 23
        // IF TB/U = median SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[22] = min($himpuan_tbu_median,$himpuan_bbu_min2);
        $rulethen[22] = $this->segitigaZ($indikasi_gizi_buruk,$gizi_buruk,$indikasi_stunting,$ruleif[22]);

        // Rule 24
        // IF TB/U = median SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[23] = min($himpuan_tbu_median,$himpuan_bbu_min1);
        $rulethen[23] = $this->segitigaZ($sangat_kurus,$indikasi_gizi_buruk,$gizi_buruk,$ruleif[23]);

        // Rule 25
        // IF TB/U = median SD and BB/U = median SD THEN normal
        $ruleif[24] = min($himpuan_tbu_median,$himpuan_bbu_median);
        $rulethen[24] = $this->segitigaZ($stunting,$normal,$obesitas,$ruleif[24]);

        // Rule 26
        // IF TB/U = median SD and BB/U = +1 SD THEN Obesitas
        $ruleif[25] = min($himpuan_tbu_median,$himpuan_bbu_plus1);
        $rulethen[25] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[25]);

        // Rule 27
        // IF TB/U = median SD and BB/U = +2 SD THEN Obesitas
        $ruleif[26] = min($himpuan_tbu_median,$himpuan_bbu_plus2);
        $rulethen[26] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[26]);

        // Rule 28
        // IF TB/U = median SD and BB/U = +3 SD THEN Obesitas
        $ruleif[27] = min($himpuan_tbu_median,$himpuan_bbu_plus3);
        $rulethen[27] = $this->segitigaZ($normal,$obesitas,$unknow,$ruleif[27]);

        // Rule 29
        // IF TB/U = +1 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[28] = min($himpuan_tbu_plus1,$himpuan_bbu_min3);
        $rulethen[28] = $this->segitigaZ($sangat_pendek,$sangat_kurus,$indikasi_gizi_buruk,$ruleif[28]);

        // Rule 30
        // IF TB/U = +1 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[29] = min($himpuan_tbu_plus1,$himpuan_bbu_min2);
        $rulethen[29] = $this->segitigaZ($indikasi_gizi_buruk,$gizi_buruk,$indikasi_stunting,$ruleif[29]);

        // Rule 31
        // IF TB/U = +1 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[30] = min($himpuan_tbu_plus1,$himpuan_bbu_min1);
        $rulethen[30] = $this->segitigaZ($sangat_kurus,$indikasi_gizi_buruk,$gizi_buruk,$ruleif[30]);

        // Rule 32
        // IF TB/U = +1 SD and BB/U = median SD THEN Normal
        $ruleif[31] = min($himpuan_tbu_plus1,$himpuan_bbu_median);
        $rulethen[31] = $this->segitigaZ($stunting,$normal,$obesitas,$ruleif[31]);

        // Rule 33
        // IF TB/U = +1 SD and BB/U = +1 SD THEN Unknow
        $ruleif[32] = min($himpuan_tbu_plus1,$himpuan_bbu_plus1);
        $rulethen[32] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[32]);

        // Rule 34
        // IF TB/U = +1 SD and BB/U = +2 SD THEN Unknow
        $ruleif[33] = min($himpuan_tbu_plus1,$himpuan_bbu_plus2);
        $rulethen[33] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[33]);

        // Rule 35
        // IF TB/U = +1 SD and BB/U = +3 SD THEN Unknow
        $ruleif[34] = min($himpuan_tbu_plus1,$himpuan_bbu_plus3);
        $rulethen[34] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[34]);

        // Rule 36
        // IF TB/U = +2 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[35] = min($himpuan_tbu_plus2,$himpuan_bbu_min3);
        $rulethen[35] = $this->segitigaZ($sangat_pendek,$sangat_kurus,$indikasi_gizi_buruk,$ruleif[35]);

        // Rule 37
        // IF TB/U = +2 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[36] = min($himpuan_tbu_plus2,$himpuan_bbu_min2);
        $rulethen[36] = $this->segitigaZ($indikasi_gizi_buruk,$gizi_buruk,$indikasi_stunting,$ruleif[36]);

        // Rule 38
        // IF TB/U = +2 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[37] = min($himpuan_tbu_plus2,$himpuan_bbu_min1);
        $rulethen[37] = $this->segitigaZ($sangat_kurus,$indikasi_gizi_buruk,$gizi_buruk,$ruleif[37]);

        // Rule 39
        // IF TB/U = +2 SD and BB/U = median SD THEN Normal
        $ruleif[38] = min($himpuan_tbu_plus2,$himpuan_bbu_median);
        $rulethen[38] = $this->segitigaZ($stunting,$normal,$obesitas,$ruleif[38]);
        
        // Rule 40
        // IF TB/U = +2 SD and BB/U = +1 SD THEN Unknow
        $ruleif[39] = min($himpuan_tbu_plus2,$himpuan_bbu_plus1);
        $rulethen[39] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[39]);

        // Rule 41
        // IF TB/U = +2 SD and BB/U = +2 SD THEN Unknow
        $ruleif[40] = min($himpuan_tbu_plus2,$himpuan_bbu_plus2);
        $rulethen[40] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[40]);

        // Rule 42
        // IF TB/U = +2 SD and BB/U = +3 SD THEN Unknow
        $ruleif[41] = min($himpuan_tbu_plus2,$himpuan_bbu_plus3);
        $rulethen[41] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[41]);

        // Rule 43
        // IF TB/U = +3 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[42] = min($himpuan_tbu_plus3,$himpuan_bbu_min3);
        $rulethen[42] = $this->segitigaZ($sangat_pendek,$sangat_kurus,$indikasi_gizi_buruk,$ruleif[42]);

        // Rule 44
        // IF TB/U = +3 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[43] = min($himpuan_tbu_plus3,$himpuan_bbu_min2);
        $rulethen[43] = $this->segitigaZ($indikasi_gizi_buruk,$gizi_buruk,$indikasi_stunting,$ruleif[43]);

        // Rule 45
        // IF TB/U = +3 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[44] = min($himpuan_tbu_plus3,$himpuan_bbu_min1);
        $rulethen[44] = $this->segitigaZ($sangat_kurus,$indikasi_gizi_buruk,$gizi_buruk,$ruleif[44]);

        // Rule 46
        // IF TB/U = +3 SD and BB/U = median SD THEN Normal
        $ruleif[45] = min($himpuan_tbu_plus3,$himpuan_bbu_median);
        $rulethen[45] = $this->segitigaZ($stunting,$normal,$obesitas,$ruleif[45]);

        // Rule 47
        // IF TB/U = +3 SD and BB/U = +1 SD THEN Unknow
        $ruleif[46] = min($himpuan_tbu_plus3,$himpuan_bbu_plus1);
        $rulethen[46] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[46]);

        // Rule 48
        // IF TB/U = +3 SD and BB/U = +2 SD THEN Unknow
        $ruleif[47] = min($himpuan_tbu_plus3,$himpuan_bbu_plus2);
        $rulethen[47] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[47]);

        // Rule 49
        // IF TB/U = +3 SD and BB/U = -2 SD THEN Unknow
        $ruleif[48] = min($himpuan_tbu_plus3,$himpuan_bbu_plus3);
        $rulethen[48] = $this->trapesiumNaikZ($obesitas,$unknow,$ruleif[48]);

        // Tahap Defuzzykasi
        $hasil = $this->defuzzykasi($ruleif,$rulethen);
        // $hasil = "";

        // Hasil crisp value
        $kriteria = $this->keputusan($hasil);
        // $kriteria = "";

        return view('diagnosa/test',['rule1'=>$ruleif,'rule2'=>$rulethen,'hasil'=>$hasil,'kriteria'=>$kriteria]);
    }

    // fungsi - fungsi Rule IF
    function trapesiumTurun(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 1;
            return $nilai;
        }else if($batas1 <= $value && $value <= $batas2){
            $nilai = ($batas2-$value)/($batas2-$batas1);
            return $nilai;
        }else if($value >= $batas2){
            $nilai = 0;
            return $nilai;
        }
        return $nilai;
    }

    function trapesiumNaik(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 0;
            return $nilai;
        }else if($batas1 <= $value && $value < $batas2){
            $nilai = ($value-$batas1)/($batas2-$batas1);
            return $nilai;
        }else if($value >= $batas2){
            $nilai = 1;
            return $nilai;
        }
        return $nilai;
    }

    function segitiga(float $batas1, float $batas2, float $batas3, float $value){
        $nilai = 0;
        if($value <= $batas1 || $value >= $batas3){
            $nilai = 0;
            return $nilai;
        }else if($batas1 <= $value && $value <= $batas2){
            $nilai = ($value-$batas1)/($batas2-$batas1);
            return $nilai;
        }else if($batas2 <= $value && $value <= $batas3){
            $nilai = ($batas3-$value)/($batas3-$batas2);
            return $nilai;
        }
        return $nilai;
    }

    // Fungsi - fungsi rule then
    function trapesiumTurunZ(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value == 0){
            return $nilai;
        }else{
            $nilai = $batas2-($batas2-$batas1)*$value;
            return $nilai;    
        }
        return $nilai;
    }

    function trapesiumNaikZ(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value == 0){
            return $nilai;
        }else{
            $nilai = $value*($batas2-$batas1)+$batas1;
            return $nilai;    
        }
        return $nilai;
    }

    function segitigaZ(float $batas1, float $batas2, float $batas3, float $value){
        $nilai = 0;
        if($value == 0){
            return $nilai;
        }else{
            $nilai = $value*($batas2-$batas1)+$batas1;
            return $nilai;    
        }
        return $nilai;
    }

    function defuzzykasi(array $nilai1, array $nilai2){
        $batas1 = count($nilai1);
        $batas2 = count($nilai2);
        $hasil1 = 0;
        $hasil2 = 0;
        for ($i = 0; $i < $batas1; $i++) {
            $sum = $nilai1[$i]*$nilai2[$i];
            $hasil1 = $hasil1 + $sum; 
        }
        for ($i = 0; $i < $batas2; $i++) {
            $hasil2 = $hasil2 + $nilai1[$i]; 
        }
        $hasil = $hasil1/$hasil2;
        return $hasil;
    }

    function keputusan(float $nilai){
        $kriteria = "";
        if($nilai <= 0.1){
            $kriteria = "Sangat Pendek";
        }elseif(0.1 < $nilai && $nilai <= 0.2){
            $kriteria = "Sangat Kurus";
        }elseif(0.2 < $nilai && $nilai <= 0.3){
            $kriteria = "Gizi Buruk";
        }elseif(0.3 < $nilai && $nilai <= 0.4){
            $kriteria = "Stunting";
        }elseif(0.4 < $nilai && $nilai <= 0.5){
            $kriteria = "Indikasi Gizi Buruk";
        }elseif(0.5 < $nilai && $nilai <= 0.6){
            $kriteria = "Indikasi Stunting";
        }elseif(0.6 < $nilai && $nilai <= 0.7){
            $kriteria = "Normal";
        }elseif(0.7 < $nilai && $nilai <= 0.8){
            $kriteria = "Obesitas";
        }elseif(0.8 < $nilai && $nilai <= 0.9){
            $kriteria = "Unknow";
        }
        return $kriteria;
    }
}
