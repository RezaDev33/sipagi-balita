<?php

namespace App\Http\Controllers;

use App\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Validator;

class RuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = Rule::select(['rule.id_rule',
                'status_gizi.status_gizi as status',
                'kategori.nm_kategori as kategori',
                'rule.batas as batas'])
            ->leftJoin('status_gizi', 'rule.id_status_gizi', '=', 'status_gizi.id_status_gizi')
            ->leftJoin('kategori', 'rule.id_kategori', '=', 'kategori.id_kategori');
            return DataTables::of($data)
                ->addColumn('action', function($data){
                    $button = '<button type="button" name="edit" id="'.$data->id_rule.'" class="edit btn btn-primary btn-sm">Edit</button>';
                    $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="'.$data->id_rule.'" class="delete btn btn-danger btn-sm">Delete</button>';
                    return $button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        $kategori = DB::table('kategori')->get();
        $status = DB::table('status_gizi')->get();
        
        return view('aturan.index',  compact('kategori','kategori'),  compact('status','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'data_status'    =>  'required',
            'data_kategori'     =>  'required',
            'batas'     =>  'required'
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'id_status_gizi'            =>  $request->data_status,
            'id_kategori'            =>  $request->data_kategori,
            'batas'            =>  $request->batas
        );

        Rule::create($form_data);

        return response()->json(['success' => 'Data Added successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function show(Rule $rule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function edit($id_rule)
    {
        if(request()->ajax())
        {
            $data = Rule::select([
            'status_gizi.status_gizi as status',
            'kategori.nm_kategori as kategori',
            'rule.batas as batas'])
            ->leftJoin('status_gizi', 'rule.id_status_gizi', '=', 'status_gizi.id_status_gizi')
            ->leftJoin('kategori', 'rule.id_kategori', '=', 'kategori.id_kategori')->where('rule.id_rule',$id_rule)->first();
            return response()->json(['result' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rule $rule)
    {
        $rules = array(
            'data_status'    =>  'required',
            'data_kategori'     =>  'required',
            'batas'     =>  'required'
        );

        $error = Validator::make($request->all(), $rules);

        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $form_data = array(
            'id_status_gizi'            =>  $request->data_status,
            'id_kategori'            =>  $request->data_kategori,
            'batas'            =>  $request->batas
        );

        Rule::where('id_rule',$request->hidden_id)->update($form_data);

        return response()->json(['success' => 'Data is successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Rule  $rule
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Rule::where('id_rule',$id);
        $data->delete();
    }
}
