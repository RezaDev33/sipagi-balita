<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Diagnosis;
use App\Kecamatan;
use App\Pasien;
use DataTables;
use Validator;
use App\User;
use Session;
use App\Desa;
use Auth;
Use Exception;

class PasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @num = 0'));
            $data = Pasien::Select(DB::raw('@num  := @num  + 1 AS no'), 'pasien.id_pasien', 'pasien.tgl_lahir', 'pasien.nm_pasien','pasien.nm_ortu','pasien.alamat')
            ->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        $button = '<a href="/pasien/edit/'.$data->id_pasien.'" type="button" name="edit" id="'.$data->id_pasien.'" class="edit btn btn-primary btn-sm">Edit</a>';
                        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="'.$data->id_pasien.'" class="diagnosis btn btn-success btn-sm">Diagnosis</button>';
                        $button .= '&nbsp;&nbsp;&nbsp;<a href="/pasien/delete/'.$data->id_pasien.'" type="button" name="edit" id="'.$data->id_pasien.'" class="delete btn btn-danger btn-sm">Delete</a>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('pasien.pasien');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        $kecamatan = Kecamatan::All();
        $desa = Desa::orderBy('id_kecamatan');
        return view('pasien.addpasien',compact('kecamatan','desa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//         var_dump($methods);
// die();
        $this->validate($request, [
            'anak_ke' => 'required|integer|max:50',
            'jenis_kelamin' => 'required',
            'no_kk' => 'required',
            'nik' => 'required',
            'nm_pasien' => 'required',
            'bb_lahir' => 'required',
            'tb_lahir' => 'required',
            'nm_ortu' => 'required',
            'nik_ayah' => 'required',
            'kia' => 'required',
            'imd' => 'required',
            'tlp_ortu' => 'required',
            'alamat' => 'required',
            
        ]);

        // $add=Pasien::create([
        //     'anak_ke' => $request->get('anak_ke'),
        //     'jenis_kelamin' => $request->get('jenis_kelamin'),
        //     'no_kk' => $request->get('no_kk'),
        //     'nik' => $request->get('nik'),
        //     'nm_pasien' => $request->get('nm_pasien'),
        //     'tgl_lahir' => $request->get('tgl_lahir'),
        //     'tb_lahir' => $request->get('tb_lahir'),
        //     'bb_lahir' => $request->get('bb_lahir'),
        //     'nm_ortu' => $request->get('nm_ortu'),
        //     'nik_ayah' => $request->get('nik_ayah'),
        //     'status_ekonomi' => $request->get('status_ekonomi'),
        //     'kia' => $request->get('kia'),
        //     'imd' => $request->get('imd'),
        //     'tlp_ortu' => $request->get('tlp_ortu'),
        //     'alamat' => $request->get('alamat'),
        // ]);
        //     if($add){
        //         alert()->success('Berhasil.','Data telah ditambahkan!');
        //         return redirect()->route('pasien.index');
        //     }else{
        //         alert()->error('Error.','Data gagal ditambahkan!');
        //         return redirect()->route('pasien.index');
        //     }
        try { 
            $add=Pasien::create([
                'anak_ke' => $request->get('anak_ke'),
                'jenis_kelamin' => $request->get('jenis_kelamin'),
                'no_kk' => $request->get('no_kk'),
                'nik' => $request->get('nik'),
                'nm_pasien' => $request->get('nm_pasien'),
                'tgl_lahir' => $request->get('tgl_lahir'),
                'tb_lahir' => $request->get('tb_lahir'),
                'bb_lahir' => $request->get('bb_lahir'),
                'nm_ortu' => $request->get('nm_ortu'),
                'nik_ayah' => $request->get('nik_ayah'),
                'status_ekonomi' => $request->get('status_ekonomi'),
                'kia' => $request->get('kia'),
                'imd' => $request->get('imd'),
                'tlp_ortu' => $request->get('tlp_ortu'),
                'alamat' => $request->get('alamat'),
            ]);
                if($add){
                    alert()->success('Berhasil.','Data telah ditambahkan!');
                    return redirect()->route('pasien.index');
                }else{
                    alert()->error('Error.','Data gagal ditambahkan!');
                    return redirect()->route('pasien.index');
                }
        } catch(Exception $e){ 
            alert()->error('Error.', "Terdapat data yang salah silahkan coba lagi " );
                return redirect()->route('pasien.index');
            // Note any method of class PDOException can be called on $ex.
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id_pasien)
    {
        $pasien = Pasien::findOrFail($id_pasien);
        return view('pasien.detailpasien', compact('pasien'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pasien)
    {
        $pasien = Pasien::findOrFail($id_pasien);
        return view('pasien.editpasien', compact('pasien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pasien)
    {
        $this->validate($request, [
            'anak_ke' => 'required|integer|max:50',
            'jenis_kelamin' => 'required',
            'no_kk' => 'required',
            'nik' => 'required',
            'nm_pasien' => 'required',
            'bb_lahir' => 'required',
            'tb_lahir' => 'required',
            'nm_ortu' => 'required',
            'nik_ayah' => 'required',
            'kia' => 'required',
            'imd' => 'required',
            'tlp_ortu' => 'required',
            'alamat' => 'required',
            
        ]);

        $edit = Pasien::find($id_pasien)->update([
            'anak_ke' => $request->get('anak_ke'),
            'jenis_kelamin' => $request->get('jenis_kelamin'),
            'no_kk' => $request->get('no_kk'),
            'nik' => $request->get('nik'),
            'nm_pasien' => $request->get('nm_pasien'),
            'tgl_lahir' => $request->get('tgl_lahir'),
            'tb_lahir' => $request->get('tb_lahir'),
            'bb_lahir' => $request->get('bb_lahir'),
            'nm_ortu' => $request->get('nm_ortu'),
            'nik_ayah' => $request->get('nik_ayah'),
            'status_ekonomi' => $request->get('status_ekonomi'),
            'kia' => $request->get('kia'),
            'imd' => $request->get('imd'),
            'tlp_ortu' => $request->get('tlp_ortu'),
            'alamat' => $request->get('alamat'),
        ]);
       
        if($edit){
            alert()->success('Berhasil.','Data telah diupdate!');
            return redirect()->route('pasien.index');
        }else{
            alert()->error('Error.','Data gagal diupdate!');
            return redirect()->route('pasien.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // $delete = Pasien::find($id)->delete();
        // if($delete){
        //     alert()->success('Berhasil.','Data telah dihapus!');
        //     return redirect()->route('pasien.index');
        // }else{
        //     alert()->error('Error.','Data gagal dihapus!');
        //     return redirect()->route('pasien.index');
        // }
        try { 
            $delete = Pasien::find($id)->delete();
            if($delete){
                alert()->success('Berhasil.','Data telah dihapus!');
                return redirect()->route('pasien.index');
            }else{
                alert()->error('Error.','Data gagal dihapus!');
                return redirect()->route('pasien.index');
            }
        } catch(Exception $e){ 
            alert()->error('Error.', "Tidak dapat menghapus data ini!!!, karena sudah terhubung dengan data lain" );
                return redirect()->route('pasien.index');
            // Note any method of class PDOException can be called on $ex.
        }
    }
}
