<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diagnosis;

class DiagnosaController extends Controller
{
    public function index(){
        return view('diagnosa/index');
    }

    public function report(){
        return view('diagnosa/laporan');
    }

    public function proses(Request $request){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'tinggi_badan' => 'required|numeric',
            'berat_badan' => 'required|numeric',
            'cek' => 'required',
        ]);
        
        // Value Input Fuzzy
        $bb = $request->berat_badan;
        $pb = $request->tinggi_badan;
        $tb = $pb*10;

        // mencari keliapan lima dari data pb untuk parameter di tabel BB/PB
        while($tb % 5 != 0){
            $tb = $tb - 1;
        }
        $tb = $tb/10;

        // Get Value tabel standar pertumbuhan dari database
        $data_bbu = Diagnosis::where('tipe_tabel', 'BB/U')->where('parameter', $request->umur)->where('jenis_kelamin', $request->jenis_kelamin)->first();
        $data_pbu = Diagnosis::where('tipe_tabel', $request->jenis_pengukuran.'/U')->where('parameter', $request->umur)->where('jenis_kelamin', $request->jenis_kelamin)->first();
        $data_bbpb = Diagnosis::where('tipe_tabel', 'BB/'.$request->jenis_pengukuran)->where('parameter', $tb)->where('jenis_kelamin', $request->jenis_kelamin)->first();

        // cek data apakah kosong
        if(empty($data_bbu) || empty($data_pbu) || empty($data_bbpb)){
            return redirect()->back()->with('alert', 'Ada kesalahan pada pengisian data. Silakan periksa data kembali')->withInput();
        }else{

        // Data BB/U
        $bbu_min3 = $data_bbu->sdmintiga;
        $bbu_min2 = $data_bbu->sdmindua;
        $bbu_min1 = $data_bbu->sdminsatu;
        $bbu_median = $data_bbu->median;
        $bbu_plus1 = $data_bbu->sdplussatu;
        $bbu_plus2 = $data_bbu->sdplusdua;
        $bbu_plus3 = $data_bbu->sdplustiga;

        // Data PB/U
        $pbu_min3 = $data_pbu->sdmintiga;
        $pbu_min2 = $data_pbu->sdmindua;
        $pbu_min1 = $data_pbu->sdminsatu;
        $pbu_median = $data_pbu->median;
        $pbu_plus1 = $data_pbu->sdplussatu;
        $pbu_plus2 = $data_pbu->sdplusdua;
        $pbu_plus3 = $data_pbu->sdplustiga;

        // Data BB/PB
        $bbpb_min3 = $data_bbpb->sdmintiga;
        $bbpb_min2 = $data_bbpb->sdmindua;
        $bbpb_min1 = $data_bbpb->sdminsatu;
        $bbpb_median = $data_bbpb->median;
        $bbpb_plus1 = $data_bbpb->sdplussatu;
        $bbpb_plus2 = $data_bbpb->sdplusdua;
        $bbpb_plus3 = $data_bbpb->sdplustiga;

        // Kelainan
        $sangat_pendek = 0.1;
        $sangat_kurus = 0.2;
        $gizi_buruk = 0.3;
        $stunting = 0.4;
        $indikasi_gizi_buruk = 0.5;
        $indikasi_stunting = 0.6;
        $normal = 0.7;
        $obesitas = 0.8;
        $unknow = 0.9;

        // Himpunan Fuzzy
        // Himpunan IF TB/U
        $himpuan_pbu_min3 = $this->pertama($pbu_min3,$pb);
        $himpuan_pbu_min2 = $this->linierTurun($pbu_min3,$pbu_min2,$pb);
        $himpuan_pbu_min1 = $this->linierTurun($pbu_min2,$pbu_min1,$pb);
        $himpuan_pbu_median = $this->linierTurun($pbu_min1,$pbu_median,$pb);
        $himpuan_pbu_plus1 = $this->linierTurun($pbu_median,$pbu_plus1,$pb);
        $himpuan_pbu_plus2 = $this->linierTurun($pbu_plus1,$pbu_plus2,$pb);
        $himpuan_pbu_plus3 = $this->linierTurun($pbu_plus2,$pbu_plus3,$pb);

        // Himpunan IF BB/U
        $himpuan_bbu_min3 = $this->pertama($bbu_min3,$bb);
        $himpuan_bbu_min2 = $this->linierTurun($bbu_min3,$bbu_min2,$bb);
        $himpuan_bbu_min1 = $this->linierTurun($bbu_min2,$bbu_min1,$bb);
        $himpuan_bbu_median = $this->linierTurun($bbu_min1,$bbu_median,$bb);
        $himpuan_bbu_plus1 = $this->linierTurun($bbu_median,$bbu_plus1,$bb);
        $himpuan_bbu_plus2 = $this->linierTurun($bbu_plus1,$bbu_plus2,$bb);
        $himpuan_bbu_plus3 = $this->linierTurun($bbu_plus2,$bbu_plus3,$bb);
       
        // Himpunan IF BB/PB
        $himpuan_bbpb_min3 = $this->pertama($bbpb_min3,$bb);
        $himpuan_bbpb_min2 = $this->linierTurun($bbpb_min3,$bbpb_min2,$bb);
        $himpuan_bbpb_min1 = $this->linierTurun($bbpb_min2,$bbpb_min1,$bb);
        $himpuan_bbpb_median = $this->linierTurun($bbpb_min1,$bbpb_median,$bb);
        $himpuan_bbpb_plus1 = $this->linierTurun($bbpb_median,$bbpb_plus1,$bb);
        $himpuan_bbpb_plus2 = $this->linierTurun($bbpb_plus1,$bbpb_plus2,$bb);
        $himpuan_bbpb_plus3 = $this->linierTurun($bbpb_plus2,$bbpb_plus3,$bb);
       
        // DIGNOSIS BERDASARKAN TABEL PB/U
        // Rule 1
        $pbu_if[0] = $himpuan_pbu_min3;
        $pbu_then[0] = 10;

        // Rule 2
        $pbu_if[1] = $himpuan_pbu_min2;
        $pbu_then[1] = 20;

        // Rule 3
        $pbu_if[2] = $himpuan_pbu_min1;
        $pbu_then[2] = 30;

        // Rule 4
        $pbu_if[3] = $himpuan_pbu_median;
        $pbu_then[3] = 40;

        // Rule 5
        $pbu_if[4] = $himpuan_pbu_plus1;
        $pbu_then[4] = 50;

        // Rule 6
        $pbu_if[5] = $himpuan_pbu_plus2;
        $pbu_then[5] = 60;

        // Rule 7
        $pbu_if[6] = $himpuan_pbu_plus3;
        $pbu_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[0] = $this->defuzzykasi($pbu_if,$pbu_then);

        // Hasil crisp value
        $kriteria[0] = $this->keputusanTabel($hasil[0]);

        // DIGNOSIS BERDASARKAN TABEL BB/U
        // Rule 1
        $bbu_if[0] = $himpuan_bbu_min3;
        $bbu_then[0] = 10;

        // Rule 2
        $bbu_if[1] = $himpuan_bbu_min2;
        $bbu_then[1] = 20;

        // Rule 3
        $bbu_if[2] = $himpuan_bbu_min1;
        $bbu_then[2] = 30;

        // Rule 4
        $bbu_if[3] = $himpuan_bbu_median;
        $bbu_then[3] = 40;

        // Rule 5
        $bbu_if[4] = $himpuan_bbu_plus1;
        $bbu_then[4] = 50;

        // Rule 6
        $bbu_if[5] = $himpuan_bbu_plus2;
        $bbu_then[5] = 60;

        // Rule 7
        $bbu_if[6] = $himpuan_bbu_plus3;
        $bbu_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[1] = $this->defuzzykasi($bbu_if,$bbu_then);

        // Hasil crisp value
        $kriteria[1] = $this->keputusanTabel($hasil[1]);

        // DIGNOSIS BERDASARKAN TABEL BB/PB
        // Rule 1
        $bbpb_if[0] = $himpuan_bbpb_min3;
        $bbpb_then[0] = 10;

        // Rule 2
        $bbpb_if[1] = $himpuan_bbpb_min2;
        $bbpb_then[1] = 20;

        // Rule 3
        $bbpb_if[2] = $himpuan_bbpb_min1;
        $bbpb_then[2] = 30;

        // Rule 4
        $bbpb_if[3] = $himpuan_bbpb_median;
        $bbpb_then[3] = 40;

        // Rule 5
        $bbpb_if[4] = $himpuan_bbpb_plus1;
        $bbpb_then[4] = 50;

        // Rule 6
        $bbpb_if[5] = $himpuan_bbpb_plus2;
        $bbpb_then[5] = 60;

        // Rule 7
        $bbpb_if[6] = $himpuan_bbpb_plus3;
        $bbpb_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[2] = $this->defuzzykasi($bbpb_if,$bbpb_then);

        // Hasil crisp value
        $kriteria[2] = $this->keputusanTabel($hasil[2]);

        // KESIMPULAN DARI SEMUA DIGNOSA HASIL
        // Rule 1
        // IF TB/U = -3 SD and BB/U = -3 SD THEN Sangat Pendek 
        $ruleif[0] = min($himpuan_pbu_min3,$himpuan_bbu_min3);
        $rulethen[0] = $sangat_pendek;

        // Rule 2
        // IF TB/U = -3 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[1] = min($himpuan_pbu_min3,$himpuan_bbu_min2);
        $rulethen[1] = $gizi_buruk;

        // Rule 3
        // IF TB/U = -3 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[2] = min($himpuan_pbu_min3,$himpuan_bbu_min1);
        $rulethen[2] = $indikasi_gizi_buruk;

        // Rule 4
        // IF TB/U = -3 SD and BB/U = median SD THEN Sangat Pendek
        $ruleif[3] = min($himpuan_pbu_min3,$himpuan_bbu_median);
        $rulethen[3] = $sangat_pendek;

        // Rule 5
        // IF TB/U = -3 SD and BB/U = +1 SD THEN Obesitas
        $ruleif[4] = min($himpuan_pbu_min3,$himpuan_bbu_plus1);
        $rulethen[4] = $obesitas;
        
        // Rule 6
        // IF TB/U = -3 SD and BB/U = +2 SD THEN Obesitas
        $ruleif[5] = min($himpuan_pbu_min3,$himpuan_bbu_plus2);
        $rulethen[5] = $obesitas;
        
        // Rule 7
        // IF TB/U = -3 SD and BB/U = +3 SD THEN Obesitas
        $ruleif[6] = min($himpuan_pbu_min3,$himpuan_bbu_plus3);
        $rulethen[6] = $obesitas;

        // Rule 8
        // IF TB/U = -2 SD and BB/U = -3 SD THEN Stunting
        $ruleif[7] = min($himpuan_pbu_min2,$himpuan_bbu_min3);
        $rulethen[7] = $stunting;

        // Rule 9
        // IF TB/U = -2 SD and BB/U = -2 SD THEN Stunting
        $ruleif[8] = min($himpuan_pbu_min2,$himpuan_bbu_min2);
        $rulethen[8] = $stunting;

        // Rule 10
        // IF TB/U = -2 SD and BB/U = -1 SD THEN Stunting
        $ruleif[9] = min($himpuan_pbu_min2,$himpuan_bbu_min1);
        $rulethen[9] = $stunting;

        // Rule 11
        // IF TB/U = -2 SD and BB/U = median SD THEN Stunting
        $ruleif[10] = min($himpuan_pbu_min2,$himpuan_bbu_median);
        $rulethen[10] = $stunting;

        // Rule 12
        // IF TB/U = -2 SD and BB/U = +1 SD THEN Stunting
        $ruleif[11] = min($himpuan_pbu_min2,$himpuan_bbu_plus1);
        $rulethen[11] = $stunting;

        // Rule 13
        // IF TB/U = -2 SD and BB/U = +2 SD THEN Stunting
        $ruleif[12] = min($himpuan_pbu_min2,$himpuan_bbu_plus2);
        $rulethen[12] = $stunting;

        // Rule 14
        // IF TB/U = -2 SD and BB/U = +3 SD THEN Stunting
        $ruleif[13] = min($himpuan_pbu_min2,$himpuan_bbu_plus3);
        $rulethen[13] = $stunting;

        // Rule 15
        // IF TB/U = -1 SD and BB/U = -3 SD THEN Indikasi Stunting
        $ruleif[14] = min($himpuan_pbu_min1,$himpuan_bbu_min3);
        $rulethen[14] = $indikasi_stunting;

        // Rule 16
        // IF TB/U = -1 SD and BB/U = -2 SD THEN Indikasi Stunting
        $ruleif[15] = min($himpuan_pbu_min1,$himpuan_bbu_min2);
        $rulethen[15] = $indikasi_stunting;

        // Rule 17
        // IF TB/U = -1 SD and BB/U = -1 SD THEN Indikasi Stunting
        $ruleif[16] = min($himpuan_pbu_min1,$himpuan_bbu_min1);
        $rulethen[16] = $indikasi_stunting;
        
        // Rule 18
        // IF TB/U = -1 SD and BB/U = median SD THEN Indikasi Stunting
        $ruleif[17] = min($himpuan_pbu_min1,$himpuan_bbu_median);
        $rulethen[17] = $indikasi_stunting;

        // Rule 19
        // IF TB/U = -1 SD and BB/U = +1 SD THEN Indikasi Stunting
        $ruleif[18] = min($himpuan_pbu_min1,$himpuan_bbu_plus1);
        $rulethen[18] = $indikasi_stunting;

        // Rule 20
        // IF TB/U = -1 SD and BB/U = +2 SD THEN Indikasi Stunting
        $ruleif[19] = min($himpuan_pbu_min1,$himpuan_bbu_plus2);
        $rulethen[19] = $indikasi_stunting;

        // Rule 21
        // IF TB/U = -1 SD and BB/U = +3 SD THEN Indikasi Stunting
        $ruleif[20] = min($himpuan_pbu_min1,$himpuan_bbu_plus3);
        $rulethen[20] = $indikasi_stunting;

        // Rule 22
        // IF TB/U = median SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[21] = min($himpuan_pbu_median,$himpuan_bbu_min3);
        $rulethen[21] = $sangat_kurus;

        // Rule 23
        // IF TB/U = median SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[22] = min($himpuan_pbu_median,$himpuan_bbu_min2);
        $rulethen[22] = $gizi_buruk;

        // Rule 24
        // IF TB/U = median SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[23] = min($himpuan_pbu_median,$himpuan_bbu_min1);
        $rulethen[23] = $indikasi_gizi_buruk;

        // Rule 25
        // IF TB/U = median SD and BB/U = median SD THEN normal
        $ruleif[24] = min($himpuan_pbu_median,$himpuan_bbu_median);
        $rulethen[24] = $normal;

        // Rule 26
        // IF TB/U = median SD and BB/U = +1 SD THEN Obesitas
        $ruleif[25] = min($himpuan_pbu_median,$himpuan_bbu_plus1);
        $rulethen[25] = $obesitas;

        // Rule 27
        // IF TB/U = median SD and BB/U = +2 SD THEN Obesitas
        $ruleif[26] = min($himpuan_pbu_median,$himpuan_bbu_plus2);
        $rulethen[26] = $obesitas;

        // Rule 28
        // IF TB/U = median SD and BB/U = +3 SD THEN Obesitas
        $ruleif[27] = min($himpuan_pbu_median,$himpuan_bbu_plus3);
        $rulethen[27] = $obesitas;

        // Rule 29
        // IF TB/U = +1 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[28] = min($himpuan_pbu_plus1,$himpuan_bbu_min3);
        $rulethen[28] = $sangat_kurus;

        // Rule 30
        // IF TB/U = +1 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[29] = min($himpuan_pbu_plus1,$himpuan_bbu_min2);
        $rulethen[29] = $gizi_buruk;

        // Rule 31
        // IF TB/U = +1 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[30] = min($himpuan_pbu_plus1,$himpuan_bbu_min1);
        $rulethen[30] = $indikasi_gizi_buruk;

        // Rule 32
        // IF TB/U = +1 SD and BB/U = median SD THEN Normal
        $ruleif[31] = min($himpuan_pbu_plus1,$himpuan_bbu_median);
        $rulethen[31] = $normal;

        // Rule 33
        // IF TB/U = +1 SD and BB/U = +1 SD THEN Unknow
        $ruleif[32] = min($himpuan_pbu_plus1,$himpuan_bbu_plus1);
        $rulethen[32] = $unknow;

        // Rule 34
        // IF TB/U = +1 SD and BB/U = +2 SD THEN Unknow
        $ruleif[33] = min($himpuan_pbu_plus1,$himpuan_bbu_plus2);
        $rulethen[33] = $unknow;

        // Rule 35
        // IF TB/U = +1 SD and BB/U = +3 SD THEN Unknow
        $ruleif[34] = min($himpuan_pbu_plus1,$himpuan_bbu_plus3);
        $rulethen[34] = $unknow;

        // Rule 36
        // IF TB/U = +2 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[35] = min($himpuan_pbu_plus2,$himpuan_bbu_min3);
        $rulethen[35] = $sangat_kurus;

        // Rule 37
        // IF TB/U = +2 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[36] = min($himpuan_pbu_plus2,$himpuan_bbu_min2);
        $rulethen[36] = $gizi_buruk;

        // Rule 38
        // IF TB/U = +2 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[37] = min($himpuan_pbu_plus2,$himpuan_bbu_min1);
        $rulethen[37] = $indikasi_gizi_buruk;

        // Rule 39
        // IF TB/U = +2 SD and BB/U = median SD THEN Normal
        $ruleif[38] = min($himpuan_pbu_plus2,$himpuan_bbu_median);
        $rulethen[38] = $normal;
        
        // Rule 40
        // IF TB/U = +2 SD and BB/U = +1 SD THEN Unknow
        $ruleif[39] = min($himpuan_pbu_plus2,$himpuan_bbu_plus1);
        $rulethen[39] = $unknow;

        // Rule 41
        // IF TB/U = +2 SD and BB/U = +2 SD THEN Unknow
        $ruleif[40] = min($himpuan_pbu_plus2,$himpuan_bbu_plus2);
        $rulethen[40] = $unknow;

        // Rule 42
        // IF TB/U = +2 SD and BB/U = +3 SD THEN Unknow
        $ruleif[41] = min($himpuan_pbu_plus2,$himpuan_bbu_plus3);
        $rulethen[41] = $unknow;

        // Rule 43
        // IF TB/U = +3 SD and BB/U = -3 SD THEN Sangat Kurus
        $ruleif[42] = min($himpuan_pbu_plus3,$himpuan_bbu_min3);
        $rulethen[42] = $sangat_kurus;

        // Rule 44
        // IF TB/U = +3 SD and BB/U = -2 SD THEN Gizi Buruk
        $ruleif[43] = min($himpuan_pbu_plus3,$himpuan_bbu_min2);
        $rulethen[43] = $gizi_buruk;

        // Rule 45
        // IF TB/U = +3 SD and BB/U = -1 SD THEN Indikasi Gizi Buruk
        $ruleif[44] = min($himpuan_pbu_plus3,$himpuan_bbu_min1);
        $rulethen[44] = $indikasi_gizi_buruk;

        // Rule 46
        // IF TB/U = +3 SD and BB/U = median SD THEN Normal
        $ruleif[45] = min($himpuan_pbu_plus3,$himpuan_bbu_median);
        $rulethen[45] = $normal;

        // Rule 47
        // IF TB/U = +3 SD and BB/U = +1 SD THEN Unknow
        $ruleif[46] = min($himpuan_pbu_plus3,$himpuan_bbu_plus1);
        $rulethen[46] = $unknow;

        // Rule 48
        // IF TB/U = +3 SD and BB/U = +2 SD THEN Unknow
        $ruleif[47] = min($himpuan_pbu_plus3,$himpuan_bbu_plus2);
        $rulethen[47] = $unknow;

        // Rule 49
        // IF TB/U = +3 SD and BB/U = -2 SD THEN Unknow
        $ruleif[48] = min($himpuan_pbu_plus3,$himpuan_bbu_plus3);
        $rulethen[48] = $unknow;

        // Tahap Defuzzykasi
        $hasil[3] = $this->defuzzykasi($ruleif,$rulethen);

        // Hasil crisp value
        $kriteria[3] = $this->keputusan($hasil[3]);

        // alert
        $alert = $this->sendAlert($hasil[3]);

        return view('diagnosa/laporan',['request'=>$request,'kriteria'=>$kriteria,'alert'=>$alert]);
        }
    }

    // fungsi - fungsi Rule IF
    function pertama(float $batas1, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 1;
            return $nilai;
        }else if($value > $batas1){
            $nilai = 0;
            return $nilai;
        }
        return $nilai;
    }

    function linierTurun(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 0;
            return $nilai;
        }else if($value == $batas2){
            $nilai = 1;
            return $nilai;
        }else if($batas1 < $value && $value <= $batas2){
            $nilai = ($batas2-$value)/($batas2-$batas1);
            return $nilai;
        }else if($value > $batas2){
            $nilai = 0;
            return $nilai;
        }
        return $nilai;
    }

    function defuzzykasi(array $nilai1, array $nilai2){
        $batas1 = count($nilai1);
        $batas2 = count($nilai2);
        $hasil1 = 0;
        $hasil2 = 0;
        for ($i = 0; $i < $batas1; $i++) {
            $sum = $nilai1[$i]*$nilai2[$i];
            $hasil1 = $hasil1 + $sum; 
        }
        for ($i = 0; $i < $batas2; $i++) {
            $hasil2 = $hasil2 + $nilai1[$i]; 
        }
        return $hasil2 == 0 ? 0 : ($hasil1/$hasil2);
    }

    function keputusan(float $nilai){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknown"; // abu-abu
        }elseif($nilai <= 0.1){
            $kriteria = "Sangat Pendek"; // merah
        }elseif(0.1 < $nilai && $nilai <= 0.2){
            $kriteria = "Sangat Kurus"; // merah
        }elseif(0.2 < $nilai && $nilai <= 0.3){
            $kriteria = "Gizi Buruk"; // merah
        }elseif(0.3 < $nilai && $nilai <= 0.4){
            $kriteria = "Stunting"; // merah
        }elseif(0.4 < $nilai && $nilai <= 0.5){
            $kriteria = "Indikasi Gizi Buruk"; // kuning 
        }elseif(0.5 < $nilai && $nilai <= 0.6){
            $kriteria = "Indikasi Stunting"; // kuning
        }elseif(0.6 < $nilai && $nilai <= 0.7){
            $kriteria = "Normal"; // hijau
        }elseif(0.7 < $nilai && $nilai <= 0.81){
            $kriteria = "Obesitas"; // merah
        }elseif(0.81 < $nilai && $nilai <= 0.9){
            $kriteria = "Sangat Besar"; // merah
        }
        return $kriteria;
    }

    function sendAlert(float $nilai){
        $alert = "";
        if($nilai == 0){
            $alert = 'alert-secondary'; // abu-abu
        }elseif($nilai <= 0.1){
            $alert = 'alert-danger'; // merah
        }elseif(0.1 < $nilai && $nilai <= 0.2){
            $alert = 'alert-danger'; // merah
        }elseif(0.2 < $nilai && $nilai <= 0.3){
            $alert = 'alert-danger'; // merah
        }elseif(0.3 < $nilai && $nilai <= 0.4){
            $alert = 'alert-danger'; // merah
        }elseif(0.4 < $nilai && $nilai <= 0.5){
            $alert = 'alert-warning'; // kuning 
        }elseif(0.5 < $nilai && $nilai <= 0.6){
            $alert = 'alert-warning'; // kuning
        }elseif(0.6 < $nilai && $nilai <= 0.7){
            $alert = 'alert-success'; // hijau
        }elseif(0.7 < $nilai && $nilai <= 0.81){
            $alert = 'alert-danger'; // merah
        }elseif(0.81 < $nilai && $nilai <= 0.9){
            $alert = 'alert-danger'; // merah
        }
        return $alert;
    }

    function keputusanTabel(float $nilai){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknow";
        }elseif($nilai <= 10){
            $kriteria = "-3 SD";
        }elseif(10 < $nilai && $nilai <= 20){
            $kriteria = "-2 SD";
        }elseif(20 < $nilai && $nilai <= 30){
            $kriteria = "-1 SD";
        }elseif(30 < $nilai && $nilai <= 40){
            $kriteria = "Median";
        }elseif(40 < $nilai && $nilai <= 50){
            $kriteria = "+1 SD";
        }elseif(50 < $nilai && $nilai <= 60){
            $kriteria = "+2 SD";
        }elseif(60 < $nilai && $nilai <= 70){
            $kriteria = "+3 SD";
        }
        return $kriteria;
    }
}
