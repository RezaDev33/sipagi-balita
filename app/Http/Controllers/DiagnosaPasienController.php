<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Rule;
use App\Diagnosis;
use App\Pemeriksaan;
use App\Rule_pemeriksaan;

class DiagnosaPasienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        $data_pasien = Pasien::where('id_pasien', $request->id)->first();

        $waktu_sekarang = date('Y-m-d');
        $tanggal_lahir = $data_pasien->tgl_lahir;

        $umur_pasien = date_diff(date_create($tanggal_lahir),date_create($waktu_sekarang));
        $bulan = intval($umur_pasien->y) * 12 + intval($umur_pasien->m);
        
        // Value Input Fuzzy
        $umur = $bulan;
        $jk = $data_pasien->jenis_kelamin;
        $bb = $request->berat_badan;
        $pb = $request->tinggi_badan;
        $tb = $pb*10;

        // mencari keliapan lima dari data pb untuk parameter di tabel BB/PB
        while($tb % 5 != 0){
            $tb = $tb - 1;
        }
        $tb = $tb/10;

        // Get Value tabel standar pertumbuhan dari database
        $data_bbu = Diagnosis::where('tipe_tabel', 'BB/U')->where('parameter', $umur)->where('jenis_kelamin', $jk)->first();
        $data_pbu = Diagnosis::where('tipe_tabel', $request->jenis_pengukuran.'/U')->where('parameter', $umur)->where('jenis_kelamin', $jk)->first();
        $data_bbpb = Diagnosis::where('tipe_tabel', 'BB/'.$request->jenis_pengukuran)->where('parameter', $tb)->where('jenis_kelamin', $jk)->first();

        if(empty($data_bbu) || empty($data_pbu) || empty($data_bbpb)){
            return response()->json(['errors' => 'Pasien gagal di diagnosa.']);
        }else{
            
        $rule_bbu = Rule::select('kategori.nm_kategori','status_gizi.status_gizi')
        ->leftJoin('kategori', 'rule.id_kategori', '=', 'kategori.id_kategori')
        ->leftJoin('status_gizi', 'rule.id_status_gizi', '=', 'status_gizi.id_status_gizi')
        ->where('kategori.nm_kategori','BB/U')->get();

        $rule_pbu = Rule::select('kategori.nm_kategori','status_gizi.status_gizi')
        ->leftJoin('kategori', 'rule.id_kategori', '=', 'kategori.id_kategori')
        ->leftJoin('status_gizi', 'rule.id_status_gizi', '=', 'status_gizi.id_status_gizi')
        ->where('kategori.nm_kategori', $request->jenis_pengukuran.'/U')->get();

        $rule_bbpb = Rule::select('kategori.nm_kategori','status_gizi.status_gizi')
        ->leftJoin('kategori', 'rule.id_kategori', '=', 'kategori.id_kategori')
        ->leftJoin('status_gizi', 'rule.id_status_gizi', '=', 'status_gizi.id_status_gizi')
        ->where('kategori.nm_kategori', 'BB/'.$request->jenis_pengukuran)->get();

        $i = 0;
        foreach ($rule_bbu as $value){
            $hasil_bbu[$i++] = $value->status_gizi;
        }

        $i = 0;
        foreach ($rule_pbu as $value){
            $hasil_pbu[$i++] = $value->status_gizi;
        }

        $i = 0;
        foreach ($rule_bbpb as $value){
            $hasil_bbpb[$i++] = $value->status_gizi;
        }

        // Data BB/U
        $bbu_min3 = $data_bbu->sdmintiga;
        $bbu_min2 = $data_bbu->sdmindua;
        $bbu_min1 = $data_bbu->sdminsatu;
        $bbu_median = $data_bbu->median;
        $bbu_plus1 = $data_bbu->sdplussatu;
        $bbu_plus2 = $data_bbu->sdplusdua;
        $bbu_plus3 = $data_bbu->sdplustiga;

        // Data PB/U
        $pbu_min3 = $data_pbu->sdmintiga;
        $pbu_min2 = $data_pbu->sdmindua;
        $pbu_min1 = $data_pbu->sdminsatu;
        $pbu_median = $data_pbu->median;
        $pbu_plus1 = $data_pbu->sdplussatu;
        $pbu_plus2 = $data_pbu->sdplusdua;
        $pbu_plus3 = $data_pbu->sdplustiga;

        // Data BB/PB
        $bbpb_min3 = $data_bbpb->sdmintiga;
        $bbpb_min2 = $data_bbpb->sdmindua;
        $bbpb_min1 = $data_bbpb->sdminsatu;
        $bbpb_median = $data_bbpb->median;
        $bbpb_plus1 = $data_bbpb->sdplussatu;
        $bbpb_plus2 = $data_bbpb->sdplusdua;
        $bbpb_plus3 = $data_bbpb->sdplustiga;

        // Himpunan Fuzzy
        // Himpunan IF TB/U
        $himpuan_pbu_min3 = $this->pertama($pbu_min3,$pb);
        $himpuan_pbu_min2 = $this->linierTurun($pbu_min3,$pbu_min2,$pb);
        $himpuan_pbu_min1 = $this->linierTurun($pbu_min2,$pbu_min1,$pb);
        $himpuan_pbu_median = $this->linierTurun($pbu_min1,$pbu_median,$pb);
        $himpuan_pbu_plus1 = $this->linierTurun($pbu_median,$pbu_plus1,$pb);
        $himpuan_pbu_plus2 = $this->linierTurun($pbu_plus1,$pbu_plus2,$pb);
        $himpuan_pbu_plus3 = $this->linierTurun($pbu_plus2,$pbu_plus3,$pb);

        // Himpunan IF BB/U
        $himpuan_bbu_min3 = $this->pertama($bbu_min3,$bb);
        $himpuan_bbu_min2 = $this->linierTurun($bbu_min3,$bbu_min2,$bb);
        $himpuan_bbu_min1 = $this->linierTurun($bbu_min2,$bbu_min1,$bb);
        $himpuan_bbu_median = $this->linierTurun($bbu_min1,$bbu_median,$bb);
        $himpuan_bbu_plus1 = $this->linierTurun($bbu_median,$bbu_plus1,$bb);
        $himpuan_bbu_plus2 = $this->linierTurun($bbu_plus1,$bbu_plus2,$bb);
        $himpuan_bbu_plus3 = $this->linierTurun($bbu_plus2,$bbu_plus3,$bb);
       
        // Himpunan IF BB/PB
        $himpuan_bbpb_min3 = $this->pertama($bbpb_min3,$bb);
        $himpuan_bbpb_min2 = $this->linierTurun($bbpb_min3,$bbpb_min2,$bb);
        $himpuan_bbpb_min1 = $this->linierTurun($bbpb_min2,$bbpb_min1,$bb);
        $himpuan_bbpb_median = $this->linierTurun($bbpb_min1,$bbpb_median,$bb);
        $himpuan_bbpb_plus1 = $this->linierTurun($bbpb_median,$bbpb_plus1,$bb);
        $himpuan_bbpb_plus2 = $this->linierTurun($bbpb_plus1,$bbpb_plus2,$bb);
        $himpuan_bbpb_plus3 = $this->linierTurun($bbpb_plus2,$bbpb_plus3,$bb);
       
        // DIGNOSIS BERDASARKAN TABEL PB/U
        // Rule 1
        $pbu_if[0] = $himpuan_pbu_min3;
        $pbu_then[0] = 30;

        // Rule 2
        $pbu_if[1] = $himpuan_pbu_min2;
        $pbu_then[1] = 60;

        // Rule 3
        $pbu_if[2] = $himpuan_pbu_min1;
        $pbu_then[2] = 90;

        // Rule 4
        $pbu_if[3] = $himpuan_pbu_median;
        $pbu_then[3] = 90;

        // Rule 5
        $pbu_if[4] = $himpuan_pbu_plus1;
        $pbu_then[4] = 90;

        // Rule 6
        $pbu_if[5] = $himpuan_pbu_plus2;
        $pbu_then[5] = 90;

        // Rule 7
        $pbu_if[6] = $himpuan_pbu_plus3;
        $pbu_then[6] = 90;

        // Tahap Defuzzykasi
        $hasil[0] = $this->defuzzykasi($pbu_if,$pbu_then);

        // Hasil crisp value
        $kriteria[0] = $this->keputusanPbu($hasil[0],$hasil_pbu);

        // DIGNOSIS BERDASARKAN TABEL BB/U
        // Rule 1
        $bbu_if[0] = $himpuan_bbu_min3;
        $bbu_then[0] = 20;

        // Rule 2
        $bbu_if[1] = $himpuan_bbu_min2;
        $bbu_then[1] = 40;

        // Rule 3
        $bbu_if[2] = $himpuan_bbu_min1;
        $bbu_then[2] = 60;

        // Rule 4
        $bbu_if[3] = $himpuan_bbu_median;
        $bbu_then[3] = 60;

        // Rule 5
        $bbu_if[4] = $himpuan_bbu_plus1;
        $bbu_then[4] = 60;

        // Rule 6
        $bbu_if[5] = $himpuan_bbu_plus2;
        $bbu_then[5] = 60;

        // Rule 7
        $bbu_if[6] = $himpuan_bbu_plus3;
        $bbu_then[6] = 80;

        // Tahap Defuzzykasi
        $hasil[1] = $this->defuzzykasi($bbu_if,$bbu_then);

        // Hasil crisp value
        $kriteria[1] = $this->keputusanBbu($hasil[1],$hasil_bbu);

        // DIGNOSIS BERDASARKAN TABEL BB/PB
        // Rule 1
        $bbpb_if[0] = $himpuan_bbpb_min3;
        $bbpb_then[0] = 15;

        // Rule 2
        $bbpb_if[1] = $himpuan_bbpb_min2;
        $bbpb_then[1] = 30;

        // Rule 3
        $bbpb_if[2] = $himpuan_bbpb_min1;
        $bbpb_then[2] = 45;

        // Rule 4
        $bbpb_if[3] = $himpuan_bbpb_median;
        $bbpb_then[3] = 45;

        // Rule 5
        $bbpb_if[4] = $himpuan_bbpb_plus1;
        $bbpb_then[4] = 60;

        // Rule 6
        $bbpb_if[5] = $himpuan_bbpb_plus2;
        $bbpb_then[5] = 75;

        // Rule 7
        $bbpb_if[6] = $himpuan_bbpb_plus3;
        $bbpb_then[6] = 90;

        // Tahap Defuzzykasi
        $hasil[2] = $this->defuzzykasi($bbpb_if,$bbpb_then);

        // Hasil crisp value
        $kriteria[2] = $this->keputusanBbpb($hasil[2],$hasil_bbpb);

        // DIGNOSIS BERDASARKAN TABEL PB/U
        // Rule 1
        $pbu_if[0] = $himpuan_pbu_min3;
        $pbu_then[0] = 10;

        // Rule 2
        $pbu_if[1] = $himpuan_pbu_min2;
        $pbu_then[1] = 20;

        // Rule 3
        $pbu_if[2] = $himpuan_pbu_min1;
        $pbu_then[2] = 30;

        // Rule 4
        $pbu_if[3] = $himpuan_pbu_median;
        $pbu_then[3] = 40;

        // Rule 5
        $pbu_if[4] = $himpuan_pbu_plus1;
        $pbu_then[4] = 50;

        // Rule 6
        $pbu_if[5] = $himpuan_pbu_plus2;
        $pbu_then[5] = 60;

        // Rule 7
        $pbu_if[6] = $himpuan_pbu_plus3;
        $pbu_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[3] = $this->defuzzykasi($pbu_if,$pbu_then);

        // Hasil crisp value
        $kriteria[3] = $this->keputusanTabel($hasil[3]);

        // DIGNOSIS BERDASARKAN TABEL BB/U
        // Rule 1
        $bbu_if[0] = $himpuan_bbu_min3;
        $bbu_then[0] = 10;

        // Rule 2
        $bbu_if[1] = $himpuan_bbu_min2;
        $bbu_then[1] = 20;

        // Rule 3
        $bbu_if[2] = $himpuan_bbu_min1;
        $bbu_then[2] = 30;

        // Rule 4
        $bbu_if[3] = $himpuan_bbu_median;
        $bbu_then[3] = 40;

        // Rule 5
        $bbu_if[4] = $himpuan_bbu_plus1;
        $bbu_then[4] = 50;

        // Rule 6
        $bbu_if[5] = $himpuan_bbu_plus2;
        $bbu_then[5] = 60;

        // Rule 7
        $bbu_if[6] = $himpuan_bbu_plus3;
        $bbu_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[4] = $this->defuzzykasi($bbu_if,$bbu_then);

        // Hasil crisp value
        $kriteria[4] = $this->keputusanTabel($hasil[4]);

        // DIGNOSIS BERDASARKAN TABEL BB/PB
        // Rule 1
        $bbpb_if[0] = $himpuan_bbpb_min3;
        $bbpb_then[0] = 10;

        // Rule 2
        $bbpb_if[1] = $himpuan_bbpb_min2;
        $bbpb_then[1] = 20;

        // Rule 3
        $bbpb_if[2] = $himpuan_bbpb_min1;
        $bbpb_then[2] = 30;

        // Rule 4
        $bbpb_if[3] = $himpuan_bbpb_median;
        $bbpb_then[3] = 40;

        // Rule 5
        $bbpb_if[4] = $himpuan_bbpb_plus1;
        $bbpb_then[4] = 50;

        // Rule 6
        $bbpb_if[5] = $himpuan_bbpb_plus2;
        $bbpb_then[5] = 60;

        // Rule 7
        $bbpb_if[6] = $himpuan_bbpb_plus3;
        $bbpb_then[6] = 70;

        // Tahap Defuzzykasi
        $hasil[5] = $this->defuzzykasi($bbpb_if,$bbpb_then);

        // Hasil crisp value
        $kriteria[5] = $this->keputusanTabel($hasil[5]);

        $form_data = array(
            'id_pasien'            =>  $request->id,
            'id_kecamatan'            =>  '21817',
            'id_posyandu'            =>  '1',
            'validasi'            =>  '1',
            'tgl_pemeriksaan'            =>  $waktu_sekarang,
            'umur'            =>  $bulan,
            'asi_eks'            =>  '1',
            'vit_a'            =>  '1',
            'cara_ukur'            =>  '1',
            'bb'            =>  $request->berat_badan,
            'pb'            =>  $request->tinggi_badan,
            'bb_u'            =>  $kriteria[4],
            strtolower($request->jenis_pengukuran).'_u'            =>  $kriteria[3],
            'bb_'.strtolower($request->jenis_pengukuran)            => $kriteria[5],
            'diagnosis_bbu' => $kriteria[1],
            'diagnosis_pbu' => $kriteria[0],
            'diagnosis_bbpb' => $kriteria[2],
        );

        Pemeriksaan::create($form_data);

        return response()->json(['success' => 'Pasien berhasil di diagnosa.']);
    }
    }

    // fungsi - fungsi Rule IF
    function pertama(float $batas1, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 1;
            return $nilai;
        }else if($value > $batas1){
            $nilai = 0;
            return $nilai;
        }
        return $nilai;
    }

    function linierTurun(float $batas1, float $batas2, float $value){
        $nilai = 0;
        if($value <= $batas1){
            $nilai = 0;
            return $nilai;
        }else if($value == $batas2){
            $nilai = 1;
            return $nilai;
        }else if($batas1 < $value && $value <= $batas2){
            $nilai = ($batas2-$value)/($batas2-$batas1);
            return $nilai;
        }else if($value > $batas2){
            $nilai = 0;
            return $nilai;
        }
        return $nilai;
    }

    function defuzzykasi(array $nilai1, array $nilai2){
        $batas1 = count($nilai1);
        $batas2 = count($nilai2);
        $hasil1 = 0;
        $hasil2 = 0;
        for ($i = 0; $i < $batas1; $i++) {
            $sum = $nilai1[$i]*$nilai2[$i];
            $hasil1 = $hasil1 + $sum; 
        }
        for ($i = 0; $i < $batas2; $i++) {
            $hasil2 = $hasil2 + $nilai1[$i]; 
        }
        return $hasil2 == 0 ? 0 : ($hasil1/$hasil2);
    }

    function keputusanBbu(float $nilai, $hasil){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknow";
        }elseif($nilai <= 20){
            $kriteria = $hasil[0];
        }elseif(20 < $nilai && $nilai <= 40){
            $kriteria = $hasil[1];
        }elseif(40 < $nilai && $nilai <= 60){
            $kriteria = $hasil[3];
        }elseif(60 < $nilai && $nilai <= 80){
            $kriteria = $hasil[2];
        }
        return $kriteria;
    }

    function keputusanPbu(float $nilai, $hasil){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknow";
        }elseif($nilai <= 30){
            $kriteria = $hasil[0];
        }elseif(30 < $nilai && $nilai <= 60){
            $kriteria = $hasil[1];
        }elseif(60 < $nilai && $nilai <= 90){
            $kriteria = $hasil[2];
        }
        return $kriteria;
    }

    function keputusanBbpb(float $nilai, $hasil){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknow";
        }elseif($nilai <= 15){
            $kriteria = $hasil[0];
        }elseif(15 < $nilai && $nilai <= 30){
            $kriteria = $hasil[1];
        }elseif(30 < $nilai && $nilai <= 45){
            $kriteria = $hasil[5];
        }elseif(45 < $nilai && $nilai <= 60){
            $kriteria = $hasil[2];
        }elseif(60 < $nilai && $nilai <= 75){
            $kriteria = $hasil[3];
        }elseif(75 < $nilai && $nilai <= 90){
            $kriteria = $hasil[4];
        }
        return $kriteria;
    }

    function keputusanTabel(float $nilai){
        $kriteria = "";
        if($nilai == 0){
            $kriteria = "Unknow";
        }elseif($nilai <= 10){
            $kriteria = "-3 SD";
        }elseif(10 < $nilai && $nilai <= 20){
            $kriteria = "-2 SD";
        }elseif(20 < $nilai && $nilai <= 30){
            $kriteria = "-1 SD";
        }elseif(30 < $nilai && $nilai <= 40){
            $kriteria = "Median";
        }elseif(40 < $nilai && $nilai <= 50){
            $kriteria = "+1 SD";
        }elseif(50 < $nilai && $nilai <= 60){
            $kriteria = "+2 SD";
        }elseif(60 < $nilai && $nilai <= 70){
            $kriteria = "+3 SD";
        }
        return $kriteria;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
