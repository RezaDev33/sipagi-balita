<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Pemeriksaan;
use DataTables;
use App\Exports\PemeriksaanExport;
use Maatwebsite\Excel\Facades\Excel;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Index(Request $request)
    {
        if($request->ajax())
        {
            DB::statement(DB::raw('set @num = 0'));
            $data = Pemeriksaan::select(DB::raw('@num  := @num  + 1 AS rownum') ,'pasien.jenis_kelamin','pasien.nm_pasien as nama','pemeriksaan.umur as umur','pasien.nm_ortu as orang_tua','pemeriksaan.tgl_pemeriksaan as tanggal','pemeriksaan.diagnosis_bbu','pemeriksaan.diagnosis_pbu','pemeriksaan.diagnosis_bbpb')
                ->leftJoin('pasien', 'pemeriksaan.id_pasien', '=', 'pasien.id_pasien')
                ->get();

            return DataTables::of($data)
                ->make(true);
        }
        return view('status.index');
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        return Excel::download(new PemeriksaanExport, 'Status Gizi Balita.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
