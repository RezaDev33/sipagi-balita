<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solusi extends Model
{
    protected $table = "solusi";
    protected $fillable = ['status','penyebab','solusi'];

}
