<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_pemeriksaan extends Model
{
    protected $table = "rule_pemeriksaan";
    protected $fillable = ['no_pemeriksaan','id_rule'];
}
